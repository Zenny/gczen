use std::io::Seek;
use std::{
    cmp::Ordering,
    io::{self, Read, Write},
    path::PathBuf,
};

use glam::{Quat, Vec2, Vec3, Vec4, Affine2, Mat4};
use bevy_color::{Color, ColorToComponents};
use mugltf::{AccessorComponentType, MeshPrimitive};
use num_enum::TryFromPrimitive;
use serde_json::{Map, Value};

use crate::{
    bytes::{read_i16, read_u8},
    gc::{error::ReadError, types::mesh::PrimitiveKind},
    w, wraw,
};

/// ( (i%2)*4, (i/2)*4 )
pub const REL_MAP_2: [(usize, usize); 4] = [(0, 0), (4, 0), (0, 4), (4, 4)];

/// ( i%4, i/4 )
pub const REL_MAP_4: [(usize, usize); 16] = [
    (0, 0),
    (1, 0),
    (2, 0),
    (3, 0),
    (0, 1),
    (1, 1),
    (2, 1),
    (3, 1),
    (0, 2),
    (1, 2),
    (2, 2),
    (3, 2),
    (0, 3),
    (1, 3),
    (2, 3),
    (3, 3),
];

pub const BYTE_TO_COLOR: f32 = 1.0 / 255.0;
pub const BIT3_TO_COLOR: f32 = 1.0 / 7.0;
pub const BIT4_TO_COLOR: f32 = 1.0 / 15.0;
pub const BIT5_TO_COLOR: f32 = 1.0 / 31.0;
pub const BIT6_TO_COLOR: f32 = 1.0 / 63.0;
// Seems a little silly but the name gives it meaning
// Need the 0.99 to make up for some kind of floating error
pub const COLOR_TO_BYTE: f32 = 255.99;
pub const COLOR_TO_BIT3: f32 = 7.99;
pub const COLOR_TO_BIT4: f32 = 15.99;
pub const COLOR_TO_BIT5: f32 = 31.99;
pub const COLOR_TO_BIT6: f32 = 63.99;
// todo: I am of the belief the entire i16 range is used here, which would make sense...
//  but not everything on earth makes sense
/// Convert an i16 range based rotation to an f32 in the range -180..180
pub const I16ROT_TO_DEG180: f32 = 180.0 / 32768.0;
pub const DEG180_TO_I16ROT: f32 = 32768.0 / 180.0;

const PRIM_SHIFT: u8 = 3;
const PRIM_MASK: u8 = 0x78;

pub type Mat3x2 = Affine2;

/// Writes a value to the name table, you can get the offset relative to the start of the name
/// table by using position() on the writer before calling this
pub fn write_to_name_table<W: Write + Seek>(w: &mut W, name: String) -> Result<(), io::Error> {
    let mut name_bytes = name.as_bytes().to_vec();
    name_bytes.push(0);

    wraw!(w, name_bytes)?;

    Ok(())
}

pub fn encode_draw_prim_cmd(kind: PrimitiveKind) -> u8 {
    0x80 | (((kind as u16 as u8) << PRIM_SHIFT) & PRIM_MASK)
}

pub fn decode_draw_prim_cmd(cmd: u8) -> Result<PrimitiveKind, ReadError> {
    let converted = (cmd & PRIM_MASK) >> PRIM_SHIFT;
    PrimitiveKind::try_from_primitive(converted as u16)
        .map_err(|_| ReadError::NoKnownPrimitiveKind(converted))
}

#[allow(unused)]
pub fn get_szs_path(mut path: PathBuf) -> Option<PathBuf> {
    loop {
        if path.extension().is_some_and(|e| e == "szs") {
            break;
        }
        if !path.pop() {
            return None;
        }
    }
    Some(path)
}

/// Advances position
pub fn read_color_u8<R: Read>(buf: &mut R) -> Result<Color, ReadError> {
    let r = read_u8(buf)?;
    let g = read_u8(buf)?;
    let b = read_u8(buf)?;
    let a = read_u8(buf)?;
    Ok(Color::linear_rgba(
        r as f32 * BYTE_TO_COLOR,
        g as f32 * BYTE_TO_COLOR,
        b as f32 * BYTE_TO_COLOR,
        a as f32 * BYTE_TO_COLOR,
    ))
}

pub fn write_color_u8<W: Write>(w: &mut W, color: &Color) -> Result<(), io::Error> {
    let [r, g, b, a] = color.to_linear().to_f32_array();
    let r = r * COLOR_TO_BYTE;
    let g = g * COLOR_TO_BYTE;
    let b = b * COLOR_TO_BYTE;
    let a = a * COLOR_TO_BYTE;

    w!(w, r as u8)?;
    w!(w, g as u8)?;
    w!(w, b as u8)?;
    w!(w, a as u8)?;
    Ok(())
}

/// Advances position
pub fn read_color_i16<R: Read>(buf: &mut R) -> Result<Color, ReadError> {
    let r = read_i16(buf)?;
    let g = read_i16(buf)?;
    let b = read_i16(buf)?;
    let a = read_i16(buf)?;
    Ok(Color::linear_rgba(
        r as f32 * BYTE_TO_COLOR,
        g as f32 * BYTE_TO_COLOR,
        b as f32 * BYTE_TO_COLOR,
        a as f32 * BYTE_TO_COLOR,
    ))
}

pub fn write_color_i16<W: Write>(w: &mut W, color: &Color) -> Result<(), io::Error> {
    let [r, g, b, a] = color.to_linear().to_f32_array();
    let r = r * COLOR_TO_BYTE;
    let g = g * COLOR_TO_BYTE;
    let b = b * COLOR_TO_BYTE;
    let a = a * COLOR_TO_BYTE;

    w!(w, r as i16)?;
    w!(w, g as i16)?;
    w!(w, b as i16)?;
    w!(w, a as i16)?;
    Ok(())
}

pub fn insert_extra(cur: &mut Value, k: String, v: Value) {
    match cur {
        Value::Object(_) => {}
        _ => {
            *cur = Value::Object(Map::new());
        }
    }

    let map = match cur {
        Value::Object(o) => o,
        _ => unreachable!(),
    };

    map.insert(k, v);
}

/// If none, no alignment is needed
pub fn accessor_alignment(component_type: &AccessorComponentType) -> Option<usize> {
    match component_type {
        AccessorComponentType::Byte => None,
        AccessorComponentType::UnsignedByte => None,
        AccessorComponentType::Short => Some(2),
        AccessorComponentType::UnsignedShort => Some(2),
        AccessorComponentType::UnsignedInt => Some(4),
        AccessorComponentType::Float => Some(4),
    }
}

#[rustfmt::skip]
pub fn new_mat4_rows(rows: [f32; 16]) -> Mat4 {
    Mat4::from_cols_array(&[
        rows[0], rows[4], rows[8], rows[12],
        rows[1], rows[5], rows[9], rows[13],
        rows[2], rows[6], rows[10], rows[14],
        rows[3], rows[7], rows[11], rows[15],
    ])
}

#[rustfmt::skip]
pub fn get_mat4_rows(mat: &Mat4) -> [f32; 16] {
    let col = mat.to_cols_array();
    [
        col[0], col[4], col[8], col[12],
        col[1], col[5], col[9], col[13],
        col[2], col[6], col[10], col[14],
        col[3], col[7], col[11], col[15],
    ]
}

#[rustfmt::skip]
pub fn new_mat2x3_rows(rows: [f32; 6]) -> Mat3x2 {
    Mat3x2::from_cols_array(&[
        rows[0], rows[2], rows[4],
        rows[1], rows[3], rows[5]
    ])
}

#[rustfmt::skip]
pub fn get_mat2x3_rows(mat: &Mat3x2) -> [f32; 6] {
    let col = mat.to_cols_array();
    [
        col[0], col[3],
        col[1], col[4],
        col[2], col[5],
    ]
}

pub fn is_default<T: Default + PartialEq>(t: &T) -> bool {
    t == &T::default()
}

#[allow(unused)]
pub fn find_min_material(a: &&MeshPrimitive, b: &&MeshPrimitive) -> Ordering {
    if let Some(a) = &a.material {
        if let Some(b) = &b.material {
            a.cmp(b)
        } else {
            Ordering::Less
        }
    } else if b.material.is_some() {
        Ordering::Greater
    } else {
        Ordering::Equal
    }
}

pub fn remove_start_letters(s: &str, num: usize) -> &str {
    match s.char_indices().skip(num).next() {
        Some((pos, _)) => &s[pos..],
        None => "",
    }
}

/// Normalize. Rounds the weight to the tenth decimal place (0.1) too
pub fn normalize_weights(w: &mut [f32]) {
    let sum: f32 = w.iter().sum();
    let scale = 1.0 / sum;
    for value in w {
        // Rescale the values
        *value *= scale;
        // round to tenth decimal
        *value = (*value * 10.0).round() * 0.1;
    }
}

/// Taken from ttf-parser
/// Just like TryFrom<N>, but for numeric types not supported by the Rust's std.
pub trait TryNumFrom<T>: Sized {
    /// Casts between numeric types.
    fn try_num_from(_: T) -> Option<Self>;
}

impl TryNumFrom<f32> for u8 {
    #[inline]
    fn try_num_from(v: f32) -> Option<Self> {
        i32::try_num_from(v).and_then(|v| u8::try_from(v).ok())
    }
}

impl TryNumFrom<f32> for i8 {
    #[inline]
    fn try_num_from(v: f32) -> Option<Self> {
        i32::try_num_from(v).and_then(|v| i8::try_from(v).ok())
    }
}

impl TryNumFrom<f32> for i16 {
    #[inline]
    fn try_num_from(v: f32) -> Option<Self> {
        i32::try_num_from(v).and_then(|v| i16::try_from(v).ok())
    }
}

impl TryNumFrom<f32> for u16 {
    #[inline]
    fn try_num_from(v: f32) -> Option<Self> {
        i32::try_num_from(v).and_then(|v| u16::try_from(v).ok())
    }
}

#[allow(clippy::manual_range_contains)]
impl TryNumFrom<f32> for i32 {
    #[inline]
    fn try_num_from(v: f32) -> Option<Self> {
        // Based on https://github.com/rust-num/num-traits/blob/master/src/cast.rs

        // Float as int truncates toward zero, so we want to allow values
        // in the exclusive range `(MIN-1, MAX+1)`.

        // We can't represent `MIN-1` exactly, but there's no fractional part
        // at this magnitude, so we can just use a `MIN` inclusive boundary.
        const MIN: f32 = i32::MIN as f32;
        // We can't represent `MAX` exactly, but it will round up to exactly
        // `MAX+1` (a power of two) when we cast it.
        const MAX_P1: f32 = i32::MAX as f32;
        if v >= MIN && v < MAX_P1 {
            Some(v as i32)
        } else {
            None
        }
    }
}

/// Tries to convert a primitive into a type and fails with a ReadError if it can't.
/// Do not do a read function in here or something, as it would read more than once.
#[macro_export]
macro_rules! try_from_repr {
    ($p:expr => $t:ty) => {{
        use num_enum::TryFromPrimitive;
        <$t>::try_from_primitive($p)
            .map_err(|_| ReadError::NoKnown(stringify!($t), format!("{}={:X}", stringify!($p), $p)))
    }};
}

/// Convert an Option<T> into a Result<T, E> quicker and cleaner
#[macro_export]
macro_rules! errify {
    ($v:expr, $e:expr) => {
        $v.ok_or_else(|| $e)
    };
}

/// Get an element at index, failing with a ReadError if it can't
#[macro_export]
macro_rules! get {
    ($v:expr, $i:expr) => {
        $v.get($i)
            .ok_or_else(|| ReadError::VecGet(stringify!($v), $i))
    };
}

/// Gives an ind to an existing matching item or pushes the item to the vec.
/// The item is expected to have copy and partialeq
#[macro_export]
macro_rules! find_or_insert {
    ($v:expr, $i:expr) => {
        $v.iter().position(|c| *c == $i).unwrap_or_else(|| {
            let ind = $v.len();
            $v.push($i);
            ind
        })
    };
}

pub trait TotalEq {
    /// Compares floating point types with total_cmp and returns whether they are equal or not.
    /// This is to avoid small cases such as `-0.0 == 0.0` being true when it should be false.
    fn total_eq(&self, other: &Self) -> bool;
}

impl TotalEq for Quat {
    fn total_eq(&self, other: &Self) -> bool {
        self.x.total_cmp(&other.x) == Ordering::Equal
            && self.y.total_cmp(&other.y) == Ordering::Equal
            && self.z.total_cmp(&other.z) == Ordering::Equal
            && self.w.total_cmp(&other.w) == Ordering::Equal
    }
}

impl TotalEq for Vec2 {
    fn total_eq(&self, other: &Self) -> bool {
        self.x.total_cmp(&other.x) == Ordering::Equal
            && self.y.total_cmp(&other.y) == Ordering::Equal
    }
}

impl TotalEq for Vec3 {
    fn total_eq(&self, other: &Self) -> bool {
        self.x.total_cmp(&other.x) == Ordering::Equal
            && self.y.total_cmp(&other.y) == Ordering::Equal
            && self.z.total_cmp(&other.z) == Ordering::Equal
    }
}

impl TotalEq for Vec4 {
    fn total_eq(&self, other: &Self) -> bool {
        self.x.total_cmp(&other.x) == Ordering::Equal
            && self.y.total_cmp(&other.y) == Ordering::Equal
            && self.z.total_cmp(&other.z) == Ordering::Equal
            && self.w.total_cmp(&other.w) == Ordering::Equal
    }
}

impl TotalEq for f32 {
    fn total_eq(&self, other: &Self) -> bool {
        self.total_cmp(other) == Ordering::Equal
    }
}
