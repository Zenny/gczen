use crate::{companion, gc::rarc::RarcFlags};

companion!(RarcEntryCompanion {
    id: u64,
    rarc_flags: RarcFlags
});
