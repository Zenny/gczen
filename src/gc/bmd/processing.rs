//! Processing types and functions

use std::{
    fs::read,
    io::{BufReader, Cursor},
    collections::{HashMap, HashSet},
    mem::transmute,
    path::PathBuf
};

use glam::{Mat4, Vec2, Vec3A, Vec4};
use bevy_color::{Color, ColorToPacked};

use crate::{
    companion::{
        bmd::TextureProperties,
        bti::BtiCompanion
    },
    gc::{
        bmd::{error::GltfReadError, shp1::MatrixData},
        bti::Bti,
        types::mesh::IndexedPrimitive,
    },
};

/// A vertex complete with some data
#[derive(Debug, Clone, PartialEq)]
pub struct Vertex {
    pub ind: u32,
    pub pos: Vec3A,
    pub norm: Option<Vec3A>,
    pub uvs: [Vec2; 8],
    /// the bone inds connected to the weights (eg. JOINT_0)
    pub bones: [u16; 4],
    pub weights: [f32; 4],
    pub colors: [Color; 2],
}

impl Vertex {
    pub fn new(ind: u32) -> Self {
        Self {
            ind,
            pos: Default::default(),
            norm: Default::default(),
            uvs: [Default::default(); 8],
            bones: [0; 4],
            weights: [0.0; 4],
            colors: [Color::linear_rgba(1.0, 1.0, 1.0, 1.0); 2],
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct ProcessedVertData {
    pub pos: Vec3A,
    pub norm: Option<Vec3A>,
}

pub struct CompiledVertexInfo {
    pub pos: Vec<Vec3A>,
    pub norms: Vec<Vec3A>,
    pub bones: Vec<[u16; 4]>,
    pub weights: Vec<[f32; 4]>,
    pub uvs: [Vec<Vec2>; 8],
    pub colors: [Vec<Color>; 2],
}

impl CompiledVertexInfo {
    /// Compile the vertices into separate vecs of data in the same order. The length of a
    /// vec will be 0 if it is unused, or verts.len() if it is used at all by even 1 vertex.
    /// In the latter case, vertices that do not supply data will be given a default.
    /// This is so that indices can remain consistent.
    pub fn compile_verts(
        verts: Vec<Vertex>,
        has_normals: bool,
        has_tex_chan: [bool; 8],
        has_color_chan: [bool; 2],
    ) -> Self {
        let mut pos = Vec::with_capacity(verts.len());

        let mut norms = if has_normals {
            Vec::with_capacity(verts.len())
        } else {
            Vec::new()
        };

        let mut bones = Vec::with_capacity(verts.len());
        let mut weights = Vec::with_capacity(verts.len());

        let mut uvs = has_tex_chan.map(|has| {
            if has {
                Vec::with_capacity(verts.len())
            } else {
                Vec::new()
            }
        });

        let mut colors = has_color_chan.map(|has| {
            if has {
                Vec::with_capacity(verts.len())
            } else {
                Vec::new()
            }
        });

        for vert in verts {
            pos.push(vert.pos);

            if has_normals {
                norms.push(vert.norm.unwrap_or_default());
            }

            bones.push(vert.bones);
            weights.push(vert.weights);

            for (chan, uv) in vert.uvs.into_iter().enumerate() {
                if has_tex_chan[chan] {
                    uvs[chan].push(uv);
                }
            }

            for (chan, color) in vert.colors.into_iter().enumerate() {
                if has_color_chan[chan] {
                    colors[chan].push(color);
                }
            }
        }

        Self {
            pos,
            norms,
            bones,
            weights,
            uvs,
            colors,
        }
    }
}

/// Performs matrix multipilcations on vertex data in a very specifically ordered way
pub fn mat_mul_vert_data(pos: &Vec3A, norm: &Option<Vec3A>, mat: &Mat4) -> ProcessedVertData {
    let cur_pos = Vec4::new(pos.x, pos.y, pos.z, 1.0);
    let cur_norm = norm.map(|n| Vec4::new(n.x, n.y, n.z, 1.0));
    // convert to row major
    let mrow = mat.transpose();

    // "That Company" likes row major, so the norm is modified by row major
    // (but not inverse)
    let new_norm = cur_norm.map(|n| mrow * n);

    let mut inv = mrow.inverse();

    // Back to column major
    inv = inv.transpose();

    // The pos that comes out correctly is row-inverse column major multiplication,
    // I think because modern systems like column major (glTF) but "That Company" likes
    // row major, so row major is what had to be inversed
    let new_pos = inv * cur_pos;

    ProcessedVertData {
        pos: Vec3A::new(new_pos.x, new_pos.y, new_pos.z),
        norm: new_norm.map(|n| Vec3A::new(n.x, n.y, n.z)),
    }
}

/// Performs matrix multipilcations on vertex pos in a very specifically ordered way but
/// inverse
pub fn inv_mat_mul_vert_pos(pos: Vec3A, mat: &Mat4) -> Vec3A {
    let cur_pos = Vec4::new(pos.x, pos.y, pos.z, 1.0);

    // See other function for details
    let new_pos = *mat * cur_pos;

    Vec3A::new(new_pos.x, new_pos.y, new_pos.z)
}

/// Performs matrix multipilcations on vertex normal in a very specifically ordered way but
/// inverse
pub fn inv_mat_mul_vert_norm(norm: Vec3A, mat: &Mat4) -> Vec3A {
    let cur_norm = Vec4::new(norm.x, norm.y, norm.z, 1.0);
    // convert to row major
    let mrow = mat.transpose();

    let inv = mrow.inverse();

    // "That Company" likes row major, so the norm is modified by row major
    let new_norm = inv * cur_norm;

    Vec3A::new(new_norm.x, new_norm.y, new_norm.z)
}

/// Deduplicate verts, replacing any duplicates with all of the following data matching:
/// - position
/// - texcoords (all)
/// - normal
/// - colors (all)
/// - bones (all)
/// - weights (all)
///
/// Note that this is not a merge by distance, the positions have to be exactly the same.
pub fn deduplicate(mut verts: Vec<Vertex>) -> (Vec<u32>, Vec<Vertex>) {
    let mut inds = Vec::with_capacity(verts.len());

    let mut has = HashMap::with_capacity(verts.len());
    let mut to_keep = HashSet::with_capacity(verts.len());
    let mut removed = 0;

    for us in verts.iter() {
        let pos: [u32; 3] = unsafe { transmute(us.pos.to_array()) };

        let uvs: [[u32; 2]; 8] = us.uvs.clone().map(|u| unsafe { transmute(u.to_array()) });

        let norm: Option<[u32; 3]> = us.norm.map(|n| unsafe { transmute(n.to_array()) });

        let weights: [u32; 4] = unsafe { transmute(us.weights) };

        let key = (
            pos,
            uvs,
            norm,
            us.colors.clone().map(|c| c.to_linear().to_u8_array()),
            us.bones,
            weights,
        );

        if let Some(existing) = has.get(&key) {
            removed += 1;
            // Remap to the index that already matches us and came first
            inds.push(*existing);
        } else {
            // Subtract how many we know should have been removed so far to get the "real" index.
            // This can work only because the list is sorted by index already
            let real_ind = us.ind - removed;
            has.insert(key, real_ind);
            inds.push(real_ind);
            to_keep.insert(us.ind);
        }
    }

    // We have to use to_remove because we don't have the real_ind known here
    verts.retain(|v| to_keep.contains(&v.ind));

    (inds, verts)
}

/// Compacts the matrix data (Each one is a draw call) into as few as possible (resulting in
/// as few draw calls as possible). Each one can hold up to 10 Drw1 inds
pub struct MatrixDataCompactor {
    mats: Vec<MatrixData>,
}

impl MatrixDataCompactor {
    pub fn new() -> Self {
        Self {
            mats: Vec::with_capacity(4),
        }
    }

    /// Returns the matrix ind and PNMI inds that point to the inds which were inserted by the
    /// compactor, in the same order as the inds that were passed in
    pub fn insert_prim(&mut self, inds: Vec<u16>, prim: IndexedPrimitive) -> (usize, Vec<u16>) {
        let mut out_inds = vec![0xFFFF; inds.len()];

        for (mat_ind, mat) in self.mats.iter_mut().enumerate() {
            if mat.can_fit(&inds) {
                mat.merge_inds(inds, &mut out_inds);
                mat.prims.push(prim);
                return (mat_ind, out_inds);
            }
        }

        // We need to flatten the inds because there can be duplicates
        let mut flat = Vec::with_capacity(inds.len());
        for (i, ind) in inds.into_iter().enumerate() {
            if let Some(pos) = flat.iter().position(|flat| *flat == ind) {
                out_inds[i] = pos as u16;
            } else {
                let pos = flat.len();
                flat.push(ind);
                out_inds[i] = pos as u16;
            }
        }

        #[cfg(debug_assertions)]
        {
            assert!(!out_inds.contains(&0xFFFF));
        }

        // todo split up a prim that has >10 inds
        if flat.len() > 10 {
            todo!()
        }

        // We didn't fit anywhere, so make a new one
        let mat = MatrixData {
            // todo: Assumption: cur is not always ind 0 in reality,
            //  however since we only use cur with non-skinned mesh
            //  it should always only be used when it matches ind 0
            cur: flat[0],
            inds: flat,
            prims: vec![prim],
        };

        let mat_ind = self.mats.len();

        self.mats.push(mat);
        return (mat_ind, out_inds);
    }

    pub fn finalize(self) -> Vec<MatrixData> {
        self.mats
    }
}

pub fn bti_to_properties(bti: Bti) -> TextureProperties {
    let png = bti.path.with_extension("png");
    let file_name = png.file_name().unwrap();
    let name = file_name.to_string_lossy().to_string();

    TextureProperties {
        tex_name: name,
        header: bti.header,
    }
}

pub fn properties_to_bti(
    tex_dir: &PathBuf,
    props: &TextureProperties,
) -> Result<Bti, GltfReadError> {
    let file = tex_dir.join(&props.tex_name);
    match file.extension() {
        Some(o) => {
            let s = o.to_string_lossy().to_lowercase();
            match s.as_str() {
                "png" => {
                    let data = read(&file)?;
                    let cursor = Cursor::new(data);
                    let buf_data = BufReader::new(cursor);
                    let companion = BtiCompanion::read(&file)?;

                    let mut bti = Bti::read_png(file, buf_data, companion)?;

                    let width = bti.header.width;
                    let height = bti.header.height;

                    // We have to overwrite the bti header because those companions won't help
                    // here and are not accurate
                    bti.header = props.header.clone();
                    // But we need to keep the width and height which are precalculated
                    bti.header.width = width;
                    bti.header.height = height;

                    Ok(bti)
                }
                // todo bti support?
                ext => Err(GltfReadError::InvalidTexExt(ext.to_string())),
            }
        }
        None => Err(GltfReadError::InvalidTexExt("None supplied".to_string())),
    }
}

#[cfg(test)]
mod test {
    use glam::{EulerRot, Mat4, Quat, Vec3, Vec3A};

    use crate::gc::bmd::processing::{
        inv_mat_mul_vert_norm, inv_mat_mul_vert_pos, mat_mul_vert_data,
    };

    /// The mat_mul and inv_mat_mul functions should cancel eachother out
    #[test]
    fn mat_mul() {
        let pos = Vec3A::new(1.2, 2.3, 3.4);
        let norm = Some(Vec3A::new(0.1, 1.2, 2.3));
        let mat = Mat4::from_rotation_translation(
            Quat::from_euler(
                EulerRot::XYZ,
                90.0f32.to_radians(),
                45.0f32.to_radians(),
                0.0,
            ),
            Vec3::new(5.0, 1.5, 4.4),
        );

        let data = mat_mul_vert_data(&pos, &norm, &mat);
        let again_pos = inv_mat_mul_vert_pos(data.pos, &mat);
        let again_norm = inv_mat_mul_vert_norm(data.norm.unwrap(), &mat);

        let leeway = f32::EPSILON * 2.0;

        assert!((pos.x - again_pos.x).abs() <= leeway);
        assert!((pos.y - again_pos.y).abs() <= leeway);
        assert!((pos.z - again_pos.z).abs() <= leeway);

        let norm = norm.unwrap();

        assert!((norm.x - again_norm.x).abs() <= leeway);
        assert!((norm.y - again_norm.y).abs() <= leeway);
        assert!((norm.z - again_norm.z).abs() <= leeway);
    }
}
