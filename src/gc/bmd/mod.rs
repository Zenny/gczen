use std::{
    collections::{BTreeMap, HashMap},
    fs::{create_dir, write},
    io::{BufRead, Read, Seek, SeekFrom, Write},
    path::PathBuf,
};

use bevy_color::ColorToComponents;
use glam::{Mat4, Vec3A};
use itertools::Itertools;
use mugltf::{
    AccessorComponentType, AccessorType, BufferViewTarget, Gltf, Material, Mesh,
    PbrMetallicRoughness, Skin,
};
use num_enum::TryFromPrimitive;
use serde_json::json;

use crate::{
    bytes::read_u32,
    companion::{bmd::BmdCompanion, rarc_entry::RarcEntryCompanion},
    errify,
    error::GltfError,
    gc::{
        align::{write_align, AlignType},
        bmd::{
            consts::{BILLBOARD_NORMAL, BILLBOARD_XY, BILLBOARD_Y},
            drw1::{Drw1, WeightKind},
            error::{BmdWriteError, GltfReadError},
            evp1::Evp1,
            inf1::{Inf1, NodeKind},
            jnt1::{Jnt1, MatrixKind},
            mat3::{Culling, Mat3},
            processing::{
                bti_to_properties, deduplicate, mat_mul_vert_data, properties_to_bti,
                CompiledVertexInfo, ProcessedVertData, Vertex,
            },
            shp1::Shp1,
            tex1::Tex1,
            vtx1::Vtx1,
        },
        error::ReadError,
        types::mesh::{DataVec, GxAttribute, Mode},
    },
    gltf::{gltf_builder::GltfBuilder, gltf_info::GltfInfo},
    util::insert_extra,
    w, wat,
};

pub mod consts;
pub mod drw1;
pub mod error;
pub mod evp1;
pub mod inf1;
pub mod jnt1;
pub mod mat3;
pub mod processing;
pub mod shp1;
pub mod tex1;
pub mod vtx1;

const J3D2: u32 = 0x4A334432;
const BMD: u32 = 0x626D6433;
const SVR3: u32 = 0x53565233;

#[derive(Debug, Clone)]
pub struct Bmd {
    pub path: PathBuf,
    pub len: u32,
    pub inf1: Inf1,
    pub vtx1: Vtx1,
    pub evp1: Evp1,
    pub drw1: Drw1,
    pub jnt1: Jnt1,
    pub shp1: Shp1,
    pub mat3: Mat3,
    pub tex1: Tex1,
    /// Rarc datas are only available if the file is stored within a Rarc file
    pub rarc_data: Option<RarcEntryCompanion>,
}

impl Bmd {
    pub fn new<R: BufRead + Seek>(
        path: PathBuf,
        buf: &mut R,
        rarc_data: Option<RarcEntryCompanion>,
    ) -> Result<Self, ReadError> {
        log::info!("Reading {:?}", path);
        // Start over just in case
        buf.seek(SeekFrom::Start(0))?;

        let j3d2 = read_u32(buf)?;
        let bmd = read_u32(buf)?;
        // J3D2 bmd3
        if j3d2 != J3D2 || bmd != BMD {
            return Err(ReadError::ReadCheck("J3D2bmd3"));
        }

        let len = read_u32(buf)?;
        // always 8
        let _num_sections = read_u32(buf)?;

        let _svr3 = read_u32(buf)?;

        buf.seek(SeekFrom::Current(0x0C))?;

        let header_len = 32;

        let inf1 = Inf1::new(buf)?;

        let vtx1_offset = header_len + inf1.len;
        // skip to get to vtx1, skips padding after hierarchy
        buf.seek(SeekFrom::Start((vtx1_offset) as u64))?;

        let mut vtx1 = Vtx1::new(buf, vtx1_offset)?;

        let evp1_offset = vtx1_offset + vtx1.len;
        buf.seek(SeekFrom::Start((evp1_offset) as u64))?;

        let evp1 = Evp1::new(buf, evp1_offset)?;

        let drw1_offset = evp1_offset + evp1.len;
        buf.seek(SeekFrom::Start((drw1_offset) as u64))?;

        let drw1 = Drw1::new(buf, drw1_offset)?;

        let jnt1_offset = drw1_offset + drw1.len;
        buf.seek(SeekFrom::Start((jnt1_offset) as u64))?;

        let jnt1 = Jnt1::new(buf, jnt1_offset)?;

        let shp1_offset = jnt1_offset + jnt1.len;
        buf.seek(SeekFrom::Start((shp1_offset) as u64))?;

        let shp1 = Shp1::new(buf, shp1_offset)?;

        // Now we can trim useless stuff from vtx1.
        // This is useful basically only if we fail to convert to gltf and write out bmd
        vtx1.trim(&shp1);

        let mat3_offset = shp1_offset + shp1.len;
        buf.seek(SeekFrom::Start((mat3_offset) as u64))?;

        let mat3 = Mat3::new(buf, mat3_offset)?;

        let tex1_offset = mat3_offset + mat3.len;
        buf.seek(SeekFrom::Start((tex1_offset) as u64))?;

        let tex1 = Tex1::new(buf, tex1_offset)?;

        Ok(Self {
            path,
            len,
            inf1,
            vtx1,
            evp1,
            drw1,
            jnt1,
            shp1,
            mat3,
            tex1,
            rarc_data,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        let start = w.stream_position()?;
        w!(w, J3D2)?;
        w!(w, BMD)?;
        let length_offset = w.stream_position()?;
        // Length written later
        w!(w, 0u32)?;
        w!(w, 8u32)?;
        w!(w, SVR3)?;

        write_align(w, 32, AlignType::FF)?;

        self.inf1.write_bmd(w)?;

        self.vtx1.write_bmd(w)?;

        self.evp1.write_bmd(w)?;

        self.drw1.write_bmd(w)?;

        self.jnt1.write_bmd(w)?;

        self.shp1.write_bmd(w)?;

        self.mat3.write_bmd(w)?;

        self.tex1.write_bmd(w)?;

        let len = (w.stream_position()? - start) as u32;
        wat!(w, len, length_offset);

        Ok(())
    }

    pub fn read_gltf<R: Read>(path: PathBuf, data: R, bmd_data: BmdCompanion) -> Result<Self, GltfReadError> {
        log::info!("Compiling {:?}...", path);

        let extless = path.with_extension("");
        let os_name = errify!(
            extless.file_name(),
            GltfReadError::InvalidGltfPath(path.clone())
        )?;
        let name = os_name.to_string_lossy().to_string();
        let dir = path.parent().unwrap().join(format!("_{}_tex", name));

        // Shame we have to iter here and not into_iter
        let texs = bmd_data
            .textures
            .iter()
            .map(|props| properties_to_bti(&dir, props))
            .collect::<Result<Vec<_>, _>>()?;

        let rarc_data = bmd_data.rarc_data.clone();

        let gltf: Gltf = serde_json::from_reader(data)?;
        let info = GltfInfo::new(&gltf, bmd_data)?;

        Ok(Self {
            path,
            // can be skipped because write_bmd calculates it
            len: 0,
            inf1: info.inf1,
            vtx1: info.vtx1,
            evp1: info.evp1,
            drw1: info.drw1,
            jnt1: info.jnt1,
            shp1: info.shp1,
            mat3: info.mat3,
            tex1: Tex1 { len: 0, texs },
            rarc_data,
        })
    }

    pub fn write_gltf(&self, dir: &PathBuf) -> Result<(), GltfError> {
        log::info!("Converting file {:?}...", self.path);

        // could speed it up by setting vec capacities
        let mut builder = GltfBuilder::new();

        let gltf_path = self.path.with_extension("gltf");
        let name = errify!(
            gltf_path.file_name(),
            GltfError::InvalidFileName(self.path.clone())
        )?
        .to_string_lossy();
        let out_path = dir.join(name.as_ref());

        let extless_name = if let Some(raw_name) = name.strip_suffix(".gltf") {
            raw_name.to_string()
        } else {
            name.to_string()
        };

        let tex_dir = dir.join(format!("_{}_tex", extless_name));

        let skin = Skin {
            inverse_bind_matrices: None,
            skeleton: None,
            joints: vec![],
            name: extless_name,
            extras: Default::default(),
        };

        let _skin_id = builder.add_skin(skin);

        let bones = self.setup_vert_accessors(&mut builder)?;

        let mut bone_remap = HashMap::with_capacity(self.jnt1.bone_data.len());

        if self.tex1.texs.len() > 0 {
            // It's okay if it exists don't crash
            create_dir(&tex_dir).ok();
        }

        for bti in self.tex1.texs.iter().unique_by(|t| t.path.clone()) {
            bti.write_png(&tex_dir)?;
        }

        BmdCompanion::new(
            self.rarc_data.clone(),
            self.tex1
                .texs
                .clone()
                .into_iter()
                .map(bti_to_properties)
                .collect(),
            self.vtx1.formats.clone(),
            self.mat3.hard_params.clone(),
            self.mat3.materials.clone(),
            self.mat3.indirect_texes.clone(),
        )
        .write(&out_path)?;

        // Root will never be a material, and also doesn't have a parent
        let mut fake_parent = Default::default();

        let num_shape_jnt =
            self.inf1.hierarchy.map.iter().fold(
                0,
                |a, (_, b)| {
                    if b.kind.is_material() {
                        a
                    } else {
                        a + 1
                    }
                },
            );

        let node_id = errify!(
            self.nodes_from_inf1(
                0,
                None,
                &mut fake_parent,
                &mut builder,
                &mut bone_remap,
                &mut (num_shape_jnt as u16),
            )?,
            GltfError::RootNodeFailed
        )?;

        // Invert the joints since recursion would have put them in backwards
        builder.invert_skin_joints(0);

        let mut bone_bytes = Vec::with_capacity(bones.len() * 2 * 4);

        for bone_array in bones {
            let a = bone_remap[&bone_array[0]];
            let b = bone_remap[&bone_array[1]];
            let c = bone_remap[&bone_array[2]];
            let d = bone_remap[&bone_array[3]];
            bone_bytes.extend(a.to_le_bytes());
            bone_bytes.extend(b.to_le_bytes());
            bone_bytes.extend(c.to_le_bytes());
            bone_bytes.extend(d.to_le_bytes());
        }

        builder.replace_buffer_view(
            format!("{}_BONES", GxAttribute::PositionNormalMatrixInd),
            bone_bytes,
        )?;

        builder.add_scene_node(node_id);

        let gltf = builder.finalize();

        write(out_path, serde_json::to_string(&gltf)?)?;

        Ok(())
    }

    /// Recurse through the hierarchy and convert it into gltf nodes in the same hierarchy order
    fn nodes_from_inf1(
        &self,
        id: u16,
        mut cur_mat: Option<usize>,
        parent: &mut mugltf::Node,
        builder: &mut GltfBuilder,
        bone_remap: &mut HashMap<u16, u16>,
        num_shape_jnt: &mut u16,
    ) -> Result<Option<usize>, GltfError> {
        let inf_node = errify!(self.inf1.hierarchy.get(&id), GltfError::NoInf1Node(id))?;
        let mut node = mugltf::Node {
            camera: None,
            children: vec![],
            skin: None,
            matrix: None,
            mesh: None,
            rotation: None,
            scale: None,
            translation: None,
            weights: vec![],
            name: "".to_string(),
            extras: Default::default(),
        };
        match inf_node.kind {
            NodeKind::Joint => {
                let jnt = errify!(
                    self.jnt1.bone_data.get(inf_node.ind as usize),
                    GltfError::NoJnt1Node(inf_node.ind)
                )?;

                node.name = jnt.name.clone();

                node.translation = Some(jnt.config.translate.to_array());
                node.rotation = Some(jnt.config.rotation.to_array());
                node.scale = Some(jnt.config.scale.to_array());

                insert_extra(
                    &mut node.extras,
                    "flags".to_string(),
                    json!(jnt.config.flags),
                );

                let compensate_scale = u8::from(!jnt.config.compensate_scale);

                // NOTE: Child joints should not inherit scale when compensate scale is on
                // so they get the property of yes or no because that's how blender works
                insert_extra(
                    &mut node.extras,
                    "compensate_scale".to_string(),
                    json!(compensate_scale),
                );

                if jnt.config.bounding_sphere_radius > 0.0 {
                    insert_extra(
                        &mut node.extras,
                        "bounding_sphere_radius".to_string(),
                        json!(jnt.config.bounding_sphere_radius),
                    );
                }

                let bbox = jnt.config.get_bounding_box_dims();

                // If size is 0 in any direction, we wouldn't be able to see it
                if bbox.x != 0.0 && bbox.y != 0.0 && bbox.z != 0.0 {
                    let bbox_center =
                        (jnt.config.bounding_box_min + jnt.config.bounding_box_max) * 0.5;

                    insert_extra(
                        &mut node.extras,
                        "bounding_box".to_string(),
                        json!({
                            "center": {
                                "x": bbox_center.x,
                                "y": bbox_center.y,
                                "z": bbox_center.z,
                            },
                            "size": {
                                "x": bbox.x,
                                "y": bbox.y,
                                "z": bbox.z,
                            }
                        }),
                    );
                }

                match jnt.config.matrix_kind {
                    MatrixKind::Standard => {
                        insert_extra(
                            &mut node.extras,
                            "billboard".to_string(),
                            json!(BILLBOARD_NORMAL),
                        );
                    }
                    MatrixKind::BillboardY => {
                        insert_extra(
                            &mut node.extras,
                            "billboard".to_string(),
                            json!(BILLBOARD_Y),
                        );
                    }
                    MatrixKind::BillboardXY => {
                        insert_extra(
                            &mut node.extras,
                            "billboard".to_string(),
                            json!(BILLBOARD_XY),
                        );
                    }
                }
            }
            NodeKind::Material => {
                let mat_index = inf_node.ind as usize;
                let mat = errify!(
                    self.mat3.materials.get(mat_index),
                    GltfError::NoMat3Node(inf_node.ind)
                )?;

                let mut material = Material {
                    pbr_metallic_roughness: Some(PbrMetallicRoughness {
                        base_color_factor: [1.0, 1.0, 1.0, 1.0],
                        base_color_texture: None,
                        metallic_factor: 0.0,
                        roughness_factor: 0.0,
                        metallic_roughness_texture: None,
                        extras: Default::default(),
                    }),
                    normal_texture: None,
                    occlusion_texture: None,
                    emissive_factor: [0.0, 0.0, 0.0],
                    // todo blend?
                    alpha_mode: Default::default(),
                    alpha_cutoff: 0.0,
                    double_sided: self
                        .mat3
                        .hard_params
                        .cull_modes
                        .get(mat.info.cull_mode)
                        .map(|c| *c)
                        .unwrap_or_else(|| Culling::Backface)
                        == Culling::None,
                    name: mat.name.clone(),
                    extras: json!({
                        "index": mat_index,
                    }),
                };

                // Pick the first tex as the other textures are generally additive or something
                let valid_tex = mat.info.tex_inds[0];
                if let Some(valid_tex) = valid_tex {
                    let valid_tex = valid_tex as usize;
                    let tex = errify!(
                        self.tex1.texs.get(valid_tex),
                        GltfError::InvalidMaterialTex(valid_tex, mat.name.clone())
                    )?;
                    let png_path = tex.path.with_extension("png");
                    let os_name = errify!(
                        png_path.file_name(),
                        GltfError::InvalidFileName(png_path.clone())
                    )?;
                    let tex_name = os_name.to_string_lossy().to_string();

                    let mut params = BTreeMap::new();
                    params.insert("wrap", json!(&tex.header.wrap));
                    params.insert("mag_filter", json!(&tex.header.mag_filter));

                    insert_extra(&mut material.extras, "tex".to_string(), json!(tex_name));
                    insert_extra(&mut material.extras, "params".to_string(), json!(&params));
                }

                cur_mat = Some(builder.add_material(material));
            }
            NodeKind::Shape => {
                let shape = errify!(
                    self.shp1.shapes.get(inf_node.ind as usize),
                    GltfError::NoShp1Node(inf_node.ind)
                )?;

                let mesh = Mesh {
                    primitives: shape.generate_prims(inf_node.ind, builder, cur_mat),
                    weights: vec![],
                    name: format!("shape_{}", inf_node.ind),
                    extras: Default::default(),
                };

                let mesh_ind = builder.add_mesh(mesh);

                let name = format!("shape_{}", inf_node.ind);

                node.name = name.clone();
                node.translation = Some([0.0, 0.0, 0.0]);
                node.rotation = Some([0.0, 0.0, 0.0, 1.0]);
                node.scale = Some([1.0, 1.0, 1.0]);

                if shape.bounding_sphere_radius > 0.0 {
                    insert_extra(
                        &mut node.extras,
                        "bounding_sphere_radius".to_string(),
                        json!(shape.bounding_sphere_radius),
                    );
                }

                let bbox = shape.get_bounding_box_dims();

                // If size is 0 in any direction, we wouldn't be able to see it
                if bbox.x != 0.0 && bbox.y != 0.0 && bbox.z != 0.0 {
                    let bbox_center = (shape.bounding_box_min + shape.bounding_box_max) * 0.5;

                    insert_extra(
                        &mut node.extras,
                        "bounding_box".to_string(),
                        json!({
                            "center": {
                                "x": bbox_center.x,
                                "y": bbox_center.y,
                                "z": bbox_center.z,
                            },
                            "size": {
                                "x": bbox.x,
                                "y": bbox.y,
                                "z": bbox.z,
                            }
                        }),
                    );
                }

                // todo maybe keep track of previous names or something to do automatic
                //  name change detection so users don't have to rename both?
                // This will be the "object", while the primary node is the "joint".
                // This is done because blender likes to export like this too. They can be related
                // by name. The "joint" here is not actually a joint, it is purely part of the
                // hierarchy for hierarchy's sake.
                let mut mesh_node = mugltf::Node {
                    camera: None,
                    children: vec![],
                    skin: Some(0),
                    matrix: None,
                    mesh: Some(mesh_ind),
                    rotation: None,
                    scale: None,
                    translation: None,
                    weights: vec![],
                    name,
                    extras: Default::default(),
                };

                match shape.mode {
                    Mode::SkinnedMesh => {
                        insert_extra(
                            &mut mesh_node.extras,
                            "billboard".to_string(),
                            json!(BILLBOARD_NORMAL),
                        );
                    }
                    Mode::Normal => {
                        insert_extra(
                            &mut mesh_node.extras,
                            "billboard".to_string(),
                            json!(BILLBOARD_NORMAL),
                        );
                    }
                    Mode::BillboardY => {
                        insert_extra(
                            &mut mesh_node.extras,
                            "billboard".to_string(),
                            json!(BILLBOARD_Y),
                        );
                    }
                    Mode::BillboardXY => {
                        insert_extra(
                            &mut mesh_node.extras,
                            "billboard".to_string(),
                            json!(BILLBOARD_XY),
                        );
                    }
                }

                builder.add_node(mesh_node);
            }
            _ => unreachable!(),
        }

        for child in inf_node.children.iter() {
            // We skip materials to save filespace
            if inf_node.kind.is_material() {
                // Material won't be a parent so we use current parent instead
                if let Some(child_node) = self.nodes_from_inf1(
                    *child,
                    cur_mat,
                    parent,
                    builder,
                    bone_remap,
                    num_shape_jnt,
                )? {
                    parent.children.push(child_node);
                }
            } else {
                if let Some(child_node) = self.nodes_from_inf1(
                    *child,
                    cur_mat,
                    &mut node,
                    builder,
                    bone_remap,
                    num_shape_jnt,
                )? {
                    node.children.push(child_node);
                }
            }
        }

        // We skip materials to save filespace
        if inf_node.kind.is_material() {
            Ok(None)
        } else {
            let node_id = builder.add_node(node);

            match inf_node.kind {
                NodeKind::Joint => {
                    *num_shape_jnt -= 1;
                    // Allow us to remap the internal bone ind to the node ind for gltf later
                    bone_remap.insert(inf_node.ind, *num_shape_jnt);

                    builder.add_joint_to_skin(0, node_id, inf_node.ind, &self.evp1);
                }
                NodeKind::Shape => {
                    *num_shape_jnt -= 1;

                    // Because blender hates us, it does not add false bones from the shapes if
                    // they're at the end of the hierarchy, and opts to keep them as Empties.
                    // Thus we add all shape nodes (the false joint ones) as real joints to force
                    // blender to behave
                    builder.add_joint_to_skin(0, node_id, 9999, &self.evp1);
                }
                _ => {}
            }
            Ok(Some(node_id))
        }
    }

    pub fn setup_vert_accessors(
        &self,
        builder: &mut GltfBuilder,
    ) -> Result<Vec<[u16; 4]>, GltfError> {
        let mut pos_ind: u32 = 0;

        let pos_view_id = builder.next_buffer_view_id();
        let pos_ind_view_id = pos_view_id + 1;

        // This should always exist
        let pos_ref = match errify!(
            self.vtx1.attributes.get(&GxAttribute::Position),
            GltfError::NoAttribute("VTX1 positions")
        )? {
            DataVec::Vec3(vecs) => vecs,
            o => {
                return Err(GltfError::InvalidDataVec(
                    "VTX1 positions",
                    "Vec3",
                    o.to_string(),
                ))
            }
        };

        let norm_ref = self
            .vtx1
            .attributes
            .get(&GxAttribute::Normal)
            .map(|inner| match inner {
                DataVec::Vec3(v) => Ok(v),
                o => Err(GltfError::InvalidDataVec(
                    "VTX1 normals",
                    "Vec3",
                    o.to_string(),
                )),
            })
            .transpose()?;

        let color0_ref = self
            .vtx1
            .attributes
            .get(&GxAttribute::Color0)
            .map(|inner| match inner {
                DataVec::Color(v) => Ok(v),
                o => Err(GltfError::InvalidDataVec(
                    "VTX1 color0",
                    "Color",
                    o.to_string(),
                )),
            })
            .transpose()?;

        let color1_ref = self
            .vtx1
            .attributes
            .get(&GxAttribute::Color1)
            .map(|inner| match inner {
                DataVec::Color(v) => Ok(v),
                o => Err(GltfError::InvalidDataVec(
                    "VTX1 color1",
                    "Color",
                    o.to_string(),
                )),
            })
            .transpose()?;

        // We'll mark what tex channels we're using for later (in case there's some weird
        // non uniform data going on (there probably isn't))
        let mut has_tex_chan = [false; 8];
        let mut has_color_chan = [false; 2];

        // Guess there might be 2 verts per pos
        let reserve_size = pos_ref.len() * 2;

        let mut verts = Vec::with_capacity(reserve_size);
        let mut processed_cache = HashMap::with_capacity(reserve_size);

        for (ind, s) in self.shp1.shapes.iter().enumerate() {
            for (i, mp) in s.mat_prims.iter().enumerate() {
                let mut trid =
                    Vec::with_capacity(mp.prims.iter().fold(0, |a, b| a + (b.verts.len() * 2)));
                for prim in mp.prims.iter() {
                    trid.extend(prim.ccw_triangulate()?);
                }
                // Calculate the offset in the inds
                // which is how many verts we have multiplied by 4 (inds are u32)
                let byte_offset = verts.len() * 4;

                let _pos_vert_ind_acc = builder.add_accessor(
                    pos_ind_view_id,
                    format!("INDS{:02}{:02}", ind, i),
                    byte_offset,
                    trid.len(),
                    AccessorComponentType::UnsignedInt,
                    AccessorType::Scalar,
                );

                for v in trid.into_iter() {
                    let mut vert = Vertex::new(pos_ind);
                    let drw1_ind =
                        s.find_drw1_ind(i, v.get(GxAttribute::PositionNormalMatrixInd) as usize);

                    let entry = errify!(
                        self.drw1.matrix_entries.get(drw1_ind),
                        GltfError::InvalidDrw1Matrix(drw1_ind)
                    )?;

                    if entry.kind == WeightKind::Bone {
                        // Contains a bone ind (evp1.matrices / jnt1.bone_data), it looks like
                        // if evp1 weights are len 1 they're compacted into this type
                        let bone_ind = entry.ind as usize;

                        let v_pos_ind = v.get(GxAttribute::Position) as usize;
                        let norm_ind = v.get(GxAttribute::Normal) as usize;

                        // Data we've already processed is fetched, or it's made new
                        let key = (v_pos_ind, norm_ind);
                        let processed = if processed_cache.contains_key(&key) {
                            errify!(processed_cache.get(&key), GltfError::NoProcessedVert(key))?
                        } else {
                            log::trace!("Generating new vert for cache: {:?}", key);
                            let norm = if norm_ind == u16::MAX as usize {
                                None
                            } else {
                                norm_ref.map(|v| v[norm_ind])
                            };

                            processed_cache.insert(
                                key,
                                self.transform_vert_data(&pos_ref[v_pos_ind], &norm, bone_ind)?,
                            );
                            errify!(processed_cache.get(&key), GltfError::NoProcessedVert(key))?
                        };

                        let bone_ind = entry.ind;
                        // We put the bone ind here, which is "wrong", but we remap it later
                        vert.bones[0] = bone_ind;
                        // There is only one weight, so it's 1.0
                        vert.weights[0] = 1.0;

                        vert.pos = processed.pos;
                        vert.norm = processed.norm;
                    } else {
                        // Contains an evp1.weights ind
                        let evp1_weights = errify!(
                            self.evp1.weights.get(entry.ind as usize),
                            GltfError::InvalidEvp1Weights(entry.ind)
                        )?;
                        // There will never be more than 4 weights (?)
                        let mut weight_vals = [0.0; 4];
                        let mut bone_inds = [0; 4];

                        for (i, weight) in evp1_weights.weights.iter().enumerate() {
                            weight_vals[i] = weight.weight;
                            // We put the bone ind here, which is "wrong", but we remap it later
                            bone_inds[i] = weight.bone_ind;
                        }

                        vert.bones = bone_inds;
                        vert.weights = weight_vals;

                        let v_pos_ind = v.get(GxAttribute::Position) as usize;
                        let norm_ind = v.get(GxAttribute::Normal) as usize;

                        vert.pos = pos_ref[v_pos_ind];
                        vert.norm = norm_ref.map(|v| v[norm_ind]);
                    }

                    // Process the texcoord channels
                    for chan in 0..8 {
                        let attr =
                            GxAttribute::try_from_primitive(GxAttribute::Tex0 as u8 + chan as u8)
                                .map_err(GltfError::InvalidTexChanForCoords)?;
                        if !s.vertex_descriptor.is_set(attr) {
                            continue;
                        }

                        if let Some(coord) = self.vtx1.get_vert_texcoord(attr, &v) {
                            vert.uvs[chan] = coord.clone();
                            has_tex_chan[chan] = true;
                        } else {
                            // Hopefully this never happens
                            log::warn!("Missing tex{} coord skipped", chan);
                        }
                    }

                    if let Some(color_ref) = color0_ref {
                        if s.vertex_descriptor.is_set(GxAttribute::Color0) {
                            let ind = v.get(GxAttribute::Color0) as usize;
                            vert.colors[0] = color_ref[ind];
                            has_color_chan[0] = true;
                        }
                    }

                    if let Some(color_ref) = color1_ref {
                        if s.vertex_descriptor.is_set(GxAttribute::Color1) {
                            let ind = v.get(GxAttribute::Color1) as usize;
                            vert.colors[1] = color_ref[ind];
                            has_color_chan[1] = true;
                        }
                    }

                    pos_ind += 1;
                    verts.push(vert);
                }
            }
        }

        let (inds, verts) = deduplicate(verts);

        let CompiledVertexInfo {
            pos,
            norms,
            bones,
            weights,
            uvs,
            colors,
        } = CompiledVertexInfo::compile_verts(
            verts,
            norm_ref.is_some(),
            has_tex_chan,
            has_color_chan,
        );

        let pos_count = pos.len();
        let pos_bytes = pos
            .into_iter()
            .map(|v| [v.x.to_le_bytes(), v.y.to_le_bytes(), v.z.to_le_bytes()])
            .flatten()
            .flatten()
            .collect::<Vec<_>>();

        // NOTE: Pos buffer has to be first...
        let pos_buf = builder.add_buffer_view(
            GxAttribute::Position.to_string(),
            pos_bytes,
            Some(BufferViewTarget::Vertex),
            AccessorComponentType::Float,
        );

        let _pos_acc = builder.add_accessor(
            pos_buf,
            GxAttribute::Position.to_string(),
            0,
            pos_count,
            AccessorComponentType::Float,
            AccessorType::Vec3,
        );

        // ...and vert inds have to be second
        let pos_vert_ind_bytes = inds
            .into_iter()
            .map(|s| s.to_le_bytes())
            .flatten()
            .collect::<Vec<_>>();

        let _pos_ind_buf = builder.add_buffer_view(
            format!("{}_INDEX", GxAttribute::Position),
            pos_vert_ind_bytes,
            Some(BufferViewTarget::Index),
            AccessorComponentType::UnsignedInt,
        );

        let norm_count = norms.len();
        if norm_count > 0 {
            let norm_bytes = norms
                .into_iter()
                .map(|v| [v.x.to_le_bytes(), v.y.to_le_bytes(), v.z.to_le_bytes()])
                .flatten()
                .flatten()
                .collect::<Vec<_>>();

            let norm_buf = builder.add_buffer_view(
                GxAttribute::Normal.to_string(),
                norm_bytes,
                None,
                AccessorComponentType::Float,
            );

            let _norm_acc = builder.add_accessor(
                norm_buf,
                GxAttribute::Normal.to_string(),
                0,
                norm_count,
                AccessorComponentType::Float,
                AccessorType::Vec3,
            );
        }

        let weights_count = weights.len();
        let vert_weight_bytes = weights
            .into_iter()
            .map(|s| s.map(|v| v.to_le_bytes()))
            .flatten()
            .flatten()
            .collect::<Vec<_>>();

        let weight_buf = builder.add_buffer_view(
            format!("{}_WEIGHT", GxAttribute::PositionNormalMatrixInd),
            vert_weight_bytes,
            None,
            AccessorComponentType::Float,
        );

        let _vert_weight_acc = builder.add_accessor(
            weight_buf,
            format!("{}_WEIGHT", GxAttribute::PositionNormalMatrixInd),
            0,
            weights_count,
            AccessorComponentType::Float,
            AccessorType::Vec4,
        );

        // We have to remap this so we fill it later
        let vert_bone_ind_bytes = vec![0; bones.len() * 2 * 4];

        let bone_ind_buf = builder.add_buffer_view(
            format!("{}_BONES", GxAttribute::PositionNormalMatrixInd),
            vert_bone_ind_bytes,
            None,
            AccessorComponentType::UnsignedShort,
        );

        let _vert_bone_ind_acc = builder.add_accessor(
            bone_ind_buf,
            format!("{}_BONES", GxAttribute::PositionNormalMatrixInd),
            0,
            bones.len(),
            AccessorComponentType::UnsignedShort,
            AccessorType::Vec4,
        );

        for (chan, uvs) in uvs.into_iter().enumerate() {
            if uvs.len() == 0 {
                continue;
            }

            let attr = GxAttribute::try_from_primitive(GxAttribute::Tex0 as u8 + chan as u8)
                .map_err(GltfError::InvalidTexChanForAccessors)?;

            let count = uvs.len();

            let bytes = uvs
                .into_iter()
                .map(|v| [v.x.to_le_bytes(), v.y.to_le_bytes()])
                .flatten()
                .flatten()
                .collect::<Vec<_>>();

            let buf = builder.add_buffer_view(
                attr.to_string(),
                bytes,
                None,
                AccessorComponentType::Float,
            );

            let _acc = builder.add_accessor(
                buf,
                attr.to_string(),
                0,
                count,
                AccessorComponentType::Float,
                AccessorType::Vec2,
            );
        }

        for (chan, colors) in colors.into_iter().enumerate() {
            if colors.len() == 0 {
                continue;
            }

            let attr = GxAttribute::try_from_primitive(GxAttribute::Color0 as u8 + chan as u8)
                .map_err(GltfError::InvalidColorChan)?;

            let count = colors.len();

            let bytes = colors
                .into_iter()
                .map(|c| {
                    let [r, g, b, a] = c.to_linear().to_f32_array();
                    [
                        r.to_le_bytes(),
                        g.to_le_bytes(),
                        b.to_le_bytes(),
                        a.to_le_bytes(),
                    ]
                })
                .flatten()
                .flatten()
                .collect::<Vec<_>>();

            let buf = builder.add_buffer_view(
                attr.to_string(),
                bytes,
                None,
                AccessorComponentType::Float,
            );

            let _acc = builder.add_accessor(
                buf,
                attr.to_string(),
                0,
                count,
                AccessorComponentType::Float,
                AccessorType::Vec4,
            );
        }

        // return the bones so we can map them later
        Ok(bones)
    }

    /// Transforms some indexed vert data (like positions) against some matrix
    /// so they're in a good spot for export
    fn transform_vert_data(
        &self,
        pos: &Vec3A,
        norm: &Option<Vec3A>,
        bone_ind: usize,
    ) -> Result<ProcessedVertData, GltfError> {
        if self.evp1.matrices.len() > bone_ind {
            let mat = errify!(
                self.evp1.matrices.get(bone_ind),
                GltfError::InvalidEvp1Matrix(bone_ind)
            )?;

            Ok(mat_mul_vert_data(pos, norm, mat))
        } else {
            let data = errify!(
                self.jnt1.bone_data.get(bone_ind),
                GltfError::InvalidJnt1Bone(bone_ind)
            )?;
            let m4 = Mat4::from_scale_rotation_translation(
                data.config.scale,
                data.config.rotation,
                data.config.translate,
            );

            Ok(ProcessedVertData {
                pos: m4.transform_vector3a(pos.clone()),
                norm: norm.map(|n| m4.transform_vector3a(n.clone())),
            })
        }
    }
}
