//! Some TEV info here: https://github.com/crtc-demos/tevsl

use std::io::{BufRead, Read, Seek, SeekFrom, Write};

use glam::{Vec2, Vec3};
use bevy_color::Color;
use num_enum::TryFromPrimitive;
use serde::{Deserialize, Serialize};
use strum::{EnumIter, IntoEnumIterator};

use crate::{
    bytes::{read_f32, read_u16, read_u32, read_u8},
    find_or_insert,
    gc::{
        align::{write_align, AlignType},
        bmd::{
            error::BmdWriteError,
            jnt1::{create_remap_table, read_name_table, write_name_table},
        },
        error::ReadError,
        types::{
            indirect::tex_info::IndirectTexInfo,
            tev::{
                alpha_compare::AlphaCompare, order::TevOrder, stage::TevStage,
                swap_mode::TevSwapMode, swap_mode_table::TevSwapModeTable, CombineAlphaInput,
                CombineColorInput, CompareKind, CompareOp, TevBias, TevOp, TevScale,
            },
            tex::{
                color_channel_info::ColorChannelInfo, gen_info::TexGenInfo,
                matrix_info::TexMatrixInfo, post_gen_info::PostTexGenInfo, AttenuationFn,
                ColorChannelId, ColorSrc, DiffuseFn, LightMask, TexCoordId, TexMapId, TexMatrix,
            },
        },
    },
    get, try_from_repr,
    types::maybe_invalid::MaybeInvalid,
    util::{
        is_default, new_mat4_rows, read_color_i16, read_color_u8, write_color_i16, write_color_u8,
    },
    w, wat, wbool, wraw,
};

const MAT3: u32 = 0x4D415433;

#[derive(Debug, Copy, Clone, PartialEq, EnumIter)]
#[repr(usize)]
enum Mat3Section {
    IndirectTexturingInfo = 0,
    CullingInfo = 1,
    MaterialColors = 2,
    NumColorChannels = 3,
    ColorChannelInfo = 4,
    AmbientColors = 5,
    LightColors = 6,
    NumTexGens = 7,
    TexGenInfo = 8,
    PostTexGenInfo = 9,
    TexMatrixInfo = 10,
    PostTexMatrixInfo = 11,
    TextureRemapTable = 12,
    TevOrderInfo = 13,
    TevColors = 14,
    TevKonstColors = 15,
    NumTevStages = 16,
    TevStageInfo = 17,
    TevSwapModeInfo = 18,
    TevSwapModeTableInfo = 19,
    FogInfo = 20,
    AlphaCompareInfo = 21,
    BlendModeInfo = 22,
    ZModeInfo = 23,
    ZModeIndex = 24,
    Dither = 25,
    NBTScaleInfo = 26,
}

impl Mat3Section {
    /// Length in bytes, useful for counting how many entries are available
    pub fn entry_len(&self) -> u32 {
        match self {
            Mat3Section::IndirectTexturingInfo => 312,
            Mat3Section::CullingInfo => 4,
            Mat3Section::MaterialColors => 4,
            Mat3Section::NumColorChannels => 1,
            Mat3Section::ColorChannelInfo => 8,
            Mat3Section::AmbientColors => 4,
            Mat3Section::LightColors => 4,
            Mat3Section::NumTexGens => 1,
            Mat3Section::TexGenInfo => 4,
            Mat3Section::PostTexGenInfo => 8,
            Mat3Section::TexMatrixInfo => 100,
            Mat3Section::PostTexMatrixInfo => 100,
            Mat3Section::TextureRemapTable => 2,
            Mat3Section::TevOrderInfo => 4,
            Mat3Section::TevColors => 8,
            Mat3Section::TevKonstColors => 4,
            Mat3Section::NumTevStages => 1,
            Mat3Section::TevStageInfo => 20,
            Mat3Section::TevSwapModeInfo => 4,
            Mat3Section::TevSwapModeTableInfo => 4,
            Mat3Section::FogInfo => 44,
            Mat3Section::AlphaCompareInfo => 8,
            Mat3Section::BlendModeInfo => 4,
            Mat3Section::ZModeInfo => 4,
            Mat3Section::ZModeIndex => 1,
            Mat3Section::Dither => 1,
            Mat3Section::NBTScaleInfo => 16,
        }
    }
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum Culling {
    None = 0,
    Frontface = 1,
    #[default]
    Backface = 2,
    All = 3,
}

#[derive(Debug, Copy, Clone)]
pub struct SectionInfo {
    pub offset: u32,
    pub size: u32,
}

impl SectionInfo {
    pub fn new() -> Self {
        Self { offset: 0, size: 0 }
    }
}

// todo: understand these fields
#[derive(Debug, Copy, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct FogInfo {
    // todo: what are the kinds?
    //  0 = normal
    //  2 = stuff changed
    pub kind: u8,
    pub enabled: bool,
    pub center: u16,
    pub start: f32,
    pub end: f32,
    pub near: f32,
    pub far: f32,
    pub color: Color,
    /// Fog effects become distorted as the camera's view tends toward either side of the viewport.
    /// These fixed-point u4.8 shorts adjust for this distortion, applying symmetrically from the
    /// Center x-coordinate defined above to the edge of the camera's view.
    /// By the way, u4.8 fixed point ints can range from 0 to just about 16.
    pub range_adjustments: [f32; 10],
}

impl FogInfo {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let kind = read_u8(buf)?;
        let enabled = read_u8(buf)? != 0;
        let center = read_u16(buf)?;
        let start = read_f32(buf)?;
        let end = read_f32(buf)?;
        let near = read_f32(buf)?;
        let far = read_f32(buf)?;
        let color = read_color_u8(buf)?;

        let mut range_adjustments = [0.0; 10];
        for i in 0..range_adjustments.len() {
            let v = read_u16(buf)?;
            // todo why 256
            range_adjustments[i] = v as f32 * (1.0 / 256.0);
        }

        Ok(Self {
            kind,
            enabled,
            center,
            start,
            end,
            near,
            far,
            color,
            range_adjustments,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, self.kind)?;
        wbool!(w, self.enabled)?;
        w!(w, self.center)?;
        w!(w, self.start)?;
        w!(w, self.end)?;
        w!(w, self.near)?;
        w!(w, self.far)?;
        write_color_u8(w, &self.color)?;

        for r in self.range_adjustments.iter() {
            let r = r * 256.001;
            w!(w, r as u16)?;
        }

        Ok(())
    }
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum BlendKind {
    /// Write input directly to EFB
    None = 0,
    /// Blend using blending equation
    #[default]
    Blend = 1,
    /// Blend using bitwise operation
    Logic = 2,
    /// Input subtracts from existing pixel
    Subtract = 3,
}

/// Each pixel (source or destination) is multiplied by any of these controls.
#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum BlendControl {
    #[default]
    Zero = 0,
    One = 1,
    /// This is both SrcColor and DstColor depending on context
    Color = 2,
    /// This is both OneMinusSrcColor and OneMinusDstColor depending on context
    OneMinusColor = 3,
    SrcAlpha = 4,
    OneMinusSrcAlpha = 5,
    /// framebuffer alpha
    DstAlpha = 6,
    OneMinusDstAlpha = 7,
}

/// Destination (dst) acquires the value of one of these operations, given in C syntax.
#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum LogicOp {
    /// 0
    Clear = 0,
    /// src & dst
    AND = 1,
    /// src & ~dst
    RevAND = 2,
    /// src
    #[default]
    Copy = 3,
    /// ~src & dst
    InvAND = 4,
    /// dst
    NoOp = 5,
    /// src ^ dst
    XOR = 6,
    /// src | dst
    OR = 7,
    /// ~(src | dst)
    NOR = 8,
    /// ~(src ^ dst)
    Equiv = 9,
    /// ~dst
    Inv = 10,
    /// src | ~dst
    RevOR = 11,
    /// ~src
    InvCopy = 12,
    /// ~src | dst
    InvOR = 13,
    /// ~(src & dst)
    NAND = 14,
    /// 1
    Set = 15,
}

/// Determines how the source image, generated by the graphics processor, is blended with the
/// Embedded Frame Buffer (EFB).
///
/// When BlendKind is set to `None`, the source data is written directly to the EFB. When set to
/// `Blend`, the source color and EFB pixels are blended using the following equation:
///
/// `dst_pix_clr = src_pix_clr * src_fact + dst_pix_clr * dst_fact`
///
/// The BlendControls `DSTALPHA / INVDSTALPHA` can be used only when the EFB has RGBA6_Z24 as the
/// pixel format. If the pixel format is RGBA6_Z24 then the `src_fact` and `dst_fact` are also
/// applied to the alpha channel.
///
/// When BlendKind is set to `Logic`, the source and EFB pixels are blended using logical bitwise
/// operations. When set to `Subtract`, the destination pixel is computed as follows:
///
/// `dst_pix_clr = dst_pix_clr - src_pix_clr [clamped to zero]`
///
/// Note that src_fact and dst_fact are not part of this equation.
#[derive(Debug, Copy, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct BlendMode {
    pub kind: BlendKind,
    pub src_fact: BlendControl,
    pub dst_fact: BlendControl,
    pub op: LogicOp,
}

impl BlendMode {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let kind = read_u8(buf)?;
        let src_fact = read_u8(buf)?;
        let dst_fact = read_u8(buf)?;
        let op = read_u8(buf)?;

        Ok(Self {
            kind: try_from_repr!(kind => BlendKind)?,
            src_fact: try_from_repr!(src_fact => BlendControl)?,
            dst_fact: try_from_repr!(dst_fact => BlendControl)?,
            op: try_from_repr!(op => LogicOp)?,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, self.kind as u8)?;
        w!(w, self.src_fact as u8)?;
        w!(w, self.dst_fact as u8)?;
        w!(w, self.op as u8)?;

        Ok(())
    }
}

/// Sets the Z-buffer compare mode. The result of the Z compare is used to conditionally write color
/// values to the Embedded Frame Buffer (EFB).
#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct ZMode {
    /// Enables comparisons with source and destination Z values. When set to false,
    /// Z buffering is disabled and the Z buffer is not updated.
    pub enabled: bool,
    /// Determines the comparison that is performed.
    /// The newely rasterized Z value is on the left while the value from the Z buffer is on the
    /// right. If the result of the comparison is false, the newly rasterized pixel is discarded.
    pub comp: CompareKind,
    /// Determines whether or not the Z buffer is updated with the new Z value after a comparison
    /// is performed. This parameter also affects whether the Z buffer is cleared during copy
    /// operations. Z-buffer updates can be disabled, but compares may still be enabled.
    pub update_enabled: bool,
}

impl ZMode {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let enabled = read_u8(buf)? != 0;
        let comp = read_u8(buf)?;
        let update_enabled = read_u8(buf)? != 0;

        // Skip padding
        buf.seek(SeekFrom::Current(1))?;

        Ok(Self {
            enabled,
            comp: try_from_repr!(comp => CompareKind)?,
            update_enabled,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        wbool!(w, self.enabled)?;
        w!(w, self.comp as u8)?;
        wbool!(w, self.update_enabled)?;

        w!(w, 0xFFu8)?;

        Ok(())
    }
}

impl Default for ZMode {
    fn default() -> Self {
        Self {
            enabled: true,
            comp: CompareKind::Le,
            update_enabled: true,
        }
    }
}

#[derive(Debug, Copy, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct NbtScale {
    pub unk: u8,
    pub scale: Vec3,
}

impl NbtScale {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let unk = read_u8(buf)?;

        // Skip padding
        buf.seek(SeekFrom::Current(3))?;

        let scale = Vec3::new(read_f32(buf)?, read_f32(buf)?, read_f32(buf)?);

        Ok(Self { unk, scale })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, self.unk)?;

        wraw!(w, [0xFF, 0xFF, 0xFF])?;

        w!(w, self.scale.x)?;
        w!(w, self.scale.y)?;
        w!(w, self.scale.z)?;

        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct Mat3 {
    pub indirect_texes: Vec<IndirectTexInfo>,
    pub hard_params: HardMaterialParams,
    pub materials: Vec<Material>,
    pub len: u32,
}

impl Mat3 {
    pub fn new<R: Read + Seek + BufRead>(buf: &mut R, mat3_offset: u32) -> Result<Self, ReadError> {
        let mat3 = read_u32(buf)?;
        // mat3
        if mat3 != MAT3 {
            return Err(ReadError::ReadCheck("MAT3"));
        }

        let mat3_len = read_u32(buf)?;
        // num of materials
        let mat_count = read_u16(buf)? as usize;

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        let material_data_offset = read_u32(buf)?;
        let remap_table_offset = read_u32(buf)?;
        let name_table_offset = read_u32(buf)?;

        let mut section_infos = [SectionInfo::new(); 27];
        // Note: If the first element offset is 0, we are going to still set the size.
        // this is okay because we check offset == 0 before we use the size
        let mut last_sect = 0;

        for (i, section) in Mat3Section::iter().enumerate() {
            let offset = read_u32(buf)?;

            // The last section is based on the len of this block
            if section == Mat3Section::NBTScaleInfo {
                if offset == 0 {
                    section_infos[last_sect].size = mat3_len - section_infos[last_sect].offset;
                } else {
                    section_infos[i].size = mat3_len - offset;
                }
            }

            if offset == 0 {
                continue;
            }

            section_infos[i].offset = offset;
            section_infos[last_sect].size = offset - section_infos[last_sect].offset;
            last_sect = i;
        }

        // todo: it would probably be cleaner to go through each material and figure out
        //  indexes to the data from there

        let mut indirect_texes = Vec::with_capacity(mat_count);

        let mut cull_modes = Vec::with_capacity(mat_count);
        let mut mat_colors = Vec::with_capacity(mat_count * 2);
        let mut ambient_colors = Vec::with_capacity(mat_count * 2);
        let mut light_colors = Vec::with_capacity(mat_count * 8);
        let mut num_color_channels = Vec::with_capacity(mat_count);
        let mut color_channel_infos = Vec::with_capacity(mat_count * 4);
        let mut num_tex_gens = Vec::with_capacity(mat_count);
        let mut tex_gen_infos = Vec::with_capacity(mat_count * 8);
        let mut post_tex_gen_infos = Vec::with_capacity(mat_count * 8);
        let mut tex_matrix_infos = Vec::with_capacity(mat_count * 10);
        let mut post_tex_matrix_infos = Vec::with_capacity(mat_count * 20);
        let mut tex_remaps = Vec::with_capacity(mat_count * 8);
        let mut tev_orders = Vec::with_capacity(mat_count * 16);
        let mut tev_colors = Vec::with_capacity(mat_count * 4);
        let mut tev_konst_colors = Vec::with_capacity(mat_count * 4);
        let mut num_tev_stages = Vec::with_capacity(mat_count);
        let mut tev_stage_infos = Vec::with_capacity(mat_count * 16);
        let mut tev_swap_modes = Vec::with_capacity(mat_count * 16);
        let mut tev_swap_mode_tables = Vec::with_capacity(mat_count * 4);
        let mut fog_infos = Vec::with_capacity(mat_count);
        let mut alpha_compares = Vec::with_capacity(mat_count);
        let mut blend_modes = Vec::with_capacity(mat_count);
        let mut z_modes = Vec::with_capacity(mat_count);
        let mut z_mode_early = Vec::with_capacity(mat_count);
        let mut dither = Vec::with_capacity(mat_count);
        let mut nbt_scales = Vec::with_capacity(mat_count);

        for (i, section) in Mat3Section::iter().enumerate() {
            let info = section_infos[i];
            if info.offset == 0 || info.size == 0 {
                continue;
            }

            buf.seek(SeekFrom::Start((info.offset + mat3_offset) as u64))?;

            let range = 0..(info.size / section.entry_len());

            match section {
                Mat3Section::IndirectTexturingInfo => {
                    for _ in range {
                        indirect_texes.push(IndirectTexInfo::read(buf)?);
                    }
                }
                Mat3Section::CullingInfo => {
                    for _ in range {
                        let culling = read_u32(buf)? as u8;
                        cull_modes.push(try_from_repr!(culling => Culling)?);
                    }
                }
                Mat3Section::MaterialColors => {
                    for _ in range {
                        mat_colors.push(read_color_u8(buf)?);
                    }
                }
                Mat3Section::NumColorChannels => {
                    for _ in range {
                        // should be 0, 1, 2
                        let n = read_u8(buf)?;

                        // This should never be 84 and should always represent the padding ('T')
                        if n == 84 {
                            break;
                        }

                        num_color_channels.push(n);
                    }
                }
                Mat3Section::ColorChannelInfo => {
                    for _ in range {
                        color_channel_infos.push(ColorChannelInfo::read(buf)?);
                    }
                }
                Mat3Section::AmbientColors => {
                    for _ in range {
                        ambient_colors.push(read_color_u8(buf)?);
                    }
                }
                Mat3Section::LightColors => {
                    for _ in range {
                        light_colors.push(read_color_u8(buf)?);
                    }
                }
                Mat3Section::NumTexGens => {
                    // todo need to check for padding?
                    for _ in range {
                        num_tex_gens.push(read_u8(buf)?);
                    }
                }
                Mat3Section::TexGenInfo => {
                    for _ in range {
                        tex_gen_infos.push(TexGenInfo::read(buf)?);
                    }
                }
                Mat3Section::PostTexGenInfo => {
                    for _ in range {
                        post_tex_gen_infos.push(PostTexGenInfo::read(buf)?);
                    }
                }
                Mat3Section::TexMatrixInfo => {
                    for _ in range {
                        tex_matrix_infos.push(TexMatrixInfo::read(buf)?);
                    }
                }
                Mat3Section::PostTexMatrixInfo => {
                    for _ in range {
                        post_tex_matrix_infos.push(TexMatrixInfo::read(buf)?);
                    }
                }
                Mat3Section::TextureRemapTable => {
                    for _ in range {
                        tex_remaps.push(read_u16(buf)?);
                    }
                }
                Mat3Section::TevOrderInfo => {
                    for _ in range {
                        tev_orders.push(TevOrder::read(buf)?);
                    }
                }
                Mat3Section::TevColors => {
                    for _ in range {
                        // todo: what
                        tev_colors.push(read_color_i16(buf)?);
                    }
                }
                Mat3Section::TevKonstColors => {
                    for _ in range {
                        tev_konst_colors.push(read_color_u8(buf)?);
                    }
                }
                Mat3Section::NumTevStages => {
                    // todo need to check for padding?
                    for _ in range {
                        num_tev_stages.push(read_u8(buf)?);
                    }
                }
                Mat3Section::TevStageInfo => {
                    for _ in range {
                        tev_stage_infos.push(TevStage::read(buf)?);
                    }
                }
                Mat3Section::TevSwapModeInfo => {
                    for _ in range {
                        tev_swap_modes.push(TevSwapMode::read(buf)?);
                    }
                }
                Mat3Section::TevSwapModeTableInfo => {
                    for _ in range {
                        tev_swap_mode_tables.push(TevSwapModeTable::read(buf)?);
                    }
                }
                Mat3Section::FogInfo => {
                    for _ in range {
                        fog_infos.push(FogInfo::read(buf)?);
                    }
                }
                Mat3Section::AlphaCompareInfo => {
                    for _ in range {
                        alpha_compares.push(AlphaCompare::read(buf)?);
                    }
                }
                Mat3Section::BlendModeInfo => {
                    for _ in range {
                        blend_modes.push(BlendMode::read(buf)?);
                    }
                }
                Mat3Section::ZModeInfo => {
                    for _ in range {
                        z_modes.push(ZMode::read(buf)?);
                    }
                }
                Mat3Section::ZModeIndex => {
                    for _ in range {
                        let zcomp = read_u8(buf)?;

                        // This should never be 84 and should always represent the padding ('T')
                        if zcomp == 84 {
                            break;
                        }

                        // if this is ever >1, this isn't a bool it's probably an ind into the
                        // z modes?
                        if zcomp > 1 {
                            log::warn!(
                                "Found strange zcomp value more than 1, this might indicate a problem"
                            );
                        }

                        z_mode_early.push(zcomp != 0);
                    }
                }
                Mat3Section::Dither => {
                    // Enables or disables dithering when writing the Embedded Frame Buffer (EFB).
                    //
                    // A 4x4 Bayer matrix is used for dithering.
                    //
                    // Only valid when the pixel format is either RGBA6_Z24 or RGB565_Z16.
                    //
                    // Dithering should probably be turned off if you are planning on using the
                    // result of rendering for comparisons (e.g. outline rendering algorithm that
                    // writes IDs to the alpha channel, copies the alpha channel to a texture, and
                    // later compares the texture in the TEV).
                    for _ in range {
                        let dith = read_u8(buf)?;

                        // This should never be 84 and should always represent the padding ('T')
                        if dith == 84 {
                            break;
                        }

                        // should always be 0 or 1 or something is silly
                        if dith > 1 {
                            log::warn!(
                                "Found strange dither value of {}, this might indicate a problem",
                                dith
                            );
                        }

                        dither.push(dith != 0);
                    }
                }
                Mat3Section::NBTScaleInfo => {
                    for _ in range {
                        let nbt = NbtScale::read(buf)?;

                        // This should never be 84 and should always represent the padding ('T')
                        if nbt.unk == 84 {
                            break;
                        }

                        nbt_scales.push(nbt);
                    }
                }
            }
        }

        let hard_params = HardMaterialParams {
            cull_modes,
            tex_gen_infos,
            tev_swap_modes,
            z_mode_early,
            dither,
        };

        let params = MaterialParams {
            mat_colors,
            ambient_colors,
            light_colors,
            num_color_channels,
            color_channel_infos,
            num_tex_gens,
            post_tex_gen_infos,
            tex_matrix_infos,
            post_tex_matrix_infos,
            tex_remaps,
            tev_orders,
            tev_colors,
            tev_konst_colors,
            num_tev_stages,
            tev_stage_infos,
            tev_swap_mode_tables,
            fog_infos,
            alpha_compares,
            blend_modes,
            z_modes,
            nbt_scales,
        };

        let names = read_name_table(buf, (name_table_offset + mat3_offset) as u64)?;

        buf.seek(SeekFrom::Start((remap_table_offset + mat3_offset) as u64))?;

        // remaps seem to act as a form of compression,
        // if the settings are the same (except the names)
        let mut remaps = Vec::with_capacity(mat_count);
        for _ in 0..mat_count {
            remaps.push(read_u16(buf)?);
        }

        let mut materials = Vec::with_capacity(mat_count);

        for i in 0..mat_count {
            buf.seek(SeekFrom::Start(
                (mat3_offset + material_data_offset) as u64 + (remaps[i] as u64 * 0x14C),
            ))?;
            let name = names[i].clone();
            materials.push(Material::read(buf, name, &params)?);
        }

        Ok(Self {
            indirect_texes,
            hard_params,
            materials,
            len: mat3_len,
        })
    }
    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        let start = w.stream_position()?;
        w!(w, MAT3)?;

        let length_offset = w.stream_position()?;
        w!(w, 0u32)?;

        w!(w, self.materials.len() as u16)?;

        w!(w, 0xFFFFu16)?;

        let material_data_offset = w.stream_position()?;
        w!(w, 0u32)?;
        let remap_table_offset = material_data_offset + 4;
        w!(w, 0u32)?;
        let name_table_offset = remap_table_offset + 4;
        w!(w, 0u32)?;

        let mut wrote_offsets = [0u32; 27];
        let mut offset_positions = [0u64; 27];

        let sect_offset = name_table_offset + 4;

        for (i, _) in Mat3Section::iter().enumerate() {
            offset_positions[i] = sect_offset + (4 * i as u64);
            w!(w, 0u32)?;
        }

        let items = self.materials.iter().map(|m| &m.info).collect::<Vec<_>>();
        let (remaps, dedup) = create_remap_table(items);

        // todo might be able to gain some time by calcing these values once
        let mut params = MaterialParams {
            mat_colors: Vec::with_capacity(dedup.len() * 2),
            ambient_colors: Vec::with_capacity(dedup.len() * 2),
            light_colors: Vec::with_capacity(dedup.len() * 8),
            num_color_channels: Vec::with_capacity(dedup.len()),
            color_channel_infos: Vec::with_capacity(dedup.len() * 4),
            num_tex_gens: Vec::with_capacity(dedup.len()),
            post_tex_gen_infos: Vec::with_capacity(dedup.len() * 8),
            tex_matrix_infos: Vec::with_capacity(dedup.len() * 10),
            post_tex_matrix_infos: Vec::with_capacity(dedup.len() * 20),
            tex_remaps: Vec::with_capacity(dedup.len() * 8),
            tev_orders: Vec::with_capacity(dedup.len() * 16),
            tev_colors: Vec::with_capacity(dedup.len() * 4),
            tev_konst_colors: Vec::with_capacity(dedup.len() * 4),
            num_tev_stages: Vec::with_capacity(dedup.len()),
            tev_stage_infos: Vec::with_capacity(dedup.len() * 16),
            tev_swap_mode_tables: Vec::with_capacity(dedup.len() * 4),
            fog_infos: Vec::with_capacity(dedup.len()),
            alpha_compares: Vec::with_capacity(dedup.len()),
            blend_modes: Vec::with_capacity(dedup.len()),
            z_modes: Vec::with_capacity(dedup.len()),
            nbt_scales: Vec::with_capacity(dedup.len()),
        };

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, material_data_offset);

        for info in dedup {
            info.write_bmd(w, &mut params)?;
            write_align(w, 4, AlignType::String)?;
        }

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, remap_table_offset);

        for r in remaps {
            w!(w, r)?;
        }

        // This seems right?
        write_align(w, 4, AlignType::String)?;

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, name_table_offset);

        let names = self
            .materials
            .iter()
            .map(|m| m.name.clone())
            .collect::<Vec<_>>();
        write_name_table(w, names)?;

        // This seems right?
        write_align(w, 4, AlignType::String)?;

        // todo maybe macro
        for (i, section) in Mat3Section::iter().enumerate() {
            match section {
                Mat3Section::IndirectTexturingInfo => {
                    if self.indirect_texes.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for tex in self.indirect_texes.iter() {
                            tex.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::CullingInfo => {
                    if self.hard_params.cull_modes.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in self.hard_params.cull_modes.iter() {
                            w!(w, *info as u8 as u32)?;
                        }
                    }
                }
                Mat3Section::MaterialColors => {
                    if params.mat_colors.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for color in params.mat_colors.iter() {
                            write_color_u8(w, color)?;
                        }
                    }
                }
                Mat3Section::NumColorChannels => {
                    if params.num_color_channels.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for chans in params.num_color_channels.iter() {
                            w!(w, *chans)?;
                        }
                    }
                }
                Mat3Section::ColorChannelInfo => {
                    if params.color_channel_infos.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in params.color_channel_infos.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::AmbientColors => {
                    if params.ambient_colors.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for color in params.ambient_colors.iter() {
                            write_color_u8(w, color)?;
                        }
                    }
                }
                Mat3Section::LightColors => {
                    if params.light_colors.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for color in params.light_colors.iter() {
                            write_color_u8(w, color)?;
                        }
                    }
                }
                Mat3Section::NumTexGens => {
                    if params.num_tex_gens.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for num in params.num_tex_gens.iter() {
                            w!(w, *num)?;
                        }
                    }
                }
                Mat3Section::TexGenInfo => {
                    if self.hard_params.tex_gen_infos.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in self.hard_params.tex_gen_infos.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::PostTexGenInfo => {
                    if params.post_tex_gen_infos.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in params.post_tex_gen_infos.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::TexMatrixInfo => {
                    if params.tex_matrix_infos.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in params.tex_matrix_infos.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::PostTexMatrixInfo => {
                    if params.post_tex_matrix_infos.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in params.post_tex_matrix_infos.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::TextureRemapTable => {
                    if params.tex_remaps.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for ind in params.tex_remaps.iter() {
                            w!(w, *ind)?;
                        }
                    }
                }
                Mat3Section::TevOrderInfo => {
                    if params.tev_orders.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in params.tev_orders.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::TevColors => {
                    if params.tev_colors.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for color in params.tev_colors.iter() {
                            write_color_i16(w, color)?;
                        }
                    }
                }
                Mat3Section::TevKonstColors => {
                    if params.tev_konst_colors.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for color in params.tev_konst_colors.iter() {
                            write_color_u8(w, color)?;
                        }
                    }
                }
                Mat3Section::NumTevStages => {
                    if params.num_tev_stages.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for num in params.num_tev_stages.iter() {
                            w!(w, *num)?;
                        }
                    }
                }
                Mat3Section::TevStageInfo => {
                    if params.tev_stage_infos.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in params.tev_stage_infos.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::TevSwapModeInfo => {
                    if self.hard_params.tev_swap_modes.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in self.hard_params.tev_swap_modes.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::TevSwapModeTableInfo => {
                    if params.tev_swap_mode_tables.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in params.tev_swap_mode_tables.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::FogInfo => {
                    if params.fog_infos.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in params.fog_infos.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::AlphaCompareInfo => {
                    if params.alpha_compares.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in params.alpha_compares.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::BlendModeInfo => {
                    if params.blend_modes.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in params.blend_modes.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::ZModeInfo => {
                    if params.z_modes.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in params.z_modes.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
                Mat3Section::ZModeIndex => {
                    if self.hard_params.z_mode_early.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for early in self.hard_params.z_mode_early.iter() {
                            wbool!(w, *early)?;
                        }
                    }
                }
                Mat3Section::Dither => {
                    if self.hard_params.dither.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for dither in self.hard_params.dither.iter() {
                            wbool!(w, *dither)?;
                        }
                    }
                }
                Mat3Section::NBTScaleInfo => {
                    if params.nbt_scales.len() > 0 {
                        let here = w.stream_position()?;
                        let offset = (here - start) as u32;
                        wrote_offsets[i] = offset;
                        wat!(w, offset, offset_positions[i]);

                        for info in params.nbt_scales.iter() {
                            info.write_bmd(w)?;
                        }
                    }
                }
            }
            // It seems like all of these sections may have to be rounded to 4 bytes,
            // though I think only some sections are ever not 4 byte rounded naturally
            write_align(w, 4, AlignType::String)?;
        }

        // Fix offset that is 0 to be equal to the next offset (match original file format)
        if wrote_offsets[6] == 0 {
            wat!(w, wrote_offsets[7], offset_positions[6]);
        }

        write_align(w, 32, AlignType::String)?;

        let len = (w.stream_position()? - start) as u32;
        wat!(w, len, length_offset);

        Ok(())
    }
}

/// Material params that have been seen being stored regardless of if they are used
/// by materials or not.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct HardMaterialParams {
    pub cull_modes: Vec<Culling>,

    pub tex_gen_infos: Vec<TexGenInfo>,

    pub tev_swap_modes: Vec<TevSwapMode>,

    pub z_mode_early: Vec<bool>,

    pub dither: Vec<bool>,
}

#[derive(Debug)]
pub struct MaterialParams {
    pub mat_colors: Vec<Color>,
    pub ambient_colors: Vec<Color>,
    pub light_colors: Vec<Color>,
    pub num_color_channels: Vec<u8>,
    pub color_channel_infos: Vec<ColorChannelInfo>,

    pub num_tex_gens: Vec<u8>,
    pub post_tex_gen_infos: Vec<PostTexGenInfo>,

    pub tex_matrix_infos: Vec<TexMatrixInfo>,
    pub post_tex_matrix_infos: Vec<TexMatrixInfo>,

    pub tex_remaps: Vec<u16>,

    pub tev_orders: Vec<TevOrder>,
    pub tev_colors: Vec<Color>,
    pub tev_konst_colors: Vec<Color>,

    pub num_tev_stages: Vec<u8>,
    pub tev_stage_infos: Vec<TevStage>,
    pub tev_swap_mode_tables: Vec<TevSwapModeTable>,

    pub fog_infos: Vec<FogInfo>,
    pub alpha_compares: Vec<AlphaCompare>,
    pub blend_modes: Vec<BlendMode>,
    pub z_modes: Vec<ZMode>,
    pub nbt_scales: Vec<NbtScale>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Material {
    #[serde(default, skip_serializing)]
    pub name: String,
    #[serde(flatten)]
    pub info: MaterialInfo,
}

impl Material {
    /// Advances position
    pub fn read<R: Read + Seek>(
        buf: &mut R,
        name: String,
        params: &MaterialParams,
    ) -> Result<Self, ReadError> {
        // having to clone all this stuff is kinda rough

        let flags = read_u8(buf)?;
        let cull_mode = read_u8(buf)? as usize;

        let num_color_chan_ind = read_u8(buf)? as usize;
        let num_color_chans = get!(params.num_color_channels, num_color_chan_ind)?.clone();

        let num_tex_gens_ind = read_u8(buf)? as usize;
        let num_tex_gens = get!(params.num_tex_gens, num_tex_gens_ind)?.clone();

        let num_tev_stages_ind = read_u8(buf)? as usize;
        let num_tev_stages = get!(params.num_tev_stages, num_tev_stages_ind)?.clone();

        let early_z_compare = read_u8(buf)? as usize;

        let z_mode_ind = read_u8(buf)? as usize;
        let z_mode = get!(params.z_modes, z_mode_ind)?.clone();

        let dither_ind = read_u8(buf)? as usize;
        // Still error if ind is weird but not null
        let dither = if dither_ind == 0xFF {
            None
        } else {
            Some(dither_ind)
        };

        let mut mat_colors = [None; 2];

        for i in 0..mat_colors.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            let color = get!(params.mat_colors, ind)?.clone();

            mat_colors[i] = Some(color);
        }

        let mut color_channel_infos = [MaybeInvalid::None; 4];

        for i in 0..color_channel_infos.len() {
            let ind = read_u16(buf)? as usize;

            // The entry will be None (default)
            if ind == 0xFFFF {
                continue;
            }

            let info = get!(params.color_channel_infos, ind).cloned().ok();

            if info.is_none() {
                log::warn!(
                    "Missing color channel info at {}@{}/{} (our bug or theirs?)",
                    ind,
                    i,
                    params.color_channel_infos.len() as i32 - 1
                );
            }

            color_channel_infos[i] = MaybeInvalid::none_as_invalid(info);
        }

        let mut ambient_colors = [MaybeInvalid::None; 2];

        for i in 0..ambient_colors.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            let color = get!(params.ambient_colors, ind).cloned().ok();

            if color.is_none() {
                log::warn!(
                    "Missing ambient colors at {}@{}/{} (our bug or theirs?)",
                    ind,
                    i,
                    params.ambient_colors.len() as i32 - 1
                );
            }

            ambient_colors[i] = MaybeInvalid::none_as_invalid(color);
        }

        let mut light_colors = [None; 8];

        for i in 0..light_colors.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            let color = get!(params.light_colors, ind)?.clone();

            light_colors[i] = Some(color);
        }

        let mut tex_gen_infos = [None; 8];

        for i in 0..tex_gen_infos.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            tex_gen_infos[i] = Some(ind);
        }

        let mut post_tex_gen_infos = [None; 8];

        for i in 0..post_tex_gen_infos.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            let info = get!(params.post_tex_gen_infos, ind)?.clone();

            post_tex_gen_infos[i] = Some(info);
        }

        let mut tex_matrix_infos = [None; 10];

        for i in 0..tex_matrix_infos.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            let info = get!(params.tex_matrix_infos, ind)?.clone();

            tex_matrix_infos[i] = Some(info);
        }

        let mut post_tex_matrix_infos = [None; 20];

        for i in 0..post_tex_matrix_infos.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            let info = get!(params.post_tex_matrix_infos, ind)?.clone();

            post_tex_matrix_infos[i] = Some(info);
        }

        let mut tex_inds = [None; 8];

        for i in 0..tex_inds.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            let tex_ind = get!(params.tex_remaps, ind)?.clone();

            tex_inds[i] = Some(tex_ind);
        }

        let mut tev_konst_colors = [None; 4];

        for i in 0..tev_konst_colors.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            let color = get!(params.tev_konst_colors, ind)?.clone();

            tev_konst_colors[i] = Some(color);
        }

        let mut tev_konst_color_sels = [0xFF; 16];

        for i in 0..tev_konst_color_sels.len() {
            let ind = read_u8(buf)?;

            tev_konst_color_sels[i] = ind;
        }

        let mut tev_konst_alpha_sels = [0xFF; 16];

        for i in 0..tev_konst_alpha_sels.len() {
            let ind = read_u8(buf)?;

            tev_konst_alpha_sels[i] = ind;
        }

        let mut tev_orders = [None; 16];

        for i in 0..tev_orders.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            let info = get!(params.tev_orders, ind)?.clone();

            tev_orders[i] = Some(info);
        }

        let mut tev_colors = [None; 4];

        for i in 0..tev_colors.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            let color = get!(params.tev_colors, ind)?.clone();

            tev_colors[i] = Some(color);
        }

        let mut tev_stage_infos = [None; 16];

        for i in 0..tev_stage_infos.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            let info = get!(params.tev_stage_infos, ind)?.clone();

            tev_stage_infos[i] = Some(info);
        }

        let mut tev_swap_modes = [None; 16];

        for i in 0..tev_swap_modes.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            tev_swap_modes[i] = Some(ind);
        }

        let mut tev_swap_mode_tables = [None; 4];

        for i in 0..tev_swap_mode_tables.len() {
            let ind = read_u16(buf)? as usize;

            if ind == 0xFFFF {
                continue;
            }

            let table = get!(params.tev_swap_mode_tables, ind)?.clone();

            tev_swap_mode_tables[i] = Some(table);
        }

        let mut unk_inds = [0xFFFF; 12];

        for i in 0..unk_inds.len() {
            let ind = read_u16(buf)?;

            unk_inds[i] = ind;
        }

        let fog_info_ind = read_u16(buf)? as usize;
        let fog_info = if fog_info_ind == 0xFFFF {
            None
        } else {
            Some(get!(params.fog_infos, fog_info_ind)?.clone())
        };

        let alpha_comp_ind = read_u16(buf)? as usize;
        let alpha_compare = get!(params.alpha_compares, alpha_comp_ind)?.clone();

        let blend_mode_ind = read_u16(buf)? as usize;
        let blend_mode = get!(params.blend_modes, blend_mode_ind)?.clone();

        let nbt_scale_ind = read_u16(buf)? as usize;
        let nbt_scale = if nbt_scale_ind == 0xFFFF {
            None
        } else {
            Some(get!(params.nbt_scales, nbt_scale_ind)?.clone())
        };

        Ok(Self {
            name,
            info: MaterialInfo {
                flags,
                cull_mode,
                num_color_chans,
                num_tex_gens,
                num_tev_stages,
                early_z_compare,
                z_mode,
                dither,
                mat_colors,
                color_channel_infos,
                ambient_colors,
                light_colors,
                tex_gen_infos,
                post_tex_gen_infos,
                tex_matrix_infos,
                post_tex_matrix_infos,
                tex_inds,
                tev_konst_colors,
                tev_konst_color_sels,
                tev_konst_alpha_sels,
                tev_orders,
                tev_colors,
                tev_stage_infos,
                tev_swap_modes,
                tev_swap_mode_tables,
                unk_inds,
                fog_info,
                alpha_compare,
                blend_mode,
                nbt_scale,
            },
        })
    }

    /// Sourced from SuperBMD simpleshading
    pub fn simple_material(name: String, hard_params: &mut HardMaterialParams) -> Self {
        let cull_pos = if let Some(pos) = hard_params
            .cull_modes
            .iter()
            .position(|v| v == &Culling::None)
        {
            pos
        } else {
            let pos = hard_params.cull_modes.len();
            hard_params.cull_modes.push(Culling::None);
            pos
        };

        let early_cmp_pos =
            if let Some(pos) = hard_params.z_mode_early.iter().position(|v| v == &false) {
                pos
            } else {
                let pos = hard_params.z_mode_early.len();
                hard_params.z_mode_early.push(false);
                pos
            };

        let dither_pos = if let Some(pos) = hard_params.dither.iter().position(|v| v == &false) {
            pos
        } else {
            let pos = hard_params.dither.len();
            hard_params.dither.push(false);
            pos
        };

        let texgen = TexGenInfo {
            kind: Default::default(),
            src: Default::default(),
            matrix: TexMatrix::Identity,
        };

        let texgen_pos =
            if let Some(pos) = hard_params.tex_gen_infos.iter().position(|v| v == &texgen) {
                pos
            } else {
                let pos = hard_params.tex_gen_infos.len();
                hard_params.tex_gen_infos.push(texgen);
                pos
            };

        let swap = TevSwapMode {
            ras_sel: 0,
            tex_sel: 0,
        };

        let swap_pos = if let Some(pos) = hard_params.tev_swap_modes.iter().position(|v| v == &swap)
        {
            pos
        } else {
            let pos = hard_params.tev_swap_modes.len();
            hard_params.tev_swap_modes.push(swap);
            pos
        };

        Self {
            name,
            info: MaterialInfo {
                flags: 1,
                cull_mode: cull_pos,
                num_color_chans: 2,
                num_tex_gens: 1,
                num_tev_stages: 1,
                early_z_compare: early_cmp_pos,
                z_mode: ZMode::default(),
                dither: Some(dither_pos),
                mat_colors: [Some(Color::WHITE), None],
                color_channel_infos: [
                    MaybeInvalid::Some(ColorChannelInfo {
                        enabled: true,
                        mat_src_color: ColorSrc::Vertex,
                        light_mask: LightMask::LIGHT0 | LightMask::LIGHT1,
                        diffuse_fn: DiffuseFn::Clamp,
                        attenuation_fn: AttenuationFn::Specular,
                        ambient_src_color: Default::default(),
                    }),
                    MaybeInvalid::Some(ColorChannelInfo {
                        enabled: false,
                        mat_src_color: ColorSrc::Vertex,
                        light_mask: Default::default(),
                        diffuse_fn: DiffuseFn::Clamp,
                        attenuation_fn: AttenuationFn::Specular,
                        ambient_src_color: Default::default(),
                    }),
                    MaybeInvalid::Some(ColorChannelInfo {
                        enabled: true,
                        mat_src_color: Default::default(),
                        light_mask: Default::default(),
                        diffuse_fn: DiffuseFn::Signed,
                        attenuation_fn: Default::default(),
                        ambient_src_color: Default::default(),
                    }),
                    MaybeInvalid::Some(ColorChannelInfo {
                        enabled: false,
                        mat_src_color: Default::default(),
                        light_mask: Default::default(),
                        diffuse_fn: Default::default(),
                        attenuation_fn: AttenuationFn::Unknown,
                        ambient_src_color: Default::default(),
                    }),
                ],
                ambient_colors: [
                    MaybeInvalid::Some(Color::linear_rgba(
                        0.596078435,
                        0.596078435,
                        0.596078435,
                        0.0,
                    )),
                    MaybeInvalid::Some(Color::linear_rgba(0.0, 0.0, 0.0, 0.0)),
                ],
                light_colors: [None, None, None, None, None, None, None, None],
                tex_gen_infos: [Some(texgen_pos), None, None, None, None, None, None, None],
                post_tex_gen_infos: [None, None, None, None, None, None, None, None],
                tex_matrix_infos: [
                    Some(TexMatrixInfo {
                        proj: Default::default(),
                        unk: 0,
                        effect_translation: Vec3::ZERO,
                        scale: Vec2::ONE,
                        rotation: 0.0,
                        translation: Vec2::ZERO,
                        proj_matrix: new_mat4_rows([
                            1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                            0.0, 1.0,
                        ]),
                    }),
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                ],
                post_tex_matrix_infos: [
                    None, None, None, None, None, None, None, None, None, None, None, None, None,
                    None, None, None, None, None, None, None,
                ],
                tex_inds: [None, None, None, None, None, None, None, None],
                tev_konst_colors: [
                    Some(Color::WHITE),
                    Some(Color::WHITE),
                    Some(Color::WHITE),
                    Some(Color::linear_rgba(0.0, 0.0, 0.0, 0.0)),
                ],
                tev_konst_color_sels: [
                    0x5, 0x1C, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC,
                ],
                tev_konst_alpha_sels: [
                    0x1F, 0x1F, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C,
                    0x1C, 0x1C, 0x1C,
                ],
                tev_orders: [
                    Some(TevOrder {
                        tex_coord: TexCoordId::TexCoord0,
                        tex_map: TexMapId::Map0,
                        channel_id: ColorChannelId::Color0,
                    }),
                    Some(TevOrder {
                        tex_coord: TexCoordId::TexCoord0,
                        tex_map: TexMapId::Map0,
                        channel_id: ColorChannelId::Null,
                    }),
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                ],
                tev_colors: [
                    Some(Color::WHITE),
                    Some(Color::WHITE),
                    Some(Color::WHITE),
                    Some(Color::linear_rgba(0.0, 0.0, 0.0, 0.0)),
                ],
                tev_stage_infos: [
                    Some(TevStage {
                        color_in_a: CombineColorInput::Zero,
                        color_in_b: Default::default(),
                        color_in_c: CombineColorInput::RasColor,
                        color_in_d: CombineColorInput::Zero,
                        color_op: TevOp::Add,
                        color_bias: TevBias::Zero,
                        color_scale: TevScale::Scale2,
                        color_clamp: true,
                        color_reg_id: Default::default(),
                        alpha_in_a: CombineAlphaInput::TexAlpha,
                        alpha_in_b: Default::default(),
                        alpha_in_c: Default::default(),
                        alpha_in_d: Default::default(),
                        alpha_op: TevOp::Add,
                        alpha_bias: TevBias::Zero,
                        alpha_scale: TevScale::Scale1,
                        alpha_clamp: true,
                        alpha_reg_id: Default::default(),
                    }),
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                ],
                tev_swap_modes: [
                    Some(swap_pos),
                    Some(swap_pos),
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                ],
                tev_swap_mode_tables: [
                    Some(TevSwapModeTable {
                        r: 0,
                        g: 1,
                        b: 2,
                        a: 3,
                    }),
                    Some(TevSwapModeTable {
                        r: 0,
                        g: 1,
                        b: 2,
                        a: 3,
                    }),
                    Some(TevSwapModeTable {
                        r: 0,
                        g: 1,
                        b: 2,
                        a: 3,
                    }),
                    Some(TevSwapModeTable {
                        r: 0,
                        g: 1,
                        b: 2,
                        a: 3,
                    }),
                ],
                unk_inds: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                fog_info: Some(FogInfo {
                    kind: 2,
                    enabled: false,
                    center: 320,
                    start: 0.0,
                    end: 20000.0,
                    near: 5.0,
                    far: 50000.0,
                    color: Color::WHITE,
                    range_adjustments: [
                        1.0, 1.00390625, 1.01171875, 1.0234375, 1.03515625, 1.05078125, 1.0703125,
                        1.08984375, 1.11328125, 1.140625,
                    ],
                }),
                alpha_compare: AlphaCompare {
                    comp_0: CompareKind::Gt,
                    value_0: 127,
                    op: CompareOp::AND,
                    comp_1: CompareKind::Always,
                    value_1: 0,
                },
                blend_mode: BlendMode {
                    kind: BlendKind::Blend,
                    src_fact: BlendControl::SrcAlpha,
                    dst_fact: BlendControl::OneMinusSrcAlpha,
                    op: LogicOp::NoOp,
                },
                nbt_scale: Some(NbtScale {
                    unk: 0,
                    scale: Default::default(),
                }),
            },
        }
    }
}

// todo: it might be wise for some of these to be stored somewhere else, and compared via
//  reference address or something to see if they're the same, or just compress them
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct MaterialInfo {
    #[serde(default, skip_serializing_if = "is_default")]
    pub flags: u8,
    #[serde(default, skip_serializing_if = "is_default")]
    pub cull_mode: usize,
    /// Sent to GX_SetNumChans(u8). Sets the number of color channels that are output to the TEV
    /// stages. Basically defines the number of per-vertex colors that get rasterized.
    /// If num is set to 0, then at least one texture coordinate must be generated. If num is set
    /// to 1, then channel GX_COLOR0A0 will be rasterized. If num is set to 2 (the maximum value),
    /// then GX_COLOR0A0 and GX_COLOR1A1 will be rasterized.
    #[serde(default, skip_serializing_if = "is_default")]
    pub num_color_chans: u8,
    #[serde(default, skip_serializing_if = "is_default")]
    pub num_tex_gens: u8,
    #[serde(default, skip_serializing_if = "is_default")]
    pub num_tev_stages: u8,
    #[serde(default, skip_serializing_if = "is_default")]
    pub early_z_compare: usize,
    #[serde(default, skip_serializing_if = "is_default")]
    pub z_mode: ZMode,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub dither: Option<usize>,
    // todo: 2 because the max num of color channels output to TEV stages is 2?
    #[serde(default, skip_serializing_if = "is_default")]
    pub mat_colors: [Option<Color>; 2],
    /// Each one seems to represent a channel in RGBA
    #[serde(default, skip_serializing_if = "is_default")]
    pub color_channel_infos: [MaybeInvalid<ColorChannelInfo>; 4],
    // todo: 2 because the max num of color channels output to TEV stages is 2?
    #[serde(default, skip_serializing_if = "is_default")]
    pub ambient_colors: [MaybeInvalid<Color>; 2],
    /// Each one seems to represent a light
    #[serde(default, skip_serializing_if = "is_default")]
    pub light_colors: [Option<Color>; 8],
    #[serde(default, skip_serializing_if = "is_default")]
    pub tex_gen_infos: [Option<usize>; 8],
    #[serde(default, skip_serializing_if = "is_default")]
    pub post_tex_gen_infos: [Option<PostTexGenInfo>; 8],
    #[serde(default, skip_serializing_if = "is_default")]
    pub tex_matrix_infos: [Option<TexMatrixInfo>; 10],
    #[serde(default, skip_serializing_if = "is_default")]
    pub post_tex_matrix_infos: [Option<TexMatrixInfo>; 20],
    #[serde(default, skip_serializing_if = "is_default")]
    pub tex_inds: [Option<u16>; 8],
    #[serde(default, skip_serializing_if = "is_default")]
    pub tev_konst_colors: [Option<Color>; 4],
    #[serde(default, skip_serializing_if = "is_default")]
    // todo enum exists for these
    pub tev_konst_color_sels: [u8; 16],
    #[serde(default, skip_serializing_if = "is_default")]
    // todo enum exists for these
    pub tev_konst_alpha_sels: [u8; 16],
    #[serde(default, skip_serializing_if = "is_default")]
    pub tev_orders: [Option<TevOrder>; 16],
    #[serde(default, skip_serializing_if = "is_default")]
    pub tev_colors: [Option<Color>; 4],
    #[serde(default, skip_serializing_if = "is_default")]
    pub tev_stage_infos: [Option<TevStage>; 16],
    #[serde(default, skip_serializing_if = "is_default")]
    pub tev_swap_modes: [Option<usize>; 16],
    #[serde(default, skip_serializing_if = "is_default")]
    pub tev_swap_mode_tables: [Option<TevSwapModeTable>; 4],
    /// 12 shorts of unknown indexes
    #[serde(default, skip_serializing_if = "is_default")]
    pub unk_inds: [u16; 12],
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub fog_info: Option<FogInfo>,
    #[serde(default, skip_serializing_if = "is_default")]
    pub alpha_compare: AlphaCompare,
    #[serde(default, skip_serializing_if = "is_default")]
    pub blend_mode: BlendMode,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub nbt_scale: Option<NbtScale>,
}

// todo move read into here?
impl MaterialInfo {
    pub fn write_bmd<W: Write + Seek>(
        &self,
        w: &mut W,
        params: &mut MaterialParams,
    ) -> Result<(), BmdWriteError> {
        w!(w, self.flags)?;

        w!(w, self.cull_mode as u8)?;

        let ind = find_or_insert!(params.num_color_channels, self.num_color_chans);
        w!(w, ind as u8)?;

        let ind = find_or_insert!(params.num_tex_gens, self.num_tex_gens);
        w!(w, ind as u8)?;

        let ind = find_or_insert!(params.num_tev_stages, self.num_tev_stages);
        w!(w, ind as u8)?;

        let ind = self.early_z_compare;
        w!(w, ind as u8)?;

        let ind = find_or_insert!(params.z_modes, self.z_mode);
        w!(w, ind as u8)?;

        if let Some(ind) = self.dither {
            w!(w, ind as u8)?;
        } else {
            w!(w, 0xFFu8)?;
        }

        for color in self.mat_colors.clone() {
            // todo Nones might need to be at the end?
            if let Some(color) = color {
                let ind = find_or_insert!(params.mat_colors, color);
                w!(w, ind as u16)?;
            } else {
                w!(w, 0xFFFFu16)?;
            }
        }

        for chan in self.color_channel_infos {
            match chan {
                MaybeInvalid::Some(chan) => {
                    let ind = find_or_insert!(params.color_channel_infos, chan);
                    w!(w, ind as u16)?;
                }
                MaybeInvalid::None => {
                    w!(w, 0xFFFFu16)?;
                }
                MaybeInvalid::Invalid => {
                    // Assumption: FFFE won't function the same as FFFF but will act similarly to
                    // how the game picks 1,2,3 etc that are unused inds
                    w!(w, 0xFFFEu16)?;
                }
            }
        }

        for color in self.ambient_colors {
            match color {
                MaybeInvalid::Some(color) => {
                    let ind = find_or_insert!(params.ambient_colors, color);
                    w!(w, ind as u16)?;
                }
                MaybeInvalid::None => {
                    w!(w, 0xFFFFu16)?;
                }
                MaybeInvalid::Invalid => {
                    // Assumption: FFFE won't function the same as FFFF but will act similarly to
                    // how the game picks 1,2,3 etc that are unused inds
                    w!(w, 0xFFFEu16)?;
                }
            }
        }

        for color in self.light_colors.clone() {
            // todo Nones might need to be at the end?
            if let Some(color) = color {
                let ind = find_or_insert!(params.light_colors, color);
                w!(w, ind as u16)?;
            } else {
                w!(w, 0xFFFFu16)?;
            }
        }

        for info in self.tex_gen_infos.clone() {
            // todo Nones might need to be at the end?
            if let Some(ind) = info {
                w!(w, ind as u16)?;
            } else {
                w!(w, 0xFFFFu16)?;
            }
        }

        for info in self.post_tex_gen_infos.clone() {
            // todo Nones might need to be at the end?
            if let Some(info) = info {
                let ind = find_or_insert!(params.post_tex_gen_infos, info);
                w!(w, ind as u16)?;
            } else {
                w!(w, 0xFFFFu16)?;
            }
        }

        for info in self.tex_matrix_infos.clone() {
            // todo Nones might need to be at the end?
            if let Some(info) = info {
                let ind = find_or_insert!(params.tex_matrix_infos, info);
                w!(w, ind as u16)?;
            } else {
                w!(w, 0xFFFFu16)?;
            }
        }

        for info in self.post_tex_matrix_infos.clone() {
            // todo Nones might need to be at the end?
            if let Some(info) = info {
                let ind = find_or_insert!(params.post_tex_matrix_infos, info);
                w!(w, ind as u16)?;
            } else {
                w!(w, 0xFFFFu16)?;
            }
        }

        for ind in self.tex_inds.clone() {
            // todo Nones might need to be at the end?
            if let Some(ind) = ind {
                let ind = find_or_insert!(params.tex_remaps, ind);
                w!(w, ind as u16)?;
            } else {
                w!(w, 0xFFFFu16)?;
            }
        }

        for color in self.tev_konst_colors.clone() {
            // todo Nones might need to be at the end?
            if let Some(color) = color {
                let ind = find_or_insert!(params.tev_konst_colors, color);
                w!(w, ind as u16)?;
            } else {
                w!(w, 0xFFFFu16)?;
            }
        }

        for color in self.tev_konst_color_sels.clone() {
            w!(w, color)?;
        }

        for alpha in self.tev_konst_alpha_sels.clone() {
            w!(w, alpha)?;
        }

        for order in self.tev_orders.clone() {
            // todo Nones might need to be at the end?
            if let Some(order) = order {
                let ind = find_or_insert!(params.tev_orders, order);
                w!(w, ind as u16)?;
            } else {
                w!(w, 0xFFFFu16)?;
            }
        }

        for color in self.tev_colors.clone() {
            // todo Nones might need to be at the end?
            if let Some(color) = color {
                let ind = find_or_insert!(params.tev_colors, color);
                w!(w, ind as u16)?;
            } else {
                w!(w, 0xFFFFu16)?;
            }
        }

        for info in self.tev_stage_infos.clone() {
            // todo Nones might need to be at the end?
            if let Some(info) = info {
                let ind = find_or_insert!(params.tev_stage_infos, info);
                w!(w, ind as u16)?;
            } else {
                w!(w, 0xFFFFu16)?;
            }
        }

        for mode in self.tev_swap_modes.clone() {
            // todo Nones might need to be at the end?
            if let Some(ind) = mode {
                w!(w, ind as u16)?;
            } else {
                w!(w, 0xFFFFu16)?;
            }
        }

        for mode in self.tev_swap_mode_tables.clone() {
            // todo Nones might need to be at the end?
            if let Some(mode) = mode {
                let ind = find_or_insert!(params.tev_swap_mode_tables, mode);
                w!(w, ind as u16)?;
            } else {
                w!(w, 0xFFFFu16)?;
            }
        }

        for unk in self.unk_inds.clone() {
            w!(w, unk)?;
        }

        if let Some(fog_info) = self.fog_info {
            let ind = find_or_insert!(params.fog_infos, fog_info);
            w!(w, ind as u16)?;
        } else {
            w!(w, 0xFFFFu16)?;
        }

        let ind = find_or_insert!(params.alpha_compares, self.alpha_compare);
        w!(w, ind as u16)?;

        let ind = find_or_insert!(params.blend_modes, self.blend_mode);
        w!(w, ind as u16)?;

        if let Some(nbt_scale) = self.nbt_scale {
            let ind = find_or_insert!(params.nbt_scales, nbt_scale);
            w!(w, ind as u16)?;
        } else {
            w!(w, 0xFFFFu16)?;
        }

        Ok(())
    }
}

/*
todo some of this stuff needs tightening but will work in a loose form.
 Can just read the materials first and follow the indexes which I think was
 intended
let det = info.size / section.entry_len();
println!("{:?}: {}:{} {:X?} want {}@{} got {}@{:?}", section, info.offset, info.size, data, mat_count, section.entry_len(), det, info.size.checked_div(det));

NumColorChannels: "\u{1}Thi" want 1@1 got 4@Some(1)
ColorChannelInfo: "\u{1}\0\u{1}\u{2}\u{2}���\0\0\0\u{2}\u{2}���" want 1@8 got 2@Some(8)
NumTexGens: "\0Thi" want 1@1 got 4@Some(1)
TexGenInfo: "" want 1@4 got 0@None
TextureRemapTable: "" want 1@2 got 0@None
TevKonstColors: "����������������" want 1@4 got 4@Some(4)
NumTevStages: "\u{1}Thi" want 1@1 got 4@Some(1)
AlphaCompareInfo: "\u{7}\0\0\u{7}\0���\u{6}�\0\u{3}����\u{7}\0\0\u{7}\0���" want 1@8 got 3@Some(8)
BlendModeInfo: "\0\u{1}\0\u{3}\0\u{1}\0\u{3}\u{1}\u{4}\u{5}\u{3}" want 1@4 got 3@Some(4)
ZCompareInfo: "\u{1}ThiThis" want 1@1 got 8@Some(1)
 */
