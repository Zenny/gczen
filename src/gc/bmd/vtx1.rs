// common reference (component type, data type):
// Position 1 3
// Normal   0 3
// Color0   1 5
// Tex0     1 3

use std::{
    collections::HashMap,
    io::{Read, Seek, SeekFrom, Write},
};

use glam::Vec2;
use num_enum::TryFromPrimitive;
use serde::{Deserialize, Serialize};

use crate::{
    bytes::{read_u32, read_u8},
    gc::{
        align::{write_align, AlignType},
        bmd::{error::BmdWriteError, shp1::Shp1},
        error::ReadError,
        types::mesh::{DataVec, GxAttribute, GxComponentType, GxDataType, IndexedVertex},
    },
    w, wat, wraw,
};

const VTX1: u32 = 0x56545831;

// todo I think I could use this in another section that does similar
/// Calculates the length of a section given an array of offsets which may or may not be 0
pub struct DataBlock {
    pub offset: u32,
    pub len: u32,
}

impl DataBlock {
    pub fn get_offset_len(arr: &[u32; 13], ind: usize, vtx1_len: u32) -> Self {
        let mut i = ind;
        let mut block = Self {
            offset: arr[ind],
            len: 0,
        };
        loop {
            i += 1;
            if i == 13 {
                block.len = vtx1_len - block.offset;
                return block;
            }

            let next_offset = arr[i];
            if next_offset == 0 {
                continue;
            }
            block.len = next_offset - block.offset;
            return block;
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AttributeFormat {
    pub data_type: GxDataType,
    pub component_type: GxComponentType,
    /// Number of mantissa bits for fixed point numbers (position of decimal point)
    pub fractional: u8,
}

#[derive(Debug, Clone)]
pub struct Vtx1 {
    // todo these formats might need to be generated on the fly on first open after a model change
    /// Defines the type of each attribute as it is stored in the bmd, sometimes scaled by some
    /// fraction (u8) (see: load_vec3_data)
    pub formats: HashMap<GxAttribute, AttributeFormat>,
    pub attributes: HashMap<GxAttribute, DataVec>,
    pub len: u32,
}

impl Vtx1 {
    pub fn new<R: Read + Seek>(buf: &mut R, vtx1_offset: u32) -> Result<Self, ReadError> {
        let vtx1 = read_u32(buf)?;
        // VTX1
        if vtx1 != VTX1 {
            return Err(ReadError::ReadCheck("VTX1"));
        }

        let vtx1_len = read_u32(buf)?;
        // relative to the beginning of this section. It's the
        // next section so no skipping is needed
        let _format_offset = read_u32(buf)?;

        // these are relative to the beginning of this section so we will add offset
        // to be relative to the start later
        // See data_offset_index() for order
        let offsets = [
            read_u32(buf)?,
            read_u32(buf)?,
            read_u32(buf)?,
            read_u32(buf)?,
            read_u32(buf)?,
            read_u32(buf)?,
            read_u32(buf)?,
            read_u32(buf)?,
            read_u32(buf)?,
            read_u32(buf)?,
            read_u32(buf)?,
            read_u32(buf)?,
            read_u32(buf)?,
        ];

        let mut formats = HashMap::new();
        let mut attrs = HashMap::new();

        loop {
            // Gotta read 4 bytes even though only 1 is used
            let attr = read_u32(buf)? as u8;
            let attr = GxAttribute::try_from_primitive(attr)
                .map_err(|_| ReadError::InvalidGxAttribute(attr))?;
            if attr == GxAttribute::Null {
                break;
            }

            let component_type = GxComponentType::try_from(attr, read_u32(buf)?)?;
            let data_type = GxDataType::try_from(attr, read_u32(buf)?)?;
            let fract = read_u8(buf)?;
            let format = AttributeFormat {
                data_type,
                component_type,
                fractional: fract,
            };

            // Skip padding
            buf.seek(SeekFrom::Current(3))?;

            // Bookmark pos to come back here later after reading data
            let pos = buf.stream_position()?;

            // The size of each piece of data
            let data_size = data_type.size() * component_type.channels();

            let est_block =
                DataBlock::get_offset_len(&offsets, attr.data_offset_index()?, vtx1_len);

            // The number of datas in the chunk (Some might be padding)
            let est_data_count = est_block.len / data_size;

            let data =
                attr.load_data(buf, est_block.offset + vtx1_offset, est_data_count, &format)?;
            attrs.insert(attr, data);
            formats.insert(attr, format);

            buf.seek(SeekFrom::Start(pos))?;
        }

        Ok(Self {
            formats,
            attributes: attrs,
            len: vtx1_len,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        // So offsets can be relative to this section and calc length
        let start = w.stream_position()?;
        w!(w, VTX1)?;

        let length_offset = w.stream_position()?;
        // Length written later
        w!(w, 0u32)?;

        let format_offset = w.stream_position()?;
        // format offset written later
        w!(w, 0u32)?;

        // Each offset is 4 bytes so we can do ind * 4 to get the right one
        // See data_offset_index() for order
        let offsets_start = w.stream_position()?;
        for _ in 0..GxAttribute::vertex_data_types().len() {
            w!(w, 0u32)?;
        }

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, format_offset);

        // Trim out unsupported attributes (We shouldn't iterate self.attributes in case there's
        // unwanted stuff)
        for attr in GxAttribute::vertex_data_types().into_iter() {
            // Formats that do not exist do not get entered and the offset stays 0
            if self.attributes.contains_key(&attr) {
                if let Some(format) = self.formats.get(&attr) {
                    w!(w, attr as u8 as u32)?;
                    w!(w, format.component_type.try_into_u32(attr)?)?;
                    w!(w, format.data_type.try_into_u32(attr)?)?;
                    w!(w, format.fractional)?;

                    write_align(w, 16, AlignType::FF)?;
                }
            }
        }

        // null attr to signify end
        wraw!(
            w,
            [0, 0, 0, 0xFF, 0, 0, 0, 0x01, 0, 0, 0, 0, 0, 0xFF, 0xFF, 0xFF]
        )?;

        write_align(w, 32, AlignType::String)?;

        for (i, attr) in GxAttribute::vertex_data_types().into_iter().enumerate() {
            // Formats that do not exist do not get entered and the offset stays 0
            if let (Some(data), Some(format)) =
                (self.attributes.get(&attr), self.formats.get(&attr))
            {
                let here = w.stream_position()?;
                // Write the current location to the offset relative to this attribute
                wat!(w, (here - start) as u32, offsets_start + (i as u64 * 4));

                data.write_data(w, format)?;

                write_align(w, 32, AlignType::String)?;
            }
        }

        let len = (w.stream_position()? - start) as u32;
        wat!(w, len, length_offset);

        Ok(())
    }

    pub fn get_vert_texcoord(&self, attr: GxAttribute, vert: &IndexedVertex) -> Option<&Vec2> {
        let DataVec::Vec2(v) = self
            .attributes
            .get(&attr)
            .expect("Failed get attr that should always be there")
        else {
            return None;
        };

        v.get(vert.get(attr) as usize)
    }

    pub fn trim(&mut self, shp1: &Shp1) {
        for (attr, data) in self.attributes.iter_mut() {
            let mut max_ind = 0;
            for s in shp1.shapes.iter() {
                for mp in s.mat_prims.iter() {
                    for prim in mp.prims.iter() {
                        for vert in prim.verts.iter() {
                            // Note the max index
                            let ind = vert.get(*attr) as usize;
                            if ind > max_ind {
                                max_ind = ind;
                            }
                        }
                    }
                }
            }

            // Add 1 so we're a proper length
            max_ind += 1;
            let old_len = data.len();
            if max_ind == old_len {
                continue;
            }

            log::debug!("Trimming {:?} from len {} to {}", attr, old_len, max_ind);

            match data {
                DataVec::Float(v) => v.truncate(max_ind),
                DataVec::Vec2(v) => v.truncate(max_ind),
                DataVec::Vec3(v) => v.truncate(max_ind),
                DataVec::Color(v) => v.truncate(max_ind),
            }
        }
    }
}
