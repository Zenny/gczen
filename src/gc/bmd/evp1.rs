use std::io::{Read, Seek, SeekFrom, Write};

use glam::Mat4;

use crate::{
    bytes::{read_f32, read_u16, read_u32, read_u8},
    gc::{
        align::{write_align, AlignType},
        bmd::error::BmdWriteError,
        error::ReadError,
        types::mesh::{VertexWeights, Weight},
    },
    util::new_mat4_rows,
    w, wat,
};

const EVP1: u32 = 0x45565031;

#[derive(Debug, Clone)]
pub struct Evp1 {
    pub len: u32,
    pub weights: Vec<VertexWeights>,
    /// Inverse bind matrices
    pub matrices: Vec<Mat4>,
}

impl Evp1 {
    pub fn new<R: Read + Seek>(buf: &mut R, evp1_offset: u32) -> Result<Self, ReadError> {
        let evp1 = read_u32(buf)?;
        // EVP1
        if evp1 != EVP1 {
            return Err(ReadError::ReadCheck("EVP1"));
        }

        let evp1_len = read_u32(buf)?;
        // num of envelope matrices
        let env_count = read_u16(buf)? as usize;

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        let bone_count_table_offset = read_u32(buf)?;
        let bone_ind_table_offset = read_u32(buf)?;
        let weight_table_offset = read_u32(buf)?;
        let inverse_bind_pos_table_offset = read_u32(buf)?;

        // If the offset is 0, there is no bone count table, so technically nothing
        // else should exist
        if bone_count_table_offset != 0 {
            let mut bone_counts = Vec::with_capacity(env_count);
            let mut bone_inds = Vec::with_capacity(env_count);
            let mut weights = Vec::with_capacity(env_count);

            for _ in 0..env_count {
                bone_counts.push(read_u8(buf)?);
            }

            buf.seek(SeekFrom::Start(
                (bone_ind_table_offset + evp1_offset) as u64,
            ))?;

            // todo can probably compress these next two if statements into one
            for i in 0..env_count {
                for _ in 0..bone_counts[i] {
                    bone_inds.push(read_u16(buf)?);
                }
            }

            buf.seek(SeekFrom::Start((weight_table_offset + evp1_offset) as u64))?;

            let mut total = 0;
            for i in 0..env_count {
                let count = bone_counts[i];
                let mut vertex_weights = VertexWeights::new(count as usize);

                for j in 0..count {
                    vertex_weights
                        .weights
                        .push(Weight::new(read_f32(buf)?, bone_inds[total + j as usize]));
                }

                weights.push(vertex_weights);
                total += count as usize;
            }

            let mat_count = ((evp1_len - inverse_bind_pos_table_offset) / 48) as usize;
            let mut matrices = Vec::with_capacity(mat_count);

            for _ in 0..mat_count {
                #[rustfmt::skip]
                matrices.push(new_mat4_rows([
                    read_f32(buf)?, read_f32(buf)?, read_f32(buf)?, read_f32(buf)?,
                    read_f32(buf)?, read_f32(buf)?, read_f32(buf)?, read_f32(buf)?,
                    read_f32(buf)?, read_f32(buf)?, read_f32(buf)?, read_f32(buf)?,
                    0.0, 0.0, 0.0, 1.0,
                ]));
            }

            Ok(Self {
                len: evp1_len,
                weights,
                matrices,
            })
        } else {
            Ok(Self {
                len: evp1_len,
                weights: vec![],
                matrices: vec![],
            })
        }
    }
    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        let start = w.stream_position()?;
        w!(w, EVP1)?;

        let length_offset = w.stream_position()?;
        w!(w, 0u32)?;

        w!(w, self.weights.len() as u16)?;
        // Padding - Doesn't seem to be an alignment?
        w!(w, 0xFFFFu16)?;

        let bone_count_table_offset = w.stream_position()?;
        w!(w, 0u32)?;

        let bone_ind_table_offset = w.stream_position()?;
        w!(w, 0u32)?;

        let weight_table_offset = w.stream_position()?;
        w!(w, 0u32)?;

        let inverse_bind_pos_table_offset = w.stream_position()?;
        w!(w, 0u32)?;

        // No data? Nothing should exist
        if self.weights.len() > 0 {
            let here = w.stream_position()?;
            wat!(w, (here - start) as u32, bone_count_table_offset);

            for group in self.weights.iter() {
                w!(w, group.weights.len() as u8)?;
            }

            let here = w.stream_position()?;
            wat!(w, (here - start) as u32, bone_ind_table_offset);

            for group in self.weights.iter() {
                for weight in group.weights.iter() {
                    w!(w, weight.bone_ind)?;
                }
            }

            write_align(w, 4, AlignType::String)?;

            let here = w.stream_position()?;
            wat!(w, (here - start) as u32, weight_table_offset);

            for group in self.weights.iter() {
                for weight in group.weights.iter() {
                    w!(w, weight.weight)?;
                }
            }

            // todo maybe not?
            write_align(w, 4, AlignType::String)?;

            let here = w.stream_position()?;
            wat!(w, (here - start) as u32, inverse_bind_pos_table_offset);

            for mat in self.matrices.iter() {
                // Skip the last row because it's 0,0,0,1
                for i in 0..3 {
                    let r = mat.row(i);
                    w!(w, r.x)?;
                    w!(w, r.y)?;
                    w!(w, r.z)?;
                    w!(w, r.w)?;
                }
            }
        }

        write_align(w, 32, AlignType::String)?;

        let len = (w.stream_position()? - start) as u32;
        wat!(w, len, length_offset);

        Ok(())
    }

    /// Returns an ind into evp1.weights for the entry
    pub fn insert_raw_weights(&mut self, weights: &[f32], bones: &[u16]) -> usize {
        let new_weight = VertexWeights {
            weights: generate_weights(weights, bones),
        };

        if let Some(pos) = self.weights.iter().position(|w| w == &new_weight) {
            pos
        } else {
            let ind = self.weights.len();
            self.weights.push(new_weight);
            ind
        }
    }
}

/// Converts weight and bone arrays assumed to be the same length into weight entries.
fn generate_weights(weights: &[f32], bones: &[u16]) -> Vec<Weight> {
    let mut out = Vec::with_capacity(4);

    for (i, w) in weights.iter().enumerate() {
        out.push(Weight {
            // round weights to 1 decimal place
            weight: *w,
            bone_ind: bones[i],
        });
    }

    out
}
