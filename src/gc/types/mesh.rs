//! Mesh related types

use std::{
    cmp::Ordering,
    collections::BTreeMap,
    fmt::{Debug, Display, Formatter},
    io::{Read, Seek, SeekFrom, Write},
    str::FromStr,
};

use glam::{Vec2, Vec3A};
use bevy_color::{Color, ColorToComponents};
use mugltf::{AccessorComponentType, AccessorType};
use num_enum::TryFromPrimitive;
use serde::{Deserialize, Serialize};
use strum::{EnumIter, IntoEnumIterator};

use crate::{
    bytes::{read_f32, read_u16, read_u32, read_u8},
    errify,
    error::GltfError,
    gc::{
        bmd::{error::BmdWriteError, vtx1::AttributeFormat},
        error::ReadError,
        types::error::AttributeParseError,
    },
    util::{
        TryNumFrom, BIT4_TO_COLOR, BIT5_TO_COLOR, BIT6_TO_COLOR, BYTE_TO_COLOR, COLOR_TO_BIT4,
        COLOR_TO_BIT5, COLOR_TO_BIT6, COLOR_TO_BYTE,
    },
    w, wraw,
};

/// NOTE: Only TriangleStrip and TriangleFan are used ever apparently
#[derive(Debug, Copy, Clone, PartialEq, TryFromPrimitive)]
#[repr(u16)]
pub enum PrimitiveKind {
    Quads = 0,
    Quads2 = 1,
    Triangles = 2,
    TriangleStrip = 3,
    TriangleFan = 4,
    Lines = 5,
    LineStrip = 6,
    Points = 7,
}

#[derive(Debug, Clone, PartialEq, TryFromPrimitive)]
#[repr(u8)]
pub enum Mode {
    /// Objects that have all weight of 1 to a parent bone can be turned into "Normal" (non skinned)
    /// meshes as the game will be able to figure out which matrix to use based on the parent joint.
    Normal = 0,
    BillboardXY = 1,
    BillboardY = 2,
    SkinnedMesh = 3,
}

#[derive(Clone, PartialEq)]
pub struct IndexedVertex {
    /// A list of indexes at positions denoted by GxAttributes, that is to say at
    /// \[GxAttribute::Position] is an index to a position vert
    inds: Vec<u16>,
}

impl IndexedVertex {
    pub fn new() -> Self {
        Self {
            inds: vec![0xFFFF; GxAttribute::MaxAttr as u8 as usize],
        }
    }

    /// Usually contains an index to access some data in vtx1
    pub fn get(&self, attr: GxAttribute) -> u16 {
        let attr = attr as u8;
        if attr > GxAttribute::NBT as u8 {
            panic!("IndexedVertex bad access");
        }
        self.inds[attr as usize]
    }

    pub fn set(&mut self, attr: GxAttribute, v: u16) {
        let attr = attr as u8;
        if attr > GxAttribute::NBT as u8 {
            panic!("IndexedVertex bad access");
        }
        self.inds[attr as usize] = v;
    }
}

impl Debug for IndexedVertex {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut s = f.debug_struct("IndexedVertex");
        for attr in GxAttribute::iter() {
            if attr == GxAttribute::MaxAttr {
                break;
            }

            let val = self.get(attr);
            if val < 0xFFFF {
                s.field(&attr.to_string(), &val);
            }
        }

        s.finish()
    }
}

/// Each of these generally represents eg. a single triangle strip
#[derive(Debug, Clone, PartialEq)]
pub struct IndexedPrimitive {
    pub kind: PrimitiveKind,
    /// You probably don't want to be pushing into this as it starts with the elements
    pub verts: Vec<IndexedVertex>,
}

impl IndexedPrimitive {
    /// Initializes an indexed primitive with `capacity` number of verts in it
    pub fn new(kind: PrimitiveKind, capacity: usize) -> Self {
        Self {
            kind,
            verts: vec![IndexedVertex::new(); capacity],
        }
    }

    /// Converts possible triangulations to standard CCW triangulation
    pub fn ccw_triangulate(&self) -> Result<Vec<IndexedVertex>, GltfError> {
        let mut out = Vec::with_capacity(self.verts.len() * 3);

        match &self.kind {
            PrimitiveKind::Triangles => return Ok(self.verts.clone()),
            PrimitiveKind::TriangleStrip => {
                let mut even = true;

                for v in 2..self.verts.len() {
                    let mut tri = [
                        IndexedVertex::new(),
                        IndexedVertex::new(),
                        IndexedVertex::new(),
                    ];

                    // Push the tri in order
                    tri[0] = self.verts[v - 2].clone();
                    if even {
                        tri[1] = self.verts[v].clone();
                        tri[2] = self.verts[v - 1].clone();
                    } else {
                        tri[1] = self.verts[v - 1].clone();
                        tri[2] = self.verts[v].clone();
                    }

                    // Degenerate triangle check
                    // todo: could maybe speed this up by keeping some updated hash of the indexedvertex
                    if tri[0] != tri[1] && tri[1] != tri[2] && tri[2] != tri[0] {
                        out.extend(tri);
                    } else {
                        log::warn!("Degenerate triangle skipped for TriStrip");
                    }
                    even = !even;
                }
            }
            PrimitiveKind::TriangleFan => {
                for v in 1..self.verts.len() - 1 {
                    let tri = [
                        self.verts[v].clone(),
                        self.verts[v + 1].clone(),
                        self.verts[0].clone(),
                    ];
                    if tri[0] != tri[1] && tri[1] != tri[2] && tri[2] != tri[0] {
                        out.extend(tri);
                    } else {
                        log::warn!("Degenerate triangle skipped for TriFan");
                    }
                }
            }
            k => return Err(GltfError::UnimplementedTriType(*k)),
        }

        Ok(out)
    }
}

#[derive(Clone, PartialEq, Eq, Hash)]
pub struct VertexDescriptor {
    pub attributes: BTreeMap<GxAttribute, GxAttributeKind>,
    /// A set of flags compiled from the attributes
    descriptor: u32,
}

impl VertexDescriptor {
    #[allow(unused)]
    pub fn new() -> Self {
        Self {
            attributes: Default::default(),
            descriptor: 0,
        }
    }

    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let mut attributes = BTreeMap::new();

        loop {
            // Gotta read 4 bytes even though only 1 is used
            let attr = read_u32(buf)? as u8;
            let attr = GxAttribute::try_from_primitive(attr)
                .map_err(|_| ReadError::InvalidGxAttribute(attr))?;

            if attr == GxAttribute::Null {
                break;
            }

            // Gotta read 4 bytes even though only 1 is used
            let kind = read_u32(buf)? as u8;
            let kind = GxAttributeKind::try_from_primitive(kind)
                .map_err(|_| ReadError::InvalidGxAttributeKind(kind))?;

            attributes.insert(attr, kind);
        }

        let mut new = Self {
            attributes,
            descriptor: 0,
        };

        new.calc_descriptor();

        Ok(new)
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        for (attr, kind) in self.attributes.iter() {
            w!(w, *attr as u8 as u32)?;
            w!(w, *kind as u8 as u32)?;
        }
        w!(w, 0xFFu32)?;
        w!(w, 0x00u32)?;

        Ok(())
    }

    pub fn get_descriptor(&self) -> u32 {
        self.descriptor
    }

    pub fn is_set(&self, attr: GxAttribute) -> bool {
        self.descriptor & (1 << (attr as u8)) != 0
    }

    pub fn get_supported(&self) -> Vec<GxAttribute> {
        let mut out = Vec::with_capacity(27);

        for a in GxAttribute::iter() {
            if a == GxAttribute::MaxAttr {
                break;
            }

            if self.is_set(a) {
                out.push(a);
            }
        }

        out
    }

    /// Only call when attributes are updated
    pub fn calc_descriptor(&mut self) {
        let mut desc = 0;
        for attr in GxAttribute::iter() {
            if attr == GxAttribute::MaxAttr {
                break;
            }
            if self
                .attributes
                .get(&attr)
                .is_some_and(|k| *k != GxAttributeKind::None)
            {
                desc |= 1 << (attr as u8);
            }
        }
        self.descriptor = desc;
    }
}

impl Debug for VertexDescriptor {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("VertexDescriptor")
            .field("attributes", &self.attributes)
            .field("descriptor", &format_args!("{:032b}", self.descriptor))
            .finish()
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, TryFromPrimitive)]
#[repr(u8)]
pub enum GxAttributeKind {
    None = 0,   // No data will be sent
    Direct = 1, // Data will be sent directly
    Byte = 2,   // 8 bit ind
    Short = 3,  // 16 bit ind
}

#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub enum GxDataType {
    #[serde(rename = "Byte")]
    U8,
    #[serde(rename = "SignedByte")]
    I8,
    #[serde(rename = "Short")]
    U16,
    #[serde(rename = "SignedShort")]
    I16,
    #[serde(rename = "Float")]
    F32,

    // Only colors use these
    RGB565,
    RGB8,
    RGBX8, // The X means the 4th channel is discarded/ignored
    RGBA4,
    RGBA6,
    RGBA8,
}

impl GxDataType {
    pub fn try_from(attribute: GxAttribute, num: u32) -> Result<Self, ReadError> {
        use GxAttribute::*;

        Ok(match attribute {
            Color0 | Color1 => match num {
                0 => Self::RGB565,
                1 => Self::RGB8,
                2 => Self::RGBX8,
                3 => Self::RGBA4,
                4 => Self::RGBA6,
                5 => Self::RGBA8,
                _ => return Err(ReadError::NoKnownGxDataType((attribute, num))),
            },
            _ => match num {
                0 => Self::U8,
                1 => Self::I8,
                2 => Self::U16,
                3 => Self::I16,
                4 => Self::F32,
                _ => return Err(ReadError::NoKnownGxDataType((attribute, num))),
            },
        })
    }

    pub fn try_into_u32(&self, attribute: GxAttribute) -> Result<u32, BmdWriteError> {
        use GxAttribute::*;

        Ok(match attribute {
            Color0 | Color1 => match *self {
                Self::RGB565 => 0,
                Self::RGB8 => 1,
                Self::RGBX8 => 2,
                Self::RGBA4 => 3,
                Self::RGBA6 => 4,
                Self::RGBA8 => 5,
                _ => return Err(BmdWriteError::NoKnownGxDataType((attribute, *self))),
            },
            _ => match *self {
                Self::U8 => 0,
                Self::I8 => 1,
                Self::U16 => 2,
                Self::I16 => 3,
                Self::F32 => 4,
                _ => return Err(BmdWriteError::NoKnownGxDataType((attribute, *self))),
            },
        })
    }

    /// The size of the entire type in bytes
    pub fn size(&self) -> u32 {
        match self {
            GxDataType::U8 | GxDataType::I8 => 1,
            GxDataType::U16 | GxDataType::I16 => 2,
            GxDataType::F32 => 4,

            GxDataType::RGB565 | GxDataType::RGBA4 => 2,
            GxDataType::RGB8 | GxDataType::RGBX8 | GxDataType::RGBA6 | GxDataType::RGBA8 => 4,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub enum GxComponentType {
    PositionXY,
    PositionXYZ,

    NormalXYZ,
    NormalNBT, // todo never seen used so may break
    // NormalNBT3, todo need more information on this type (len and such)
    ColorRGB,
    ColorRGBA, // todo I don't think this is ever used?

    TexCoordS,
    TexCoordST,
}

impl GxComponentType {
    pub fn try_from(attribute: GxAttribute, num: u32) -> Result<Self, ReadError> {
        use GxAttribute::*;

        let pair = (attribute, num);
        Ok(match pair {
            (Position, 0) => Self::PositionXY,
            (Position, 1) => Self::PositionXYZ,
            (Normal, 0) => Self::NormalXYZ,
            (Normal, 1) => Self::NormalNBT,
            (Color0 | Color1, 0) => Self::ColorRGB,
            (Color0 | Color1, 1) => Self::ColorRGBA,
            // Any Tex channel
            (Tex0 | Tex1 | Tex2 | Tex3 | Tex4 | Tex5 | Tex6 | Tex7, 0) => Self::TexCoordS,
            (Tex0 | Tex1 | Tex2 | Tex3 | Tex4 | Tex5 | Tex6 | Tex7, 1) => Self::TexCoordST,
            (_, _) => return Err(ReadError::NoKnownGxComponentType(pair)),
        })
    }

    pub fn try_into_u32(&self, attribute: GxAttribute) -> Result<u32, BmdWriteError> {
        use GxAttribute::*;

        let pair = (attribute, *self);
        Ok(match pair {
            (Position, Self::PositionXY) => 0,
            (Position, Self::PositionXYZ) => 1,
            (Normal, Self::NormalXYZ) => 0,
            (Normal, Self::NormalNBT) => 1,
            (Color0 | Color1, Self::ColorRGB) => 0,
            (Color0 | Color1, Self::ColorRGBA) => 1,
            // Any Tex channel
            (Tex0 | Tex1 | Tex2 | Tex3 | Tex4 | Tex5 | Tex6 | Tex7, Self::TexCoordS) => 0,
            (Tex0 | Tex1 | Tex2 | Tex3 | Tex4 | Tex5 | Tex6 | Tex7, Self::TexCoordST) => 1,
            (_, _) => return Err(BmdWriteError::NoKnownGxComponentType(pair)),
        })
    }

    /// The number of channels for the current type. eg. `XYZ = 3`.
    ///
    /// Somewhat counterintuitively, for color types, this is 1,
    /// because they are often packed differently.
    ///
    /// Often this should be multiplied with GxDataType::size() to get
    /// the full size taken by a datatype with a certain component type.
    pub fn channels(&self) -> u32 {
        match self {
            GxComponentType::PositionXY => 2,
            GxComponentType::PositionXYZ => 3,
            GxComponentType::NormalXYZ => 3,
            GxComponentType::NormalNBT => 3 * 3, // todo: I believe this is 3 vectors of 3?
            GxComponentType::ColorRGB => 1, // The colors are weird, since their size is determined
            GxComponentType::ColorRGBA => 1, // by the size of the channels which varies
            GxComponentType::TexCoordS => 1,
            GxComponentType::TexCoordST => 2,
        }
    }
}

#[derive(Debug, Clone)]
pub enum DataVec {
    Float(Vec<f32>),
    Vec2(Vec<Vec2>),
    Vec3(Vec<Vec3A>),
    Color(Vec<Color>),
}

impl DataVec {
    pub fn accessor_type(&self) -> AccessorType {
        match self {
            DataVec::Float(_) => AccessorType::Scalar,
            DataVec::Vec2(_) => AccessorType::Vec2,
            DataVec::Vec3(_) => AccessorType::Vec3,
            DataVec::Color(_) => AccessorType::Vec4,
        }
    }

    pub fn component_type(&self) -> AccessorComponentType {
        // It's all floats for now
        AccessorComponentType::Float
    }

    pub fn len(&self) -> usize {
        match self {
            DataVec::Float(v) => v.len(),
            DataVec::Vec2(v) => v.len(),
            DataVec::Vec3(v) => v.len(),
            DataVec::Color(v) => v.len(),
        }
    }

    pub fn bytes(&self) -> Vec<u8> {
        match self {
            DataVec::Float(v) => v
                .iter()
                .map(|f| f.to_le_bytes())
                .flatten()
                .collect::<Vec<_>>(),
            DataVec::Vec2(v) => v
                .iter()
                .map(|v| [v.x.to_le_bytes(), v.y.to_le_bytes()])
                .flatten()
                .flatten()
                .collect::<Vec<_>>(),
            DataVec::Vec3(v) => v
                .iter()
                .map(|v| [v.x.to_le_bytes(), v.y.to_le_bytes(), v.z.to_le_bytes()])
                .flatten()
                .flatten()
                .collect::<Vec<_>>(),
            DataVec::Color(v) => v
                .iter()
                .map(|c| {
                    let [r, g, b, a] = c.to_linear().to_f32_array();
                    [
                        r.to_le_bytes(),
                        g.to_le_bytes(),
                        b.to_le_bytes(),
                        a.to_le_bytes(),
                    ]
                })
                .flatten()
                .flatten()
                .collect::<Vec<_>>(),
        }
    }

    pub fn write_data<W: Write + Seek>(
        &self,
        w: &mut W,
        format: &AttributeFormat,
    ) -> Result<(), BmdWriteError> {
        match self {
            DataVec::Float(data) => write_float_data(w, data, format)?,
            DataVec::Vec2(data) => write_vec2_data(w, data, format)?,
            DataVec::Vec3(data) => write_vec3_data(w, data, format)?,
            DataVec::Color(data) => write_color_data(w, data, format)?,
        }
        Ok(())
    }
}

impl Display for DataVec {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            DataVec::Float(_) => write!(f, "Float"),
            DataVec::Vec2(_) => write!(f, "Vec2"),
            DataVec::Vec3(_) => write!(f, "Vec3"),
            DataVec::Color(_) => write!(f, "Color"),
        }
    }
}

#[derive(
    Debug,
    Copy,
    Clone,
    PartialEq,
    Eq,
    PartialOrd,
    Hash,
    TryFromPrimitive,
    EnumIter,
    Serialize,
    Deserialize,
)]
#[repr(u8)]
pub enum GxAttribute {
    PositionNormalMatrixInd = 0,
    TexMatrix0 = 1,
    TexMatrix1 = 2,
    TexMatrix2 = 3,
    TexMatrix3 = 4,
    TexMatrix4 = 5,
    TexMatrix5 = 6,
    TexMatrix6 = 7,
    TexMatrix7 = 8,
    Position = 9,
    Normal = 10,
    Color0 = 11,
    Color1 = 12,
    Tex0 = 13,
    Tex1 = 14,
    Tex2 = 15,
    Tex3 = 16,
    Tex4 = 17,
    Tex5 = 18,
    Tex6 = 19,
    Tex7 = 20,
    PositionMatrixArray = 21,
    NormalMatrixArray = 22,
    TextureMatrixArray = 23,
    LitMatrixArray = 24,
    // NBT = normal binormal tangent
    NBT = 25,
    MaxAttr = 26,

    Null = 255,
}

impl GxAttribute {
    pub fn supports_direct(&self) -> bool {
        match self {
            GxAttribute::PositionNormalMatrixInd
            | GxAttribute::TexMatrix0
            | GxAttribute::TexMatrix1 => true,
            _ => false,
        }
    }

    pub fn data_offset_index(&self) -> Result<usize, ReadError> {
        Ok(match self {
            GxAttribute::Position => 0,
            GxAttribute::Normal => 1,
            GxAttribute::NBT => 2, // todo: is this right? (also in vertex_data_types)
            GxAttribute::Color0 => 3,
            GxAttribute::Color1 => 4,
            GxAttribute::Tex0 => 5,
            GxAttribute::Tex1 => 6,
            GxAttribute::Tex2 => 7,
            GxAttribute::Tex3 => 8,
            GxAttribute::Tex4 => 9,
            GxAttribute::Tex5 => 10,
            GxAttribute::Tex6 => 11,
            GxAttribute::Tex7 => 12,
            _ => return Err(ReadError::NoKnownDataOffsetIndex(*self)),
        })
    }

    pub const fn vertex_data_types() -> [Self; 13] {
        [
            GxAttribute::Position,
            GxAttribute::Normal,
            GxAttribute::NBT,
            GxAttribute::Color0,
            GxAttribute::Color1,
            GxAttribute::Tex0,
            GxAttribute::Tex1,
            GxAttribute::Tex2,
            GxAttribute::Tex3,
            GxAttribute::Tex4,
            GxAttribute::Tex5,
            GxAttribute::Tex6,
            GxAttribute::Tex7,
        ]
    }

    // Vanilla investigations:
    // position is always short
    // normal is always short
    // tex0 is always short
    // tex1 is always short
    // tex2 is always short
    // color0 is always short
    // color1 is always short
    // PositionNormalMatrixInd is always Direct
    /// Only some attributes need kinds. This will return None
    /// if the attribute does not need a kind (or perhaps it's
    /// not known)
    pub fn default_kind(&self) -> Option<GxAttributeKind> {
        match self {
            GxAttribute::PositionNormalMatrixInd => Some(GxAttributeKind::Direct),
            GxAttribute::Position => Some(GxAttributeKind::Short),
            GxAttribute::Normal => Some(GxAttributeKind::Short),
            GxAttribute::Color0 | GxAttribute::Color1 => Some(GxAttributeKind::Short),
            GxAttribute::Tex0
            | GxAttribute::Tex1
            | GxAttribute::Tex2
            | GxAttribute::Tex3
            | GxAttribute::Tex4
            | GxAttribute::Tex5
            | GxAttribute::Tex6
            | GxAttribute::Tex7 => Some(GxAttributeKind::Short),
            _ => None,
        }
    }

    pub fn load_data<R: Read + Seek>(
        &self,
        buf: &mut R,
        offset: u32,
        count: u32,
        format: &AttributeFormat,
    ) -> Result<DataVec, ReadError> {
        buf.seek(SeekFrom::Start(offset as u64))?;

        Ok(match self {
            GxAttribute::Position => match format.component_type {
                GxComponentType::PositionXY => load_vec2_data(buf, count, format)?,
                GxComponentType::PositionXYZ => load_vec3_data(buf, count, format)?,
                _ => unreachable!(),
            },
            GxAttribute::Normal => match format.component_type {
                GxComponentType::NormalXYZ => load_vec3_data(buf, count, format)?,
                GxComponentType::NormalNBT => unimplemented!(),
                _ => unreachable!(),
            },
            GxAttribute::Color0 | GxAttribute::Color1 => load_color_data(buf, count, format)?,
            GxAttribute::Tex0
            | GxAttribute::Tex1
            | GxAttribute::Tex2
            | GxAttribute::Tex3
            | GxAttribute::Tex4
            | GxAttribute::Tex5
            | GxAttribute::Tex6
            | GxAttribute::Tex7 => match format.component_type {
                GxComponentType::TexCoordS => load_float_data(buf, count, format)?,
                GxComponentType::TexCoordST => load_vec2_data(buf, count, format)?,
                _ => unreachable!(),
            },
            _ => {
                return Err(ReadError::GxAttributeTypeNotImplemented(*self));
            }
        })
    }
}

impl Ord for GxAttribute {
    fn cmp(&self, other: &Self) -> Ordering {
        // kinda dumb
        (*self as u8).cmp(&(*other as u8))
    }
}

impl Display for GxAttribute {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        // some of these may also be errors but I just don't know
        write!(
            f,
            "{}",
            match self {
                GxAttribute::PositionNormalMatrixInd => "PNMI",
                GxAttribute::TexMatrix0 => "TEX_MATRIX_0",
                GxAttribute::TexMatrix1 => "TEX_MATRIX_1",
                GxAttribute::TexMatrix2 => "TEX_MATRIX_2",
                GxAttribute::TexMatrix3 => "TEX_MATRIX_3",
                GxAttribute::TexMatrix4 => "TEX_MATRIX_4",
                GxAttribute::TexMatrix5 => "TEX_MATRIX_5",
                GxAttribute::TexMatrix6 => "TEX_MATRIX_6",
                GxAttribute::TexMatrix7 => "TEX_MATRIX_7",
                GxAttribute::Position => "POSITION",
                GxAttribute::Normal => "NORMAL",
                GxAttribute::Color0 => "COLOR_0",
                GxAttribute::Color1 => "COLOR_1",
                GxAttribute::Tex0 => "TEXCOORD_0",
                GxAttribute::Tex1 => "TEXCOORD_1",
                GxAttribute::Tex2 => "TEXCOORD_2",
                GxAttribute::Tex3 => "TEXCOORD_3",
                GxAttribute::Tex4 => "TEXCOORD_4",
                GxAttribute::Tex5 => "TEXCOORD_5",
                GxAttribute::Tex6 => "TEXCOORD_6",
                GxAttribute::Tex7 => "TEXCOORD_7",
                GxAttribute::PositionMatrixArray => "POSITION_MATRIX_ARRAY",
                GxAttribute::NormalMatrixArray => "NORMAL_MATRIX_ARRAY",
                GxAttribute::TextureMatrixArray => "TEXTURE_MATRIX_ARRAY",
                GxAttribute::LitMatrixArray => "LIT_MATRIX_ARRAY",
                GxAttribute::NBT => "NBT",
                GxAttribute::MaxAttr => "ERROR_0",
                GxAttribute::Null => "ERROR_1",
            }
        )
    }
}

impl FromStr for GxAttribute {
    type Err = AttributeParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "PNMI" => GxAttribute::PositionNormalMatrixInd,
            "TEX_MATRIX_0" => GxAttribute::TexMatrix0,
            "TEX_MATRIX_1" => GxAttribute::TexMatrix1,
            "TEX_MATRIX_2" => GxAttribute::TexMatrix2,
            "TEX_MATRIX_3" => GxAttribute::TexMatrix3,
            "TEX_MATRIX_4" => GxAttribute::TexMatrix4,
            "TEX_MATRIX_5" => GxAttribute::TexMatrix5,
            "TEX_MATRIX_6" => GxAttribute::TexMatrix6,
            "TEX_MATRIX_7" => GxAttribute::TexMatrix7,
            "POSITION" => GxAttribute::Position,
            "NORMAL" => GxAttribute::Normal,
            "COLOR_0" => GxAttribute::Color0,
            "COLOR_1" => GxAttribute::Color1,
            "TEXCOORD_0" => GxAttribute::Tex0,
            "TEXCOORD_1" => GxAttribute::Tex1,
            "TEXCOORD_2" => GxAttribute::Tex2,
            "TEXCOORD_3" => GxAttribute::Tex3,
            "TEXCOORD_4" => GxAttribute::Tex4,
            "TEXCOORD_5" => GxAttribute::Tex5,
            "TEXCOORD_6" => GxAttribute::Tex6,
            "TEXCOORD_7" => GxAttribute::Tex7,
            "POSITION_MATRIX_ARRAY" => GxAttribute::PositionMatrixArray,
            "NORMAL_MATRIX_ARRAY" => GxAttribute::NormalMatrixArray,
            "TEXTURE_MATRIX_ARRAY" => GxAttribute::TextureMatrixArray,
            "LIT_MATRIX_ARRAY" => GxAttribute::LitMatrixArray,
            "NBT" => GxAttribute::NBT,
            a => return Err(AttributeParseError::AttributeNotFound(a.to_string())),
        })
    }
}

fn load_float_data<R: Read>(
    buf: &mut R,
    count: u32,
    format: &AttributeFormat,
) -> Result<DataVec, ReadError> {
    let mut out = Vec::with_capacity(count as usize);
    let f: i32 = 1 << format.fractional;
    let f = f as f32;

    // Some padding will be left in but it will be ignored later
    for _ in 0..count {
        match format.data_type {
            GxDataType::U8 => {
                let a = read_u8(buf)?;
                let fa = a as f32 / f;

                out.push(fa);
            }
            GxDataType::I8 => {
                let a = read_u8(buf)? as i8;
                let fa = a as f32 / f;

                out.push(fa);
            }
            GxDataType::U16 => {
                let a = read_u16(buf)?;
                let fa = a as f32 / f;

                out.push(fa);
            }
            GxDataType::I16 => {
                let a = read_u16(buf)? as i16;
                let fa = a as f32 / f;

                out.push(fa);
            }
            GxDataType::F32 => {
                out.push(read_f32(buf)?);
            }
            _ => unreachable!(),
        }
    }

    Ok(DataVec::Float(out))
}

fn load_vec2_data<R: Read>(
    buf: &mut R,
    count: u32,
    format: &AttributeFormat,
) -> Result<DataVec, ReadError> {
    let mut out = Vec::with_capacity(count as usize);
    let f: i32 = 1 << format.fractional;
    let f = f as f32;

    // Some padding will be left in but it will be ignored later
    for _ in 0..count {
        match format.data_type {
            GxDataType::U8 => {
                let a = read_u8(buf)?;
                let b = read_u8(buf)?;
                let fa = a as f32 / f;
                let fb = b as f32 / f;

                out.push(Vec2::new(fa, fb));
            }
            GxDataType::I8 => {
                let a = read_u8(buf)? as i8;
                let b = read_u8(buf)? as i8;
                let fa = a as f32 / f;
                let fb = b as f32 / f;

                out.push(Vec2::new(fa, fb));
            }
            GxDataType::U16 => {
                let a = read_u16(buf)?;
                let b = read_u16(buf)?;
                let fa = a as f32 / f;
                let fb = b as f32 / f;

                out.push(Vec2::new(fa, fb));
            }
            GxDataType::I16 => {
                let a = read_u16(buf)? as i16;
                let b = read_u16(buf)? as i16;
                let fa = a as f32 / f;
                let fb = b as f32 / f;

                out.push(Vec2::new(fa, fb));
            }
            GxDataType::F32 => {
                out.push(Vec2::new(read_f32(buf)?, read_f32(buf)?));
            }
            _ => unreachable!(),
        }
    }

    Ok(DataVec::Vec2(out))
}

fn load_vec3_data<R: Read>(
    buf: &mut R,
    count: u32,
    format: &AttributeFormat,
) -> Result<DataVec, ReadError> {
    let mut out = Vec::with_capacity(count as usize);
    let f: i32 = 1 << format.fractional;
    let f = f as f32;

    // Some padding will be left in but it will be ignored later
    for _ in 0..count {
        match format.data_type {
            GxDataType::U8 => {
                let a = read_u8(buf)?;
                let b = read_u8(buf)?;
                let c = read_u8(buf)?;
                let fa = a as f32 / f;
                let fb = b as f32 / f;
                let fc = c as f32 / f;

                out.push(Vec3A::new(fa, fb, fc));
            }
            GxDataType::I8 => {
                let a = read_u8(buf)? as i8;
                let b = read_u8(buf)? as i8;
                let c = read_u8(buf)? as i8;
                let fa = a as f32 / f;
                let fb = b as f32 / f;
                let fc = c as f32 / f;

                out.push(Vec3A::new(fa, fb, fc));
            }
            GxDataType::U16 => {
                let a = read_u16(buf)?;
                let b = read_u16(buf)?;
                let c = read_u16(buf)?;
                let fa = a as f32 / f;
                let fb = b as f32 / f;
                let fc = c as f32 / f;

                out.push(Vec3A::new(fa, fb, fc));
            }
            GxDataType::I16 => {
                let a = read_u16(buf)? as i16;
                let b = read_u16(buf)? as i16;
                let c = read_u16(buf)? as i16;
                let fa = a as f32 / f;
                let fb = b as f32 / f;
                let fc = c as f32 / f;

                out.push(Vec3A::new(fa, fb, fc));
            }
            GxDataType::F32 => {
                out.push(Vec3A::new(read_f32(buf)?, read_f32(buf)?, read_f32(buf)?));
            }
            _ => unreachable!(),
        }
    }

    Ok(DataVec::Vec3(out))
}

// todo: linear or not? I think probably linear
fn load_color_data<R: Read>(
    buf: &mut R,
    count: u32,
    format: &AttributeFormat,
) -> Result<DataVec, ReadError> {
    let mut out = Vec::with_capacity(count as usize);

    // Some padding will be left in but it will be ignored later
    for _ in 0..count {
        match format.data_type {
            GxDataType::RGB565 => {
                let c = read_u16(buf)?;
                let r = c >> 11;
                let g = (c & 0b0000_0111_1110_0000) >> 5;
                let b = c & 0b0000_0000_0001_1111;

                out.push(Color::linear_rgb(
                    r as f32 * BIT5_TO_COLOR,
                    g as f32 * BIT6_TO_COLOR,
                    b as f32 * BIT5_TO_COLOR,
                ));
            }
            GxDataType::RGB8 | GxDataType::RGBX8 => {
                let r = read_u8(buf)?;
                let g = read_u8(buf)?;
                let b = read_u8(buf)?;
                read_u8(buf)?; // skip a

                out.push(Color::linear_rgb(
                    r as f32 * BYTE_TO_COLOR,
                    g as f32 * BYTE_TO_COLOR,
                    b as f32 * BYTE_TO_COLOR,
                ));
            }
            GxDataType::RGBA4 => {
                let c = read_u16(buf)?;
                let r = c >> 12;
                let g = (c & 0x0F00) >> 8;
                let b = (c & 0x00F0) >> 4;
                let a = c & 0x000F;

                out.push(Color::linear_rgba(
                    r as f32 * BIT4_TO_COLOR,
                    g as f32 * BIT4_TO_COLOR,
                    b as f32 * BIT4_TO_COLOR,
                    a as f32 * BIT4_TO_COLOR,
                ));
            }
            GxDataType::RGBA6 => {
                let c = read_u32(buf)?;
                let r = (c & 0xFC0000) >> 18;
                let g = (c & 0x03F000) >> 12;
                let b = (c & 0x000FC0) >> 6;
                let a = c & 0x00003F;

                out.push(Color::linear_rgba(
                    r as f32 * BIT6_TO_COLOR,
                    g as f32 * BIT6_TO_COLOR,
                    b as f32 * BIT6_TO_COLOR,
                    a as f32 * BIT6_TO_COLOR,
                ));
            }
            GxDataType::RGBA8 => {
                let r = read_u8(buf)?;
                let g = read_u8(buf)?;
                let b = read_u8(buf)?;
                let a = read_u8(buf)?;

                out.push(Color::linear_rgba(
                    r as f32 * BYTE_TO_COLOR,
                    g as f32 * BYTE_TO_COLOR,
                    b as f32 * BYTE_TO_COLOR,
                    a as f32 * BYTE_TO_COLOR,
                ));
            }
            _ => unreachable!(),
        }
    }

    Ok(DataVec::Color(out))
}

fn write_float_data<W: Write>(
    w: &mut W,
    data: &Vec<f32>,
    format: &AttributeFormat,
) -> Result<(), BmdWriteError> {
    let f: i32 = 1 << format.fractional;
    let f = f as f32;

    for a in data {
        match format.data_type {
            GxDataType::U8 => {
                let fa = a * f;
                w!(
                    w,
                    errify!(u8::try_num_from(fa), BmdWriteError::FailedCast("byte"))?
                )?;
            }
            GxDataType::I8 => {
                let fa = a * f;
                w!(
                    w,
                    errify!(
                        i8::try_num_from(fa),
                        BmdWriteError::FailedCast("signed byte")
                    )?
                )?;
            }
            GxDataType::U16 => {
                let fa = a * f;

                w!(
                    w,
                    errify!(u16::try_num_from(fa), BmdWriteError::FailedCast("short"))?
                )?;
            }
            GxDataType::I16 => {
                let fa = a * f;
                w!(
                    w,
                    errify!(
                        i16::try_num_from(fa),
                        BmdWriteError::FailedCast("signed short")
                    )?
                )?;
            }
            GxDataType::F32 => {
                w!(w, a)?;
            }
            d => {
                return Err(BmdWriteError::UnsupportedAttributeDataCombo(
                    d,
                    "float data",
                ))
            }
        }
    }

    Ok(())
}

fn write_vec2_data<W: Write>(
    w: &mut W,
    data: &Vec<Vec2>,
    format: &AttributeFormat,
) -> Result<(), BmdWriteError> {
    let f: i32 = 1 << format.fractional;
    let f = f as f32;

    // Some padding will be left in but it will be ignored later
    for v in data {
        match format.data_type {
            GxDataType::U8 => {
                let fa = v.x * f;
                let fb = v.y * f;

                w!(
                    w,
                    errify!(u8::try_num_from(fa), BmdWriteError::FailedCast("byte"))?
                )?;
                w!(
                    w,
                    errify!(u8::try_num_from(fb), BmdWriteError::FailedCast("byte"))?
                )?;
            }
            GxDataType::I8 => {
                let fa = v.x * f;
                let fb = v.y * f;

                w!(
                    w,
                    errify!(
                        i8::try_num_from(fa),
                        BmdWriteError::FailedCast("signed byte")
                    )?
                )?;
                w!(
                    w,
                    errify!(
                        i8::try_num_from(fb),
                        BmdWriteError::FailedCast("signed byte")
                    )?
                )?;
            }
            GxDataType::U16 => {
                let fa = v.x * f;
                let fb = v.y * f;

                w!(
                    w,
                    errify!(u16::try_num_from(fa), BmdWriteError::FailedCast("short"))?
                )?;
                w!(
                    w,
                    errify!(u16::try_num_from(fb), BmdWriteError::FailedCast("short"))?
                )?;
            }
            GxDataType::I16 => {
                let fa = v.x * f;
                let fb = v.y * f;

                w!(
                    w,
                    errify!(
                        i16::try_num_from(fa),
                        BmdWriteError::FailedCast("signed short")
                    )?
                )?;
                w!(
                    w,
                    errify!(
                        i16::try_num_from(fb),
                        BmdWriteError::FailedCast("signed short")
                    )?
                )?;
            }
            GxDataType::F32 => {
                w!(w, v.x)?;
                w!(w, v.y)?;
            }
            d => return Err(BmdWriteError::UnsupportedAttributeDataCombo(d, "XY data")),
        }
    }

    Ok(())
}

fn write_vec3_data<W: Write>(
    w: &mut W,
    data: &Vec<Vec3A>,
    format: &AttributeFormat,
) -> Result<(), BmdWriteError> {
    let f: i32 = 1 << format.fractional;
    let f = f as f32;

    // Some padding will be left in but it will be ignored later
    for v in data {
        match format.data_type {
            GxDataType::U8 => {
                let fa = v.x * f;
                let fb = v.y * f;
                let fc = v.z * f;

                w!(
                    w,
                    errify!(u8::try_num_from(fa), BmdWriteError::FailedCast("byte"))?
                )?;
                w!(
                    w,
                    errify!(u8::try_num_from(fb), BmdWriteError::FailedCast("byte"))?
                )?;
                w!(
                    w,
                    errify!(u8::try_num_from(fc), BmdWriteError::FailedCast("byte"))?
                )?;
            }
            GxDataType::I8 => {
                let fa = v.x * f;
                let fb = v.y * f;
                let fc = v.z * f;

                w!(
                    w,
                    errify!(
                        i8::try_num_from(fa),
                        BmdWriteError::FailedCast("signed byte")
                    )?
                )?;
                w!(
                    w,
                    errify!(
                        i8::try_num_from(fb),
                        BmdWriteError::FailedCast("signed byte")
                    )?
                )?;
                w!(
                    w,
                    errify!(
                        i8::try_num_from(fc),
                        BmdWriteError::FailedCast("signed byte")
                    )?
                )?;
            }
            GxDataType::U16 => {
                let fa = v.x * f;
                let fb = v.y * f;
                let fc = v.z * f;

                w!(
                    w,
                    errify!(u16::try_num_from(fa), BmdWriteError::FailedCast("short"))?
                )?;
                w!(
                    w,
                    errify!(u16::try_num_from(fb), BmdWriteError::FailedCast("short"))?
                )?;
                w!(
                    w,
                    errify!(u16::try_num_from(fc), BmdWriteError::FailedCast("short"))?
                )?;
            }
            GxDataType::I16 => {
                let fa = v.x * f;
                let fb = v.y * f;
                let fc = v.z * f;

                w!(
                    w,
                    errify!(
                        i16::try_num_from(fa),
                        BmdWriteError::FailedCast("signed short")
                    )?
                )?;
                w!(
                    w,
                    errify!(
                        i16::try_num_from(fb),
                        BmdWriteError::FailedCast("signed short")
                    )?
                )?;
                w!(
                    w,
                    errify!(
                        i16::try_num_from(fc),
                        BmdWriteError::FailedCast("signed short")
                    )?
                )?;
            }
            GxDataType::F32 => {
                w!(w, v.x)?;
                w!(w, v.y)?;
                w!(w, v.z)?;
            }
            d => return Err(BmdWriteError::UnsupportedAttributeDataCombo(d, "XYZ data")),
        }
    }

    Ok(())
}

// todo: linear or not? I think probably linear
fn write_color_data<W: Write>(
    w: &mut W,
    data: &Vec<Color>,
    format: &AttributeFormat,
) -> Result<(), BmdWriteError> {
    // Some padding will be left in but it will be ignored later
    // todo maybe bit masking?
    for color in data {
        let [r, g, b, a] = color.to_linear().to_f32_array();
        match format.data_type {
            GxDataType::RGB565 => {
                let mut c = (b * COLOR_TO_BIT5) as u16;
                c |= ((g * COLOR_TO_BIT5) as u16) << 5;
                c |= ((r * COLOR_TO_BIT5) as u16) << 11;

                w!(w, c)?;
            }
            GxDataType::RGB8 | GxDataType::RGBX8 => {
                let r = (r * COLOR_TO_BYTE) as u8;
                let g = (g * COLOR_TO_BYTE) as u8;
                let b = (b * COLOR_TO_BYTE) as u8;

                wraw!(w, [r, g, b, 0])?;
            }
            GxDataType::RGBA4 => {
                let mut c = (a * COLOR_TO_BIT4) as u16;
                c |= ((b * COLOR_TO_BIT4) as u16) << 4;
                c |= ((g * COLOR_TO_BIT4) as u16) << 8;
                c |= ((r * COLOR_TO_BIT4) as u16) << 12;

                w!(w, c)?;
            }
            GxDataType::RGBA6 => {
                let mut c = (a * COLOR_TO_BIT6) as u32;
                c |= ((b * COLOR_TO_BIT6) as u32) << 6;
                c |= ((g * COLOR_TO_BIT6) as u32) << 12;
                c |= ((r * COLOR_TO_BIT6) as u32) << 18;

                w!(w, c)?;
            }
            GxDataType::RGBA8 => {
                let r = (r * COLOR_TO_BYTE) as u8;
                let g = (g * COLOR_TO_BYTE) as u8;
                let b = (b * COLOR_TO_BYTE) as u8;
                let a = (a * COLOR_TO_BYTE) as u8;

                wraw!(w, [r, g, b, a])?;
            }
            d => {
                return Err(BmdWriteError::UnsupportedAttributeDataCombo(
                    d,
                    "color data",
                ))
            }
        }
    }

    Ok(())
}

#[derive(Debug, Clone, PartialEq)]
pub struct VertexWeights {
    pub weights: Vec<Weight>,
}

impl VertexWeights {
    pub fn new(capacity: usize) -> Self {
        Self {
            weights: Vec::with_capacity(capacity),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Weight {
    pub weight: f32,
    pub bone_ind: u16,
}

impl Weight {
    pub fn new(weight: f32, bone_ind: u16) -> Self {
        Self { weight, bone_ind }
    }
}
