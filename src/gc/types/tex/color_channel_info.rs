use std::io::{Read, Seek, SeekFrom, Write};

#[allow(unused)] // Flags is false positive
use bitflags::Flags;
use serde::{Deserialize, Serialize};

use crate::{
    bytes::read_u8,
    gc::{
        bmd::error::BmdWriteError,
        error::ReadError,
        types::tex::{AttenuationFn, ColorSrc, DiffuseFn, LightMask},
    },
    try_from_repr, w, wbool,
};

#[derive(Debug, Copy, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct ColorChannelInfo {
    pub enabled: bool,
    pub mat_src_color: ColorSrc,
    pub light_mask: LightMask,
    pub diffuse_fn: DiffuseFn,
    pub attenuation_fn: AttenuationFn,
    pub ambient_src_color: ColorSrc,
}

impl ColorChannelInfo {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let enabled = read_u8(buf)?;
        let mat_src = read_u8(buf)?;
        let light_mask = read_u8(buf)?;
        let diffuse_fn = read_u8(buf)?;
        let attenuation_fn = read_u8(buf)?;
        let ambient_src = read_u8(buf)?;

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        Ok(Self {
            enabled: enabled != 0,
            mat_src_color: try_from_repr!(mat_src => ColorSrc)?,
            light_mask: LightMask::from_bits_retain(light_mask),
            diffuse_fn: try_from_repr!(diffuse_fn => DiffuseFn)?,
            attenuation_fn: try_from_repr!(attenuation_fn => AttenuationFn)?,
            ambient_src_color: try_from_repr!(ambient_src => ColorSrc)?,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        wbool!(w, self.enabled)?;
        w!(w, self.mat_src_color as u8)?;
        w!(w, self.light_mask.bits())?;
        w!(w, self.diffuse_fn as u8)?;
        w!(w, self.attenuation_fn as u8)?;
        w!(w, self.ambient_src_color as u8)?;

        w!(w, 0xFFFFu16)?;

        Ok(())
    }
}
