use serde::{Deserialize, Serialize};
use std::io::{Read, Seek, SeekFrom, Write};

use crate::{
    bytes::{read_f32, read_u8},
    gc::{bmd::error::BmdWriteError, error::ReadError},
    util::{get_mat2x3_rows, new_mat2x3_rows, Mat3x2},
    w, wraw,
};

#[derive(Debug, Copy, Clone, Default, Serialize, Deserialize)]
pub struct IndirectTexMatrix {
    pub mat2x3: Mat3x2,
    /// The exponent (of 2) to multiply the matrix by
    pub exp: u8,
}

impl IndirectTexMatrix {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        #[rustfmt::skip]
        let mat2x3 = new_mat2x3_rows([
            read_f32(buf)?, read_f32(buf)?, read_f32(buf)?,
            read_f32(buf)?, read_f32(buf)?, read_f32(buf)?,
        ]);
        let exp = read_u8(buf)?;

        // Skip padding (byte + short)
        buf.seek(SeekFrom::Current(3))?;

        Ok(Self { mat2x3, exp })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        let rows = get_mat2x3_rows(&self.mat2x3);
        for r in rows {
            w!(w, r)?;
        }
        w!(w, self.exp)?;

        wraw!(w, [0xFF, 0xFF, 0xFF])?;

        Ok(())
    }
}
