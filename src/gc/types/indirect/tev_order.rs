use serde::{Deserialize, Serialize};
use std::io::{Read, Seek, SeekFrom, Write};

use crate::{
    bytes::read_u8,
    gc::{
        bmd::error::BmdWriteError,
        error::ReadError,
        types::tex::{TexCoordId, TexMapId},
    },
    try_from_repr, w,
};

#[derive(Debug, Copy, Clone, Default, Serialize, Deserialize)]
pub struct IndirectTevOrder {
    pub tex_coord: TexCoordId,
    pub tex_map: TexMapId,
}

impl IndirectTevOrder {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let tex_coord = read_u8(buf)?;
        let tex_map = read_u8(buf)?;

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        Ok(Self {
            tex_coord: try_from_repr!(tex_coord => TexCoordId)?,
            tex_map: try_from_repr!(tex_map => TexMapId)?,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, self.tex_coord as u8)?;
        w!(w, self.tex_map as u8)?;

        w!(w, 0xFFFFu16)?;

        Ok(())
    }
}
