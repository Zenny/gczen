use std::io::{Read, Seek, SeekFrom, Write};

use serde::{Deserialize, Serialize};

use crate::{
    bytes::read_u8,
    gc::{bmd::error::BmdWriteError, error::ReadError},
    w,
};

/// GXSetTevSwapMode(GXTevStageID stage, GXTevSwapSel ras_sel, GXTevSwapSel tex_sel)
/// For each TEV Stage you can select an entry from the GXSetTevSwapModeTable for the rasterized
/// input and the texture color input. The texture color inputs and the rasterized color inputs
/// can be swapped, and a color channel can be selected.
#[derive(Debug, Copy, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct TevSwapMode {
    /// Raster
    pub ras_sel: u8,
    /// Texture
    pub tex_sel: u8,
}

impl TevSwapMode {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let ras_sel = read_u8(buf)?;
        let tex_sel = read_u8(buf)?;

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        Ok(Self { ras_sel, tex_sel })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, self.ras_sel)?;
        w!(w, self.tex_sel)?;

        w!(w, 0xFFFFu16)?;

        Ok(())
    }
}
