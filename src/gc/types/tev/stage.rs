use std::io::{Read, Seek, SeekFrom, Write};

use serde::{Deserialize, Serialize};

use crate::{
    bytes::read_u8,
    gc::{
        bmd::error::BmdWriteError,
        error::ReadError,
        types::tev::{
            CombineAlphaInput, CombineColorInput, TevBias, TevOp, TevRegisterId, TevScale,
        },
    },
    try_from_repr, w, wbool,
};

#[derive(Debug, Copy, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct TevStage {
    pub color_in_a: CombineColorInput,
    pub color_in_b: CombineColorInput,
    pub color_in_c: CombineColorInput,
    pub color_in_d: CombineColorInput,

    pub color_op: TevOp,
    pub color_bias: TevBias,
    pub color_scale: TevScale,
    pub color_clamp: bool,
    pub color_reg_id: TevRegisterId,

    pub alpha_in_a: CombineAlphaInput,
    pub alpha_in_b: CombineAlphaInput,
    pub alpha_in_c: CombineAlphaInput,
    pub alpha_in_d: CombineAlphaInput,

    pub alpha_op: TevOp,
    pub alpha_bias: TevBias,
    pub alpha_scale: TevScale,
    pub alpha_clamp: bool,
    pub alpha_reg_id: TevRegisterId,
}

impl TevStage {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        // Skip padding
        buf.seek(SeekFrom::Current(1))?;

        let color_a = read_u8(buf)?;
        let color_b = read_u8(buf)?;
        let color_c = read_u8(buf)?;
        let color_d = read_u8(buf)?;

        let color_op = read_u8(buf)?;
        let color_bias = read_u8(buf)?;
        let color_scale = read_u8(buf)?;
        let color_clamp = read_u8(buf)? != 0;
        let color_reg = read_u8(buf)?;

        let alpha_a = read_u8(buf)?;
        let alpha_b = read_u8(buf)?;
        let alpha_c = read_u8(buf)?;
        let alpha_d = read_u8(buf)?;

        let alpha_op = read_u8(buf)?;
        let alpha_bias = read_u8(buf)?;
        let alpha_scale = read_u8(buf)?;
        let alpha_clamp = read_u8(buf)? != 0;
        let alpha_reg = read_u8(buf)?;

        // Skip padding
        buf.seek(SeekFrom::Current(1))?;

        Ok(Self {
            color_in_a: try_from_repr!(color_a => CombineColorInput)?,
            color_in_b: try_from_repr!(color_b => CombineColorInput)?,
            color_in_c: try_from_repr!(color_c => CombineColorInput)?,
            color_in_d: try_from_repr!(color_d => CombineColorInput)?,
            color_op: try_from_repr!(color_op => TevOp)?,
            color_bias: try_from_repr!(color_bias => TevBias)?,
            color_scale: try_from_repr!(color_scale => TevScale)?,
            color_clamp,
            color_reg_id: try_from_repr!(color_reg => TevRegisterId)?,
            alpha_in_a: try_from_repr!(alpha_a => CombineAlphaInput)?,
            alpha_in_b: try_from_repr!(alpha_b => CombineAlphaInput)?,
            alpha_in_c: try_from_repr!(alpha_c => CombineAlphaInput)?,
            alpha_in_d: try_from_repr!(alpha_d => CombineAlphaInput)?,
            alpha_op: try_from_repr!(alpha_op => TevOp)?,
            alpha_bias: try_from_repr!(alpha_bias => TevBias)?,
            alpha_scale: try_from_repr!(alpha_scale => TevScale)?,
            alpha_clamp,
            alpha_reg_id: try_from_repr!(alpha_reg => TevRegisterId)?,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, 0xFFu8)?;

        w!(w, self.color_in_a as u8)?;
        w!(w, self.color_in_b as u8)?;
        w!(w, self.color_in_c as u8)?;
        w!(w, self.color_in_d as u8)?;

        w!(w, self.color_op as u8)?;
        w!(w, self.color_bias as u8)?;
        w!(w, self.color_scale as u8)?;
        wbool!(w, self.color_clamp)?;
        w!(w, self.color_reg_id as u8)?;

        w!(w, self.alpha_in_a as u8)?;
        w!(w, self.alpha_in_b as u8)?;
        w!(w, self.alpha_in_c as u8)?;
        w!(w, self.alpha_in_d as u8)?;

        w!(w, self.alpha_op as u8)?;
        w!(w, self.alpha_bias as u8)?;
        w!(w, self.alpha_scale as u8)?;
        wbool!(w, self.alpha_clamp)?;
        w!(w, self.alpha_reg_id as u8)?;

        w!(w, 0xFFu8)?;

        Ok(())
    }
}
