use clap::ValueEnum;
use mugl::{AddressMode, FilterMode};
use mugltf::MinFilterMode;
use num_enum::TryFromPrimitive;
use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize, ValueEnum)]
#[repr(u8)]
pub enum JUTTransparency {
    /// No transparency
    #[default]
    Opaque = 0,
    /// Only fully transparent pixels
    Clip = 1,
    /// Partial transparency
    Translucent = 2,
    /// BMD models use this
    // todo
    Bmd = 0xCC,
}

#[derive(Debug, Copy, Clone, PartialEq, Default, TryFromPrimitive, Serialize, Deserialize, ValueEnum)]
#[repr(u8)]
pub enum Anisotropy {
    #[default]
    X1 = 0,
    X2 = 1,
    X4 = 3,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize, ValueEnum)]
#[repr(u8)]
pub enum TextureFormat {
    /// 1 channel (gray), 4 bits per channel
    I4 = 0,
    /// 1 channel (gray), 8 bits per channel
    I8 = 1,
    /// 2 channels (gray, alpha), 4 bits per channel
    IA4 = 2,
    /// 2 channels (gray, alpha), 8 bits per channel
    IA8 = 3,
    /// 3 channels (rgb), 5 bit red/blue, 6 bit green
    #[default]
    RGB565 = 4,
    /// 4 channels (rgba), 5 bit color, 3 bit alpha
    RGB5A3 = 5,
    /// 4 channels (rgba), 8 bits per channel
    RGBA8 = 6,

    C4 = 8,
    /// Palette-indexed colors, 8 bit index
    C8 = 9,
    C14X2 = 0xA,
    /// Block-compressed colors
    Compressed = 0xE,

    Z8 = 0x11,
    Z16 = 0x13,
    Z24X8 = 0x16,

    CTFR4 = 0x20,
    CTFRA4 = 0x22,
    CTFRA8 = 0x23,
    CTFYUVA8 = 0x26,
    CTFA8 = 0x27,
    CTFR8 = 0x28,
    CTFG8 = 0x29,
    CTFB8 = 0x2A,
    CTFRG8 = 0x2B,
    CTFGB8 = 0x2C,

    CTFZ4 = 0x30,
    CTFZ8M = 0x39,
    CTFZ8L = 0x3A,
    CTFZ16L = 0x3C,
    //ExtensionRawRgba32 = 0xFF
}

impl TextureFormat {
    pub fn use_palette(&self) -> bool {
        match self {
            TextureFormat::C4 | TextureFormat::C8 | TextureFormat::C14X2 => true,
            _ => false,
        }
    }

    pub fn calc_data_size(&self, width: u32, height: u32) -> u32 {
        let blocks_wide = (width + self.block_width() - 1) / self.block_width();
        let blocks_tall = (height + self.block_height() - 1) / self.block_height();

        blocks_wide * blocks_tall * self.block_data_size()
    }

    // Based on the size in bytes within the actual type (?)
    pub const fn block_width(&self) -> u32 {
        match self {
            // u16
            TextureFormat::IA8
            | TextureFormat::RGB565
            | TextureFormat::RGB5A3
            | TextureFormat::RGBA8
            | TextureFormat::C14X2 => 4,
            // u8
            TextureFormat::I4
            | TextureFormat::I8
            | TextureFormat::IA4
            | TextureFormat::C4
            | TextureFormat::C8
            | TextureFormat::Compressed => 8,
            _ => unimplemented!(),
        }
    }

    pub const fn block_height(&self) -> u32 {
        match self {
            TextureFormat::I8
            | TextureFormat::IA4
            | TextureFormat::IA8
            | TextureFormat::RGB565
            | TextureFormat::RGB5A3
            | TextureFormat::RGBA8
            | TextureFormat::C8
            | TextureFormat::C14X2 => 4,
            // This is because it's 2 bytes per 1 (?) (Not sure about compressed). Could maybe be
            // 16 width and 4 height instead
            TextureFormat::I4 | TextureFormat::C4 | TextureFormat::Compressed => 8,
            _ => unimplemented!(),
        }
    }

    pub const fn block_data_size(&self) -> u32 {
        match self {
            TextureFormat::I4
            | TextureFormat::I8
            | TextureFormat::IA4
            | TextureFormat::IA8
            | TextureFormat::C4
            | TextureFormat::C8
            | TextureFormat::C14X2
            | TextureFormat::RGB565
            | TextureFormat::RGB5A3
            | TextureFormat::Compressed => 32,
            TextureFormat::RGBA8 => 64,
            _ => unimplemented!(),
        }
    }

    /// Number of decodes to complete a block (Depends on underlying datatype size)
    pub const fn num_iters(&self) -> u32 {
        match self {
            TextureFormat::I4
            | TextureFormat::I8
            | TextureFormat::IA4
            | TextureFormat::C4
            | TextureFormat::C8 => 32,
            TextureFormat::IA8
            | TextureFormat::RGB565
            | TextureFormat::RGB5A3
            | TextureFormat::C14X2 => 16,
            TextureFormat::RGBA8 => 1,
            TextureFormat::Compressed => 4,
            _ => unimplemented!(),
        }
    }

    pub const fn num_colors(&self) -> usize {
        match self {
            TextureFormat::C4 => 16,
            TextureFormat::C8 => 256,
            TextureFormat::C14X2 => 16384,
            _ => unimplemented!(),
        }
    }
}

#[derive(
    Debug, Copy, Clone, Default, PartialEq, Eq, Hash, TryFromPrimitive, Serialize, Deserialize, ValueEnum,
)]
#[repr(u8)]
pub enum TextureWrap {
    Clamp = 0,
    #[default]
    Repeat = 1,
    Mirror = 2,
}

impl TextureWrap {
    pub fn to_mugltf_wrap(&self) -> AddressMode {
        match self {
            TextureWrap::Clamp => AddressMode::ClampToEdge,
            TextureWrap::Repeat => AddressMode::Repeat,
            TextureWrap::Mirror => AddressMode::MirrorRepeat,
        }
    }
}

#[derive(
    Debug, Copy, Clone, Default, PartialEq, Eq, Hash, TryFromPrimitive, Serialize, Deserialize, ValueEnum,
)]
#[repr(u8)]
pub enum TextureFilter {
    /// Point sampling, no mipmap
    Near = 0,
    /// Bilinear filtering, no mipmap
    #[default]
    Linear = 1,
    /// Point sampling, discrete mipmap
    NearMipNear = 2,
    /// Bilinear filtering, discrete mipmap
    LinearMipNear = 3,
    /// Point sampling, linear mipmap
    NearMipLinear = 4,
    /// Trilinear filtering
    LinearMipLinear = 5,
}

impl TextureFilter {
    pub fn to_mugltf_mag_filter(&self) -> FilterMode {
        match self {
            TextureFilter::Near => FilterMode::Nearest,
            TextureFilter::Linear => FilterMode::Linear,
            v => panic!("mag filter is strange value: {:?}", v),
        }
    }

    pub fn to_mugltf_min_filter(&self) -> MinFilterMode {
        match self {
            TextureFilter::Near => MinFilterMode::Nearest,
            TextureFilter::Linear => MinFilterMode::Linear,
            TextureFilter::NearMipNear => MinFilterMode::NearestMipmapNearest,
            TextureFilter::LinearMipNear => MinFilterMode::LinearMipmapNearest,
            TextureFilter::NearMipLinear => MinFilterMode::NearestMipmapLinear,
            TextureFilter::LinearMipLinear => MinFilterMode::LinearMipmapLinear,
        }
    }
}

/// The C4, C8, C14X2, and CMPR image formats all use palettes to store color data. For C4, C8, and
/// C14X2, the palette format will be specified in the PaletteFormat variable in the image's header
/// CMPR stores palettes within the compressed blocks of image data.
#[derive(Debug, Copy, Clone, PartialEq, Default, TryFromPrimitive, Serialize, Deserialize, ValueEnum)]
#[repr(u8)]
pub enum PaletteFormat {
    /// 2 channels (gray, alpha), 8 bits per channel
    IA8 = 0,
    /// 3 channels (rgb), 5 bit red/blue, 6 bit green
    #[default]
    RGB565 = 1,
    /// 4 channels (rgba), 5 bit color, 3 bit alpha
    RGB5A3 = 2,
}
