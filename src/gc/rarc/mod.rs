//! How data is stored:
//!
//! In terms of types of data, MRAM first, ARAM second, DVD last. This can then be
//! identified using the RarcHeader's fields, and loaded appropriately.
//!
//! The name table is right before the actual file data. The file data should start padded to the
//! nearest 32 byte address, so the name table should end with that padding (zeros).

use std::{
    ffi::OsString,
    cmp::Ordering,
    fs::{
        DirEntry,
        read,
        read_dir
    },
    fmt::{Debug, Formatter},
    io::{BufRead, Cursor, Read, Seek, SeekFrom, Write},
    path::{Component, PathBuf},
    collections::HashMap
};

#[allow(unused)]
use bitflags::{bitflags, Flags};
use serde::{Deserialize, Serialize};
use yaz0::{CompressionLevel, Yaz0Writer};

use crate::{
    companion::{
        bmd::BmdCompanion,
        bti::BtiCompanion,
        error::CompanionError,
        generic::GenericCompanion,
        rarc::RarcCompanion,
        rarc_entry::RarcEntryCompanion,
        COMPANION_EXT
    },
    bytes::{read_u16, read_u16_le, read_u32, read_u8},
    gc::{bmd::jnt1::hash_name, error::ReadError, file::GcFile, rarc::error::RarcCompileError},
    w,
    wat,
    wraw,
    gc::align::{AlignType, write_align}
};

pub mod error;

const RARC: u32 = 0x52415243;
const RARC_NODE_SIZE: u32 = 0x10;
const RARC_FILE_ENTRY_SIZE: u32 = 0x14;
pub const PRELOAD_TO_MRAM: &str = "Preload to MRAM";
pub const PRELOAD_TO_ARAM: &str = "Preload to ARAM";
pub const LOAD_FROM_DVD: &str = "Load from DVD";
// todo should this ever happen?
pub const LOAD_NONE: &str = "None";

pub type Reader = Cursor<Vec<u8>>;

#[derive(Clone)]
pub struct Rarc {
    /// The path to the file. This becomes the dir it's extracted to later
    pub path: PathBuf,
    /// The data offset of these files is relative to the RARC data, not the ISO data
    pub files: Vec<GcFile>,
    pub info: RarcInfo,
    pub root_name: String,
    /// Rarc data are only available if the file is stored within a Rarc file
    /// todo is a rarc within a rarc a thing?
    pub rarc_data: Option<RarcEntryCompanion>,
}

impl Debug for Rarc {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Rarc {{ ... }}")
    }
}

impl Rarc {
    pub fn read_szs(
        path: PathBuf,
        data: Vec<u8>,
        rarc_data: Option<RarcEntryCompanion>,
    ) -> Result<Self, ReadError> {
        #[cfg(debug_assertions)]
        let actual_len = data.len() as u32;

        let mut b = Cursor::new(data);

        // header
        // Check for RARC ascii at start
        let check = read_u32(&mut b)?;
        if check != RARC {
            return Err(ReadError::NotRarc);
        }

        let header = RarcHeader::new(&mut b)?;

        #[cfg(debug_assertions)]
        assert_eq!(actual_len, header.rarc_len);

        // info
        let info = RarcInfo::new(&mut b, header.data_header_offset)?;

        let root = RarcNode::new(&mut b, 0, &info)?;

        if root.ident != "ROOT" {
            return Err(ReadError::NoRoot);
        }

        let root_name = root.name.clone();
        //let root_path = path.join(&root.name);

        let files = read_node(&mut b, root, &header, &info, path.clone())?;
        // todo if other file types reference paths using this root, maybe it makes more sense to have it
        //files.insert(0, GcFile::Folder(GenericFile::new(root_path, Vec::new(), None)));

        Ok(Self {
            path,
            files,
            info,
            root_name,
            rarc_data,
        })
    }

    /// Compiles a directory as a rarc type in binary. Skips the fluff of converting all of
    /// the inner files into GcFile
    pub fn compile_dir<W: Write + Seek>(w: &mut W, path: PathBuf) -> Result<(), RarcCompileError> {
        log::debug!("Compiling dir {:?}...", path);
        w!(w, RARC)?;

        let rarc_companion = RarcCompanion::read(&path)?;

        // iterate all the files and folders, collecting their data into vecs
        // then write the headers and stuff using the data collected

        // header 1
        let mut header = RarcHeader::reserve_space(w)?;
        // header 2
        let mut info = RarcInfo::reserve_space(w)?;
        info.next_id = u16::try_from(rarc_companion.next_id)
            .map_err(|_| RarcCompileError::NextIdOutOfRange(rarc_companion.next_id))?;
        info.sync_ids = rarc_companion.sync_ids;

        let root_node = RarcNode {
            ident: "ROOT".to_string(),
            name: rarc_companion.root_name.clone(),
            // gets filled in later
            num_entries: 0,
            start_index: 0,
        };

        let mut nodes = vec![
            NodeInfo {
                path: path.clone(),
                node: root_node,
                parent_index: u32::MAX,
            },
        ];

        let mut names = vec![".".to_string(), "..".to_string(), rarc_companion.root_name.clone()];

        // Populate nodes and names from folder
        recur_nodes(path, 0, &mut nodes, &mut names)?;

        // Collect up entries and populate some missing node data like num entries
        let data = collect_data(&mut nodes, rarc_companion)?;

        let mut name_table = Vec::with_capacity((nodes.len() + data.entries.len()) * 10);
        write_name_table(&mut name_table, &names)?;

        // nodes
        info.num_nodes = nodes.len() as u32;
        // remove because it's relative to header
        info.node_offset = w.stream_position()? as u32 - header.data_header_offset;
        log::debug!("Writing {} nodes...", info.num_nodes);
        for ninfo in nodes {
            ninfo.node.write(w, &names)?;
        }

        write_align(w, 32, AlignType::Zero)?;

        // entries
        info.num_entries = data.entries.len() as u32;
        // remove because it's relative to header
        info.entry_offset = w.stream_position()? as u32 - header.data_header_offset;
        log::debug!("Writing {} entries...", info.num_entries);

        for entry in data.entries {
            /*if entry.flags.contains(FileFlags::IS_FILE) {
                // Move the offset to where the data is going to be written,
                //
                // todo these never seem to be used so it's assumed this is how it works?
                //  it seems like it could be susceptible to overflow and we do write the
                //  section lens already so maybe it's just calculated
                /*if entry.flags.contains(FileFlags::PRELOAD_ARAM) {
                    entry.offset_or_index += data.mram_data.len() as u32;
                } else if !entry.flags.contains(FileFlags::PRELOAD_MRAM) {
                    entry.offset_or_index += data.mram_data.len() as u32 + data.aram_data.len() as u32;
                }*/
            }*/
            entry.write(w, &names)?;
        }

        write_align(w, 32, AlignType::Zero)?;

        // name table
        let name_table_start = w.stream_position()? as u32;
        // remove because it's relative to header
        info.name_table_offset = w.stream_position()? as u32 - header.data_header_offset;
        wraw!(w, name_table)?;

        write_align(w, 32, AlignType::Zero)?;

        // Why do they do this?
        info.name_table_len = w.stream_position()? as u32 - name_table_start;

        // file data at very end (mram aram dvd)
        // remove because it's relative to header
        header.data_offset = w.stream_position()? as u32 - header.data_header_offset;
        header.mram_data_len = data.mram_data.len() as u32;
        wraw!(w, data.mram_data)?;
        header.aram_data_len = data.aram_data.len() as u32;
        wraw!(w, data.aram_data)?;
        header.total_data_len =
            header.mram_data_len + header.aram_data_len + data.dvd_data.len() as u32;
        wraw!(w, data.dvd_data)?;

        log::debug!("Writing info...");
        info.write(w)?;

        let len = w.stream_position()?;
        header.rarc_len = u32::try_from(len).map_err(|_| RarcCompileError::RarcTooBig)?;
        log::debug!("Writing header...");
        header.write(w)?;

        Ok(())
    }
}

#[derive(Debug, Clone)]
struct RarcHeader {
    pub rarc_len: u32,
    pub data_header_offset: u32,
    pub data_offset: u32,
    /// Size of all blocks combined
    pub total_data_len: u32,
    /// Size of all files with MRAM flag in total
    pub mram_data_len: u32,
    /// Size of all files with ARAM flag in total
    pub aram_data_len: u32,
}

impl RarcHeader {
    pub fn new<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let rarc_len = read_u32(buf)?;
        let data_header_offset = read_u32(buf)?;
        // the offset is relative to the end of the header, so we add the header len
        let data_offset = read_u32(buf)? + data_header_offset;
        let total_data_len = read_u32(buf)?;
        let mram_data_len = read_u32(buf)?;
        let aram_data_len = read_u32(buf)?;
        let _padding = read_u32(buf)?;

        Ok(Self {
            rarc_len,
            data_header_offset,
            data_offset,
            total_data_len,
            mram_data_len,
            aram_data_len,
        })
    }

    /// Creates itself in an empty state for convenience
    pub fn reserve_space<W: Write + Seek>(w: &mut W) -> Result<Self, RarcCompileError> {
        // Reserve 28 bytes for header (32 - magic)
        wraw!(w, [0; 28])?;

        Ok(Self {
            rarc_len: 0,
            // include header already since it's known
            data_header_offset: 0x20,
            data_offset: 0,
            total_data_len: 0,
            mram_data_len: 0,
            aram_data_len: 0,
        })
    }

    /// Fill in the data from the fields into the reserved header space
    pub fn write<W: Write + Seek>(&self, w: &mut W) -> Result<(), RarcCompileError> {
        wat!(w, self.rarc_len, 0x04);
        wat!(w, self.data_header_offset, 0x08);
        wat!(w, self.data_offset, 0x0C);
        wat!(w, self.total_data_len, 0x10);
        wat!(w, self.mram_data_len, 0x14);
        wat!(w, self.aram_data_len, 0x18);
        // padding here stays 0 so do not write it

        Ok(())
    }
}

/// Header about the data within the RARC, always directly below the RARC header
#[derive(Debug, Clone)]
pub struct RarcInfo {
    /// Total number of nodes (dirs)
    pub num_nodes: u32,
    /// This field is relative to the start of the RARC itself,
    /// but when written it will be relative to the start of this header
    ///
    /// Always 0x40 basically since the layout is kinda static
    pub node_offset: u32,
    /// Total number of file entries
    pub num_entries: u32,
    /// This field is relative to the start of the RARC itself,
    /// but when written it will be relative to the start of this header
    pub entry_offset: u32,
    pub name_table_len: u32,
    /// This field is relative to the start of the RARC itself,
    /// but when written it will be relative to the start of this header
    pub name_table_offset: u32,
    /// If a new file is added to this ARC, this is the ID that it should be given.
    /// Then this value should be incremented by 1.
    pub next_id: u16,
    /// Seems to be a boolean that, when true, indicates all the files will have file IDs
    /// equal to their index in the list of all the files in the ARC. When false, the file IDs will
    /// be out of sync with their indexes. By default, this is 0x01 in all sunshine szs files
    pub sync_ids: bool,
}

impl RarcInfo {
    pub fn new<R: Read + Seek>(buf: &mut R, data_header_offset: u32) -> Result<Self, ReadError> {
        let num_nodes = read_u32(buf)?;
        // the offset is relative to the end of the header, so we add the header len
        let node_offset = read_u32(buf)? + data_header_offset;
        let num_entries = read_u32(buf)?;
        // the offset is relative to the end of the header, so we add the header len
        let entry_offset = read_u32(buf)? + data_header_offset;
        let name_table_len = read_u32(buf)?;
        // the offset is relative to the end of the header, so we add the header len
        let name_table_offset = read_u32(buf)? + data_header_offset;
        let next_id = read_u16(buf)?;
        let sync_ids = read_u8(buf)? != 0;
        // Might be padding? Might be more flags (seems unused, always 0)
        let _unk = read_u8(buf)?;
        // always 0
        let _pad0 = read_u32(buf)?;

        Ok(Self {
            num_nodes,
            node_offset,
            num_entries,
            entry_offset,
            name_table_len,
            name_table_offset,
            next_id,
            sync_ids,
        })
    }

    /// Creates itself in an empty state for convenience
    pub fn reserve_space<W: Write + Seek>(w: &mut W) -> Result<Self, RarcCompileError> {
        wraw!(w, [0; 32])?;

        Ok(Self {
            num_nodes: 0,
            node_offset: 0x40,
            num_entries: 0,
            entry_offset: 0,
            name_table_len: 0,
            name_table_offset: 0,
            next_id: 0,
            sync_ids: false,
        })
    }

    pub fn write<W: Write + Seek>(
        &self,
        w: &mut W,
    ) -> Result<(), RarcCompileError> {
        const BASE: u64 = 0x20;
        wat!(w, self.num_nodes, BASE);
        wat!(w, self.node_offset, BASE + 0x04);
        wat!(w, self.num_entries, BASE + 0x08);
        wat!(w, self.entry_offset, BASE + 0x0C);
        wat!(w, self.name_table_len, BASE + 0x10);
        wat!(w, self.name_table_offset, BASE + 0x14);
        wat!(w, self.next_id, BASE + 0x18);
        wat!(w, if self.sync_ids { 1u8 } else { 0 }, BASE + 0x1A);
        // padding here stays 0
        // padding here stays 0

        Ok(())
    }
}

/// A directory
#[derive(Debug, Clone)]
struct RarcNode {
    /// If it is the base node, it is ROOT. For all others, it is the first 4 letters
    /// of the `name` in all UPPERCASE, or padded with spaces to reach 4 characters.
    /// Spaces do not need to be added here, they are handled.
    pub ident: String,
    /// Name of the folder
    pub name: String,
    /// num entries for this specific directory
    pub num_entries: u16,
    /// The index of the first file entry that belongs to this directory
    pub start_index: u32,
}

impl RarcNode {
    pub fn new<R: Read + Seek + BufRead>(
        buf: &mut R,
        index: u32,
        info: &RarcInfo,
    ) -> Result<Self, ReadError> {
        buf.seek(SeekFrom::Start(
            info.node_offset as u64 + (index * RARC_NODE_SIZE) as u64,
        ))?;

        let mut ident_buf: [u8; 4] = [0, 0, 0, 0];
        buf.read_exact(&mut ident_buf)?;
        let ident = String::from_utf8(ident_buf.to_vec())?;
        // The value is relative to the start of the name table, so we add
        // the offset to make it relative to 0
        let name_offset = read_u32(buf)? + info.name_table_offset;
        let _hash = read_u16(buf)?;
        let num_entries = read_u16(buf)?;
        let start_index = read_u32(buf)?;

        buf.seek(SeekFrom::Start(name_offset as u64))?;

        let mut buf_name = Vec::with_capacity(8);
        buf.read_until(0, &mut buf_name)?;
        // remove trailing 0
        buf_name.pop();
        let name = String::from_utf8(buf_name)?;

        Ok(Self {
            ident,
            name,
            num_entries,
            start_index,
        })
    }

    pub fn write<W: Write + Seek>(
        &self,
        w: &mut W,
        names: &[String],
    ) -> Result<(), RarcCompileError> {
        let mut ident_buf = vec![0; 4];
        let mut c = Cursor::new(&mut ident_buf);
        let ident_len = self.ident.len();
        let bytes = if ident_len > 4 {
            self.ident[..4.min(ident_len)].as_bytes()
        } else {
            self.ident.as_bytes()
        };
        if bytes.len() < 4 {
            let mut bytes = bytes.to_vec();
            // Empty slots need to be filled with spaces (0x20)
            bytes.resize(4, 0x20);

            c.write_all(&bytes)?;
        } else {
            c.write_all(bytes)?;
        }

        wraw!(w, ident_buf)?;

        let name_offset = get_name_offset(names, &self.name) as u32;

        w!(w, name_offset)?;
        w!(w, hash_name(&self.name))?;
        w!(w, self.num_entries)?;
        w!(w, self.start_index)?;

        // this is size 16 so maybe it's padded to 32? todo

        Ok(())
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RarcFlags {
    pub compressed: bool,
    pub load_type: String,
}

bitflags! {
    #[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
    pub struct FileFlags: u16 {
        const IS_FILE = 0x01;
        const IS_DIR = 0x02;
        /// This entry's data is compressed, either with Yaz0 or Yay0.
        /// todo: implement this inside rarc
        const IS_COMPRESSED = 0x04;
        /// Indicates that this file gets Pre-loaded into Main RAM
        const PRELOAD_MRAM = 0x10;
        /// Indicates that this file gets Pre-loaded into Auxiliary RAM
        const PRELOAD_ARAM = 0x20;
        /// Indicates that this file does not get pre-loaded, but rather read from the DVD
        const DVD_LOAD = 0x40;
        /// IS_COMPRESSED must be set for this to do anything
        const IS_YAZ0 = 0x80;
    }
}

impl FileFlags {
    pub fn to_rarc_flags(&self) -> RarcFlags {
        let is_compressed = self.contains(FileFlags::IS_COMPRESSED);
        let load_type = if self.contains(FileFlags::PRELOAD_MRAM) {
            PRELOAD_TO_MRAM
        } else if self.contains(FileFlags::PRELOAD_ARAM) {
            PRELOAD_TO_ARAM
        } else if self.contains(FileFlags::DVD_LOAD) {
            LOAD_FROM_DVD
        } else {
            LOAD_NONE
        };

        RarcFlags {
            compressed: is_compressed,
            load_type: load_type.to_string(),
        }
    }

    pub fn insert_rarc_flags(&mut self, rarc_flags: &RarcFlags) {
        let mut new = Self::empty();

        if rarc_flags.compressed {
            new.set(FileFlags::IS_COMPRESSED, true);
            // Always YAZ0 for compressed in sunshine
            new.set(FileFlags::IS_YAZ0, true);
        }

        let load_flag = match rarc_flags.load_type.as_str() {
            PRELOAD_TO_MRAM => Some(FileFlags::PRELOAD_MRAM),
            PRELOAD_TO_ARAM => Some(FileFlags::PRELOAD_ARAM),
            LOAD_FROM_DVD => Some(FileFlags::DVD_LOAD),
            LOAD_NONE | _ => None,
        };

        if let Some(load_flag) = load_flag {
            new.set(load_flag, true);
        }

        self.insert(new);
    }
}

#[derive(Debug, Clone)]
struct RarcFileEntry {
    /// Unique ID for the file
    pub id: u16,
    /// Is little endian
    pub flags: FileFlags,
    pub name: String,
    /// Offset to the data if it's a file, index of the representative node if it's a folder
    pub offset_or_index: u32,
    /// For files: size of the file, for subdirectories: always 0x10 (size of the node entry?)
    pub len: u32,
    /// Always 0 in SMS. Probably padding
    pub unk: u32,
}

impl RarcFileEntry {
    pub fn new<R: Read + Seek + BufRead>(
        buf: &mut R,
        index: u32,
        info: &RarcInfo,
    ) -> Result<Self, ReadError> {
        buf.seek(SeekFrom::Start(
            info.entry_offset as u64 + (index * RARC_FILE_ENTRY_SIZE) as u64,
        ))?;

        let id = read_u16(buf)?;
        let _hash = read_u16(buf)?;
        let flags = FileFlags::from_bits_retain(read_u16_le(buf)?);
        let name_offset = read_u16(buf)? as u32 + info.name_table_offset;
        let offset_or_index = read_u32(buf)?;
        let len = read_u32(buf)?;
        let unk = read_u32(buf)?;

        buf.seek(SeekFrom::Start(name_offset as u64))?;

        let mut buf_name = Vec::with_capacity(8);
        buf.read_until(0, &mut buf_name)?;
        // remove trailing 0
        buf_name.pop();

        let name = String::from_utf8(buf_name)?;

        Ok(Self {
            id,
            flags,
            name,
            offset_or_index,
            len,
            unk,
        })
    }

    pub fn write<W: Write + Seek>(
        &self,
        w: &mut W,
        names: &[String],
    ) -> Result<(), RarcCompileError> {
        w!(w, self.id)?;
        w!(w, hash_name(&self.name))?;
        wraw!(w, self.flags.bits().to_le_bytes())?;

        let name_offset = get_name_offset(names, &self.name) as u16;

        w!(w, name_offset)?;
        w!(w, self.offset_or_index)?;
        w!(w, self.len)?;
        w!(w, self.unk)?;

        Ok(())
    }

    pub fn is_folder(&self) -> bool {
        self.flags.contains(FileFlags::IS_DIR)
    }

    pub fn to_gcfile<R: Read + Seek + BufRead>(
        &self,
        path: PathBuf,
        buf: &mut R,
        header: &RarcHeader,
    ) -> Result<GcFile, ReadError> {
        let data = if self.is_folder() {
            Vec::new()
        } else {
            buf.seek(SeekFrom::Start(
                (self.offset_or_index + header.data_offset) as u64,
            ))?;

            let mut v = vec![0; self.len as usize];
            buf.read_exact(&mut v)?;

            v
        };
        let persistent_data = RarcEntryCompanion {
            id: self.id as u64,
            rarc_flags: self.flags.to_rarc_flags(),
        };
        GcFile::new(path, data, Some(persistent_data))
    }
}

fn read_node(
    buf: &mut Reader,
    node: RarcNode,
    header: &RarcHeader,
    info: &RarcInfo,
    cur_path: PathBuf,
) -> Result<Vec<GcFile>, ReadError> {
    let mut out = Vec::with_capacity(node.num_entries as usize);

    for i in node.start_index..node.start_index + node.num_entries as u32 {
        let entry = RarcFileEntry::new(buf, i, info)?;
        if entry.name == "." || entry.name == ".." {
            // Bad for recursion and file system
            continue;
        }

        let path = cur_path.join(entry.name.clone());
        out.push(entry.to_gcfile(path.clone(), buf, header)?);

        if entry.is_folder() {
            let node = RarcNode::new(buf, entry.offset_or_index, info)?;
            let files = read_node(buf, node, header, info, path)?;
            out.extend(files.into_iter());
        }
    }

    Ok(out)
}

pub struct CompiledRarcData {
    entries: Vec<RarcFileEntry>,
    mram_data: Vec<u8>,
    aram_data: Vec<u8>,
    dvd_data: Vec<u8>,
}

fn get_name_offset(names: &[String], name: &String) -> usize {
    let mut offset = 0;

    for other in names {
        if other == name {
            break;
        } else {
            // add 1 for null term
            offset += other.len() + 1;
        }
    }

    offset
}

fn write_name_table<W: Write>(w: &mut W, names: &[String]) -> Result<(), RarcCompileError> {
    for name in names {
        let mut name_bytes = name.as_bytes().to_vec();
        name_bytes.push(0);

        wraw!(w, name_bytes)?;
    }

    Ok(())
}

/// Returns the total number of valid entries in the dir
fn collect_data(
    nodes: &mut [NodeInfo],
    companion: RarcCompanion,
) -> Result<CompiledRarcData, RarcCompileError> {
    let mut data_out = CompiledRarcData {
        entries: vec![],
        mram_data: vec![],
        aram_data: vec![],
        dvd_data: vec![],
    };

    let node_lookup = nodes.iter().enumerate().map(|(ind, node)| {
        (node.path.clone(), ind)
    }).collect::<HashMap<_, _>>();

    for (index, info) in nodes.iter_mut().enumerate() {
        // Entries need to be sorted by files first A-Z then folders A-Z (then . and ..)
        // then once all of those entries are entered, recur down each folder one after another.
        // This is repeated within each folder
        let mut entries = read_dir(&info.path)?.filter_map(|e| e.ok()).collect::<Vec<_>>();

        #[cfg(debug_assertions)]
        if entries.len() == 0 {
            // Should already be handled, but catches a bug
            unreachable!();
        }

        entries.sort_unstable_by(sort_entries);

        let mut num_entries = 0;
        // todo error?
        info.node.start_index = data_out.entries.len() as u32;

        for entry in entries {
            let path = entry.path();

            // Don't process meta file
            if path.file_name().is_none()
                || path
                .file_name()
                .is_some_and(|n| n.to_string_lossy().ends_with(COMPANION_EXT))
            {
                continue;
            }

            match path.components().next_back() {
                Some(Component::CurDir | Component::ParentDir) => {
                    // skip . and .. as we handle them manually
                    continue;
                }
                _ => {}
            };

            let os_name = path.file_name().unwrap();
            let name = os_name.to_string_lossy().to_string();

            num_entries += 1;

            let entry = if path.is_dir() {
                let flags = FileFlags::IS_DIR;

                // Create an entry for the parent dir to reference
                RarcFileEntry {
                    id: u16::MAX,
                    flags,
                    name,
                    offset_or_index: node_lookup.get(&path).cloned().unwrap_or(0) as u32,
                    len: 0x10,
                    unk: 0,
                }
            } else {
                let Some(companion) = get_rarc_entry(&path)? else {
                    return Err(RarcCompileError::EntryWithNoMeta(path));
                };

                let mut flags = FileFlags::IS_FILE;
                flags.insert_rarc_flags(&companion.rarc_flags);
                let data = if flags.contains(FileFlags::IS_COMPRESSED) {
                    let data = read(&path)?;
                    let mut out = Vec::with_capacity(data.len());

                    // todo decompress in the rarc reader
                    // Compressing is slow so info for visibility
                    log::info!("Compressing {:?}...", path);
                    Yaz0Writer::new(&mut out)
                        .compress_and_write(&data, CompressionLevel::Lookahead { quality: 10 })?;
                    log::info!("Done compressing");

                    out
                } else {
                    read(&path)?
                };

                let len = data.len() as u32;

                // The rest of the offets need to be added later, when all of the data is collected
                let offset = if flags.contains(FileFlags::PRELOAD_MRAM) {
                    let offset = data_out.mram_data.len();

                    data_out.mram_data.extend(data);

                    // extend the data to pad all files to 32 byte addresses
                    let mut cursor = Cursor::new(&mut data_out.mram_data);
                    cursor.seek(SeekFrom::End(0))?;
                    write_align(&mut cursor, 32, AlignType::Zero)?;

                    offset as u32
                } else if flags.contains(FileFlags::PRELOAD_ARAM) {
                    let offset = data_out.aram_data.len();

                    data_out.aram_data.extend(data);

                    // extend the data to pad all files to 32 byte addresses
                    let mut cursor = Cursor::new(&mut data_out.aram_data);
                    cursor.seek(SeekFrom::End(0))?;
                    write_align(&mut cursor, 32, AlignType::Zero)?;

                    offset as u32
                } else {
                    // This also includes if no flag is set
                    flags.set(FileFlags::DVD_LOAD, true);
                    let offset = data_out.dvd_data.len();

                    data_out.dvd_data.extend(data);

                    // extend the data to pad all files to 32 byte addresses
                    let mut cursor = Cursor::new(&mut data_out.dvd_data);
                    cursor.seek(SeekFrom::End(0))?;
                    write_align(&mut cursor, 32, AlignType::Zero)?;

                    offset as u32
                };

                RarcFileEntry {
                    id: u16::try_from(companion.id)
                        .map_err(|_| RarcCompileError::EntryIdOutOfRange(companion.id))?,
                    flags,
                    name,
                    offset_or_index: offset,
                    len,
                    unk: 0,
                }
            };

            #[cfg(debug_assertions)]
            if companion.sync_ids && entry.id != u16::MAX && data_out.entries.len() as u16 != entry.id {
                // Ideally this should never happen, but it will catch a bug
                log::debug!("Entries: {:?}", data_out.entries);
                return Err(RarcCompileError::NotSynced(
                    data_out.entries.len(),
                    entry.id,
                ));
            }

            data_out.entries.push(entry);
        }

        // simulate . and ..
        let cur_dir = RarcFileEntry {
            id: u16::MAX,
            flags: FileFlags::IS_DIR,
            name: ".".to_string(),
            offset_or_index: index as u32,
            len: 0x10,
            unk: 0,
        };

        let prev_dir = RarcFileEntry {
            id: u16::MAX,
            flags: FileFlags::IS_DIR,
            name: "..".to_string(),
            offset_or_index: info.parent_index,
            len: 0x10,
            unk: 0,
        };

        data_out.entries.push(cur_dir);
        data_out.entries.push(prev_dir);
        num_entries += 2;

        info.node.num_entries = num_entries;
    }

    Ok(data_out)
}

struct NodeInfo {
    path: PathBuf,
    node: RarcNode,
    parent_index: u32,
}

fn recur_nodes(
    dir: PathBuf,
    parent_index: u32,
    nodes_out: &mut Vec<NodeInfo>,
    names_out: &mut Vec<String>,
) -> Result<(), RarcCompileError> {
    // Entries need to be sorted by files first A-Z then folders A-Z (then . and ..)
    // then once all of those entries are entered, recur down each folder one after another.
    // This is repeated within each folder
    let mut entries = read_dir(dir)?.filter_map(|e| e.ok()).collect::<Vec<_>>();

    entries.sort_unstable_by(sort_entries);

    for entry in entries {
        let path = entry.path();

        match path.components().next_back() {
            Some(Component::CurDir | Component::ParentDir) => {
                // skip . and .. as we handle them manually
                continue;
            }
            _ => {}
        };

        if path.file_name().is_none() {
            continue;
        }

        let os_name = path.file_name().unwrap();
        let name = os_name.to_string_lossy().to_string();

        // Don't add companions to filenames
        if name.ends_with(COMPANION_EXT) {
            continue;
        }

        if !names_out.contains(&name) {
            names_out.push(name.clone());
        }

        // Don't process files, only dirs
        if path.is_file()
        {
            continue;
        }

        match read_dir(&path) {
            Ok(i) => {
                if i.filter_map(|e| e.ok()).filter(|e| {
                    let path = e.path();
                    match path.components().next_back() {
                        Some(Component::CurDir | Component::ParentDir) => {
                            // skip . and ..
                            false
                        }
                        _ => true,
                    }
                }).count() == 0 {
                    log::debug!("Skipping empty dir {:?}", path);
                    continue;
                }
            }
            Err(e) => {
                // todo technically this should be caught way earlier in the pipeline
                log::warn!("Skipping directory {:?} which produced error: {:?}", path, e);
                continue;
            }
        }

        // make rarc node for folder
        // root is assumed to have already been made
        let ident = name[..4.min(name.len())].to_uppercase();

        let node = RarcNode {
            ident,
            name: name.clone(),
            // we fill this in later
            num_entries: 0,
            // we fill this in later
            start_index: 0,
        };
        let our_index = nodes_out.len();

        nodes_out.push(NodeInfo {
            path: path.clone(),
            node,
            parent_index,
        });

        recur_nodes(path, our_index as u32, nodes_out, names_out)?;
    }

    Ok(())
}

fn get_rarc_entry(parent_file: &PathBuf) -> Result<Option<RarcEntryCompanion>, CompanionError> {
    let ext = parent_file.extension().map(|v| v.to_ascii_lowercase());
    match ext.unwrap_or_else(|| OsString::new()).to_str().unwrap() {
        "szs" => Ok(RarcCompanion::read(parent_file)?.rarc_data),
        "bmd" => {
            // try generic first as it's expected
            match GenericCompanion::read(parent_file) {
                Ok(got) => Ok(got.rarc_data),
                Err(e) => {
                    log::warn!(
                        "Failed reading GenericCompanion, retrying with Bmd: {:?}",
                        e
                    );
                    Ok(BmdCompanion::read(parent_file)?.rarc_data)
                }
            }
        }
        "bti" => {
            // try generic first as it's expected
            match GenericCompanion::read(parent_file) {
                Ok(got) => Ok(got.rarc_data),
                Err(e) => {
                    log::warn!(
                        "Failed reading GenericCompanion, retrying with Bti: {:?}",
                        e
                    );
                    Ok(BtiCompanion::read(parent_file)?.rarc_data)
                }
            }
        }
        _ => Ok(GenericCompanion::read(parent_file)?.rarc_data),
    }
}

fn sort_entries(a: &DirEntry, b: &DirEntry) -> Ordering {
    let ap = a.path();
    let bp = b.path();
    sort_paths(&ap, &bp)
}

fn sort_paths(ap: &PathBuf, bp: &PathBuf) -> Ordering {
    let ad = ap.is_dir();
    let bd = bp.is_dir();
    if ad && bd {
        // sort A-Z
        if let (Some(an), Some(bn)) = (ap.file_name(), bp.file_name()) {
            // todo this is an assumption that it is uppercased, which may not be true
            an.to_ascii_uppercase().cmp(&bn.to_ascii_uppercase())
        } else {
            ap.cmp(bp)
        }
    } else if ad {
        // bp is lesser because file
        Ordering::Greater
    } else if bd {
        // ap is lesser because file
        Ordering::Less
    } else {
        // both files, sort A-Z
        if let (Some(an), Some(bn)) = (ap.file_name(), bp.file_name()) {
            // todo this is an assumption that it is uppercased, which may not be true
            an.to_ascii_uppercase().cmp(&bn.to_ascii_uppercase())
        } else {
            ap.cmp(bp)
        }
    }
}

#[cfg(test)]
mod test {
    use std::env::current_dir;
    use std::fs::{create_dir, create_dir_all, remove_dir_all, write};
    use std::io::Cursor;
    use std::io::Write;
    use std::time::{SystemTime, UNIX_EPOCH};

    use bitflags::Flags;

    use crate::bytes::read_u16_le;
    use crate::gc::rarc::{sort_paths, FileFlags};
    use crate::wraw;

    #[test]
    fn flag_in_out() {
        let mut w = Vec::with_capacity(2);
        let flags: FileFlags = FileFlags::IS_FILE | FileFlags::PRELOAD_ARAM;

        wraw!(w, flags.bits().to_le_bytes()).unwrap();

        let mut r = Cursor::new(w);
        let read_flags = FileFlags::from_bits_retain(read_u16_le(&mut r).unwrap());

        assert_eq!(flags, read_flags);
    }

    #[test]
    fn path_order() {
        let ms = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_millis();
        let mut cur = current_dir().unwrap();
        cur.push("tmp");
        cur.push(format!("test_{}", ms));

        // test dir init
        create_dir_all(&cur).unwrap();

        // the test entries
        let d1 = cur.join("abc");
        let d2 = cur.join("def");
        let d3 = cur.join("ghi");
        create_dir(&d1).unwrap();
        create_dir(&d2).unwrap();
        create_dir(&d3).unwrap();
        let f1 = cur.join("ack.mt");
        let f2 = cur.join("foo.mt");
        let f3 = cur.join("zip.mt");
        write(&f1, "").unwrap();
        write(&f2, "").unwrap();
        write(&f3, "").unwrap();

        let expected = vec![
            f1.clone(),
            f2.clone(),
            f3.clone(),
            d1.clone(),
            d2.clone(),
            d3.clone(),
        ];

        let mut paths1 = vec![
            d2.clone(),
            d3.clone(),
            f1.clone(),
            f3.clone(),
            f2.clone(),
            d1.clone(),
        ];

        paths1.sort_unstable_by(sort_paths);

        if paths1 != expected {
            remove_dir_all(cur).unwrap();
            assert_eq!(paths1, expected);
            panic!();
        }

        let mut paths2 = vec![f1, d3, d2, f3, d1, f2];

        paths2.sort_unstable_by(sort_paths);

        if paths2 != expected {
            remove_dir_all(cur).unwrap();
            assert_eq!(paths2, expected);
            panic!();
        }

        // length test
        let f4 = cur.join("sand.mt");
        let f5 = cur.join("sandsmo.mt");
        let f6 = cur.join("sandsmo_sl.mt");
        let f7 = cur.join("sand_sl.mt");
        write(&f4, "").unwrap();
        write(&f5, "").unwrap();
        write(&f6, "").unwrap();
        write(&f7, "").unwrap();

        let expected = vec![f4.clone(), f5.clone(), f6.clone(), f7.clone()];

        let mut paths3 = vec![f6.clone(), f5.clone(), f7.clone(), f4.clone()];

        paths3.sort_unstable_by(sort_paths);

        if paths3 != expected {
            remove_dir_all(cur).unwrap();
            assert_eq!(paths3, expected);
            panic!();
        }

        remove_dir_all(cur).unwrap();
    }
}
