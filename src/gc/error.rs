use std::{
    path::{
        StripPrefixError,
        PathBuf
    },
    io,
    string::FromUtf8Error
};

use crate::{
    gc::{
        bmd::error::BmdWriteError,
        rarc::error::RarcCompileError,
        bti::error::BtiWriteError,
        types::mesh::GxAttribute
    },
    companion::error::CompanionError,
};
use displaydoc::Display;
use thiserror::Error;

// todo split into error for each type
#[derive(Display, Error, Debug)]
pub enum ReadError {
    /// Failed to read file: {0}
    Io(#[from] io::Error),
    /// ISO is not a GameCube ISO, got check data of: {0}
    NotGc(String),
    /// ISO contains unsupported FST number: {0},{1}
    MultiFst(u32, u8),
    /// Failed to convert to UTF8: {0}
    Utf8Conversion(#[from] FromUtf8Error),
    /// Read check failed: {0}
    ReadCheck(&'static str),
    /// File not found: {0}
    NotFound(PathBuf),
    /// File is not a RARC file
    NotRarc,
    /// Failed to find Rarc ROOT
    NoRoot,
    /// File is not a BMD file
    NotBmd,
    /// Invalid node type: {0}
    InvalidNodeKind(u16),
    /// Node ind duplicate
    NodeIndDup,
    /// Failed to decompress Yaz0: {0}
    Yaz0(#[from] yaz0::Error),
    /// Invalid GxAttribute: {0}
    InvalidGxAttribute(u8),
    /// Invalid GxAttributeKind: {0}
    InvalidGxAttributeKind(u8),
    /// No valid GxComponentType for pair: {0:?}
    NoKnownGxComponentType((GxAttribute, u32)),
    /// No valid GxDataType for pair: {0:?}
    NoKnownGxDataType((GxAttribute, u32)),
    /// No valid data offset index for {0:?}
    NoKnownDataOffsetIndex(GxAttribute),
    /// GxAttribute not implemented: {0:?}
    GxAttributeTypeNotImplemented(GxAttribute),
    /// Invalid WeightKind: {0}
    InvalidWeightKind(u8),
    /// Invalid MatrixKind: {0}
    InvalidMatrixKind(u16),
    /// Invalid Shape Mode: {0}
    InvalidShapeMode(u8),
    /// Unknown Mesh Display List command: {0}
    UnknownMDLCommand(u8),
    /// No valid PrimitiveKind: {0}
    NoKnownPrimitiveKind(u8),
    /// No valid {0} from {1}
    NoKnown(&'static str, String),
    /// PostTexGen has padding in middle @ {0}
    WeirdPostTexGen(u32),
    /// Vec get ind failed for {0}[{1}]
    VecGet(&'static str, usize),
}

#[derive(Display, Error, Debug)]
pub enum WriteError {
    /// Failed to write: {0}
    Io(#[from] io::Error),
    /// Path does not have parent: {0}
    NoParentPath(PathBuf),
    /// Failed to write BTI file: {0}
    BtiWrite(#[from] BtiWriteError),
    /// Failed to write BMD file: {0}
    BmdWrite(#[from] BmdWriteError),
    /// Failed to write generic companion file: {0}
    CompanionError(#[from] CompanionError),
}

#[derive(Display, Error, Debug)]
pub enum IsoWriteError {
    /// Failed to write: {0}
    Io(#[from] io::Error),
    /// RARC compile error: {0}
    Rarc(#[from] RarcCompileError),
    /// YAZ0 error: {0}
    Yaz0(#[from] yaz0::Error),
    /// TOC Write error: {0}
    TocWrite(#[from] TocWriteError),
    /// Failed to strip prefix: {0}
    StripPrefix(#[from] StripPrefixError),
    /**
    Exceeded 4,294,967,295 bytes of file data.
    Either files are too long or there are too many files!
     */
    ExcessiveFiles,
}

#[derive(Display, Error, Debug)]
pub enum TocWriteError {
    /// Failed to write: {0}
    Io(#[from] io::Error),
    /**
    Exceeded 16,777,215 bytes of name data.
    Either file names are too long or there are too many files!
     */
    ExcessiveNames,
}
