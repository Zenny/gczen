use std::io::{Read, Seek, SeekFrom};

use crate::{
    bytes::{read_u16, read_u32, read_u8},
    gc::{
        bti::convert::{
            get_interpolated_cmpr_colors, get_interpolated_cmpr_colors_alpha, ColorData,
        },
        error::ReadError,
        types::bti::JUTTransparency,
    },
    util::{REL_MAP_2, REL_MAP_4},
};

pub fn decode_i4<R: Read>(buf: &mut R, out: &mut Vec<ColorData>) -> Result<(), ReadError> {
    let b = read_u8(buf)?;
    for n in 0..2 {
        let i4 = (b >> ((1 - n) * 4)) & 0xF;
        out.push(ColorData::from_i4(i4));
    }

    Ok(())
}

pub fn decode_i8<R: Read>(buf: &mut R, out: &mut Vec<ColorData>) -> Result<(), ReadError> {
    let val = read_u8(buf)?;
    out.push(ColorData::from_i8(val));

    Ok(())
}

pub fn decode_ia4<R: Read>(buf: &mut R, out: &mut Vec<ColorData>) -> Result<(), ReadError> {
    let val = read_u8(buf)?;
    out.push(ColorData::from_ia4(val));

    Ok(())
}

pub fn decode_ia8<R: Read>(buf: &mut R, out: &mut Vec<ColorData>) -> Result<(), ReadError> {
    let val = read_u16(buf)?;
    out.push(ColorData::from_ia8(val));

    Ok(())
}

pub fn decode_rgb565<R: Read>(buf: &mut R, out: &mut Vec<ColorData>) -> Result<(), ReadError> {
    let val = read_u16(buf)?;
    out.push(ColorData::from_rgb565(val));

    Ok(())
}

pub fn decode_rgb5a3<R: Read>(buf: &mut R, out: &mut Vec<ColorData>) -> Result<(), ReadError> {
    let val = read_u16(buf)?;
    out.push(ColorData::from_rgb5a3(val));
    Ok(())
}

pub fn decode_rgba8<R: Read + Seek>(buf: &mut R, out: &mut Vec<ColorData>) -> Result<(), ReadError> {
    let offset = buf.stream_position()?;
    for i in 0..16 {
        let pos = i as u64 * 2;
        buf.seek(SeekFrom::Start(offset + pos))?;
        let a = read_u8(buf)?;
        let r = read_u8(buf)?;
        buf.seek(SeekFrom::Current(30))?;
        let g = read_u8(buf)?;
        let b = read_u8(buf)?;
        out.push(ColorData::Rgba([r, g, b, a]));
    }

    Ok(())
}

pub fn decode_c8<R: Read>(
    buf: &mut R,
    out: &mut Vec<ColorData>,
    colors: &Vec<ColorData>,
) -> Result<(), ReadError> {
    let ind = read_u8(buf)? as usize;
    if ind >= colors.len() {
        // filler color
        out.push(colors[0].clone());
    } else {
        out.push(colors[ind].clone());
    }

    Ok(())
}

pub fn decode_cmpr<R: Read>(
    buf: &mut R,
    out: &mut Vec<ColorData>,
    alpha: JUTTransparency,
) -> Result<(), ReadError> {
    let mut block = vec![ColorData::Gray(0); 64];

    for (subblock_x, subblock_y) in REL_MAP_2.iter() {
        // rgb565
        let (c0, c1) = (read_u16(buf)?, read_u16(buf)?);

        // todo alpha handling probably needs to support clip and stuff?
        let interp = if alpha != JUTTransparency::Opaque {
            get_interpolated_cmpr_colors_alpha(c0, c1)
        } else {
            get_interpolated_cmpr_colors(c0, c1)
        };
        let color_inds = read_u32(buf)?;

        for (j, (rel_x, rel_y)) in REL_MAP_4.iter().enumerate() {
            let shift = (15 - j) * 2;
            let ind = ((color_inds >> shift) & 0x3) as usize;
            let ind_in_block = subblock_x + (subblock_y * 8) + (rel_y * 8) + rel_x;
            block[ind_in_block] = interp[ind];
        }
    }

    out.extend(block.into_iter());

    Ok(())
}
