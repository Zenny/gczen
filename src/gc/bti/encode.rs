// todo: possibly clean up the out[offset] with an actual writing system?

use image::{DynamicImage, GenericImageView};

use crate::{
    gc::{
        bti::convert::{
            get_best_cmpr_key_colors, get_interpolated_cmpr_colors,
            get_interpolated_cmpr_colors_alpha, get_nearest_color_index_fast, ColorData,
        },
        types::bti::{JUTTransparency, TextureFormat},
    },
    util::{REL_MAP_2, REL_MAP_4},
};

pub fn encode_i4(
    img: &DynamicImage,
    width: u32,
    height: u32,
    mut offset: usize,
    block_x: u32,
    block_y: u32,
    out: &mut Vec<u8>,
) {
    for y in block_y..block_y + TextureFormat::I4.block_height() {
        for x in (block_x..block_x + TextureFormat::I4.block_width()).step_by(2) {
            let color1 = if x < width && y < height {
                unsafe { ColorData::Rgba(img.unsafe_get_pixel(x, y).0).to_i4() }
            } else {
                0x0F
            };

            let x2 = x + 1;

            let color2 = if x2 < width && y < height {
                unsafe { ColorData::Rgba(img.unsafe_get_pixel(x2, y).0).to_i4() }
            } else {
                0x0F
            };

            let byte = (color1 << 4) | color2;
            out[offset] = byte;
            offset += 1;
        }
    }
}

pub fn encode_i8(
    img: &DynamicImage,
    width: u32,
    height: u32,
    mut offset: usize,
    block_x: u32,
    block_y: u32,
    out: &mut Vec<u8>,
) {
    for y in block_y..block_y + TextureFormat::I8.block_height() {
        for x in block_x..block_x + TextureFormat::I8.block_width() {
            let color = if x < width && y < height {
                unsafe { ColorData::Rgba(img.unsafe_get_pixel(x, y).0).to_i8() }
            } else {
                0xFF
            };

            out[offset] = color;
            offset += 1;
        }
    }
}

pub fn encode_ia4(
    img: &DynamicImage,
    width: u32,
    height: u32,
    mut offset: usize,
    block_x: u32,
    block_y: u32,
    out: &mut Vec<u8>,
) {
    for y in block_y..block_y + TextureFormat::IA4.block_height() {
        for x in block_x..block_x + TextureFormat::IA4.block_width() {
            let color = if x < width && y < height {
                unsafe { ColorData::Rgba(img.unsafe_get_pixel(x, y).0).to_ia4() }
            } else {
                0xFF
            };

            out[offset] = color;
            offset += 1;
        }
    }
}

pub fn encode_ia8(
    img: &DynamicImage,
    width: u32,
    height: u32,
    mut offset: usize,
    block_x: u32,
    block_y: u32,
    out: &mut Vec<u8>,
) {
    for y in block_y..block_y + TextureFormat::IA8.block_height() {
        for x in block_x..block_x + TextureFormat::IA8.block_width() {
            let [a, i] = if x < width && y < height {
                unsafe { ColorData::Rgba(img.unsafe_get_pixel(x, y).0).to_ia8() }
            } else {
                [0xFF, 0xFF]
            };

            out[offset] = a;
            offset += 1;
            out[offset] = i;
            offset += 1;
        }
    }
}

pub fn encode_rgb565(
    img: &DynamicImage,
    width: u32,
    height: u32,
    mut offset: usize,
    block_x: u32,
    block_y: u32,
    out: &mut Vec<u8>,
) {
    for y in block_y..block_y + TextureFormat::RGB565.block_height() {
        for x in block_x..block_x + TextureFormat::RGB565.block_width() {
            let [a, b] = if x < width && y < height {
                unsafe {
                    ColorData::Rgba(img.unsafe_get_pixel(x, y).0)
                        .to_rgb565()
                        .to_be_bytes()
                }
            } else {
                [0xFF, 0xFF]
            };

            out[offset] = a;
            offset += 1;
            out[offset] = b;
            offset += 1;
        }
    }
}

pub fn encode_rgb5a3(
    img: &DynamicImage,
    alpha: JUTTransparency,
    width: u32,
    height: u32,
    mut offset: usize,
    block_x: u32,
    block_y: u32,
    out: &mut Vec<u8>,
) {
    for y in block_y..block_y + TextureFormat::RGB5A3.block_height() {
        for x in block_x..block_x + TextureFormat::RGB5A3.block_width() {
            let [a, b] = if x < width && y < height {
                unsafe {
                    ColorData::Rgba(img.unsafe_get_pixel(x, y).0)
                        .to_rgb5a3(alpha)
                        .to_be_bytes()
                }
            } else {
                [0xFF, 0xFF]
            };

            out[offset] = a;
            offset += 1;
            out[offset] = b;
            offset += 1;
        }
    }
}

pub fn encode_rgba8(
    img: &DynamicImage,
    width: u32,
    height: u32,
    mut offset: usize,
    block_x: u32,
    block_y: u32,
    out: &mut Vec<u8>,
) {
    let block_width = TextureFormat::RGBA8.block_width();
    for i in 0..16 {
        let rel_x = i % block_width;
        let rel_y = i / block_width;
        let px_x = block_x + rel_x;
        let px_y = block_y + rel_y;
        let [r, g, b, a] = if px_x < width && px_y < height {
            unsafe { img.unsafe_get_pixel(px_x, px_y).0 }
        } else {
            [0xFF, 0xFF, 0xFF, 0xFF]
        };

        let pos = i as usize * 2;
        out[offset + pos] = a;
        out[offset + pos + 1] = r;
        out[offset + pos + 32] = g;
        out[offset + pos + 33] = b;
    }
}

pub fn encode_cmpr(
    img: &DynamicImage,
    alpha: JUTTransparency,
    width: u32,
    height: u32,
    mut offset: usize,
    block_x: u32,
    block_y: u32,
    out: &mut Vec<u8>,
) {
    let width = width as usize;
    let height = height as usize;

    for (sub_x, sub_y) in REL_MAP_2.iter() {
        let mut transparent = false;
        let mut colors = Vec::with_capacity(16);

        let subblock_x = block_x as usize + sub_x;
        let subblock_y = block_y as usize + sub_y;

        for (rel_x, rel_y) in REL_MAP_4.iter() {
            let x = subblock_x + rel_x;
            let y = subblock_y + rel_y;
            if x >= width || y >= height {
                continue;
            }

            let px = unsafe { img.unsafe_get_pixel(x as u32, y as u32).0 };
            if px[3] < 16 {
                transparent = true;
            } else {
                colors.push(px);
            }
        }
        let (color0, color1) = get_best_cmpr_key_colors(colors);
        let rgb565_0 = color0.to_rgb565();
        let rgb565_1 = color1.to_rgb565();

        // Order the colors based on conditions
        let (color0, color1, rgb565_0, rgb565_1) =
            if transparent && alpha != JUTTransparency::Opaque {
                // To denote transparency, the lesser color comes first
                if rgb565_0 > rgb565_1 {
                    (color1, color0, rgb565_1, rgb565_0)
                } else {
                    (color0, color1, rgb565_0, rgb565_1)
                }
            } else {
                // Not transparent, greater color comes first
                if rgb565_0 < rgb565_1 {
                    (color1, color0, rgb565_1, rgb565_0)
                } else {
                    (color0, color1, rgb565_0, rgb565_1)
                }
            };

        let mut colors = if transparent && alpha != JUTTransparency::Opaque {
            get_interpolated_cmpr_colors_alpha(rgb565_0, rgb565_1)
        } else {
            get_interpolated_cmpr_colors(rgb565_0, rgb565_1)
        };
        colors[0] = color0;
        colors[1] = color1;

        let bytes0: [u8; 2] = rgb565_0.to_be_bytes();
        let bytes1: [u8; 2] = rgb565_1.to_be_bytes();
        out[offset] = bytes0[0];
        offset += 1;
        out[offset] = bytes0[1];
        offset += 1;
        out[offset] = bytes1[0];
        offset += 1;
        out[offset] = bytes1[1];
        offset += 1;

        let mut inds: u32 = 0;
        for (i, (rel_x, rel_y)) in REL_MAP_4.iter().enumerate() {
            let x = subblock_x + rel_x;
            let y = subblock_y + rel_y;
            if x >= width || y >= height {
                continue;
            }

            let px = unsafe { ColorData::Rgba(img.unsafe_get_pixel(x as u32, y as u32).0) };

            let ind = if alpha != JUTTransparency::Opaque {
                get_nearest_color_index_fast(px, &colors)
            } else {
                if let Some(ind) = colors.iter().position(|c| c.loose_eq(&px)) {
                    ind
                } else {
                    get_nearest_color_index_fast(px, &colors)
                }
            } as u32;

            inds |= ind << ((15 - i) * 2);
        }
        let ind_bytes = inds.to_be_bytes();

        out[offset] = ind_bytes[0];
        offset += 1;
        out[offset] = ind_bytes[1];
        offset += 1;
        out[offset] = ind_bytes[2];
        offset += 1;
        out[offset] = ind_bytes[3];
        offset += 1;
    }
}

pub fn encode_c8(
    palette_inds: &Vec<u8>,
    width: u32,
    height: u32,
    mut offset: usize,
    block_x: u32,
    block_y: u32,
    out: &mut Vec<u8>,
) {
    for y in block_y..block_y + TextureFormat::IA4.block_height() {
        for x in block_x..block_x + TextureFormat::IA4.block_width() {
            let color = if x < width && y < height {
                palette_inds[(x + y * width) as usize]
            } else {
                0xFF
            };

            out[offset] = color;
            offset += 1;
        }
    }
}
