use std::{io, path::PathBuf};

use displaydoc::Display;
use image::ImageError;
use thiserror::Error;

use crate::{companion::error::CompanionError, gc::error::ReadError};

#[derive(Display, Error, Debug)]
pub enum PngWriteError {
    /// Error reading block: {0}
    ReadError(#[from] ReadError),
    /// Error saving image: {0}
    ImageError(#[from] ImageError),
    /// File has invalid name: {0}
    InvalidFileName(PathBuf),
    /// Companion file error: {0}
    CompanionError(#[from] CompanionError),
}

#[derive(Display, Error, Debug)]
pub enum PngReadError {
    /// Error loading image: {0}
    ImageError(#[from] ImageError),
    /// Error loading from disk: {0}
    IoError(#[from] io::Error),
    /// Companion file error: {0}
    CompanionError(#[from] CompanionError),
}

#[derive(Display, Error, Debug)]
pub enum BtiWriteError {
    /// Failed to write file: {0}
    Io(#[from] io::Error),
    /// File has invalid name: {0}
    InvalidFileName(PathBuf),
}
