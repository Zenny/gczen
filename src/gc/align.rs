use std::io::{Error, Seek, Write};

use crate::wraw;

// This whole string would never be used unless we went to like 64
const PAD_STRING: &str = "This is padding data to alignment";

pub enum AlignType {
    /// Use 0xFF bytes as pad data
    FF,
    /// Use 0x00 bytes as pad data
    Zero,
    /// Use the pad string as pad data
    String,
}

/// Write until we're at an address such that the NEXT write is on the aligned address.
/// Maximum alignment of 32
pub fn write_align<W: Write + Seek>(w: &mut W, align: u64, kind: AlignType) -> Result<(), Error> {
    // Sub 1 so the next write is on aligned address
    let required = align - (w.stream_position()? % align);

    if required == align {
        return Ok(());
    }

    let required = required as usize;

    match kind {
        AlignType::FF => {
            let bytes = vec![0xFFu8; required];
            wraw!(w, &bytes)?;
        }
        AlignType::Zero => {
            let bytes = vec![0x00u8; required];
            wraw!(w, &bytes)?;
        }
        AlignType::String => {
            let substr = &PAD_STRING[..required];
            wraw!(w, substr.as_bytes())?;
        }
    }

    Ok(())
}
