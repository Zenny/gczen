//! We write NKIT v1 ISO because it's easier and faster

use std::{
    fs::{
        create_dir_all,
        write,
        read,
        read_dir,
        File
    },
    io::{BufRead, BufReader, Cursor, Read, Seek, SeekFrom, Write},
    path::{Path, PathBuf}
};

use yaz0::{CompressionLevel, Yaz0Archive, Yaz0Writer};

use crate::{
    bytes::{read_u32, seek_near},
    companion::COMPANION_EXT,
    gc::{
        align::{write_align, AlignType},
        error::{IsoWriteError, ReadError, TocWriteError},
        file::GcFile,
        rarc::Rarc,
    },
    util::write_to_name_table,
    w, wat, wraw,
};

pub type Reader = BufReader<File>;

const ISO_HDR: &str = "_iso.hdr";
const APPLOADER_LDR: &str = "_apploader.ldr";
const START_DOL: &str = "_start.dol";
const GAME_TOC: &str = "_game.toc";

#[derive(Debug)]
pub struct IsoWorker {
    pub reader: Reader,
    /// All files that are bare in the ISO uncompressed
    pub files: Vec<GcFile>,
}

// toc = table of contents?
impl IsoWorker {
    pub fn read<P: AsRef<Path>>(path: P) -> Result<Self, ReadError> {
        let f = File::open(path)?;
        let mut b = BufReader::new(f);

        // Check that we are a GC ISO
        b.seek_relative(0x1C)?;
        let check = read_u32(&mut b)?;

        if check != 0xC2339F3D {
            return Err(ReadError::NotGc(format!("{:X}", check)));
        }

        // for a total of 1024 bytes skipped (0x400 - 0x1C - 0x4)
        b.seek_relative(0x3E0)?;

        let mut files: Vec<GcFile> = Vec::with_capacity(4);

        // Read the initial system files that have no built in pathing
        // 2 = ISO.hdr
        // 3 = AppLoader.ldr
        // 4 = Start.dol
        // 5 = Game.toc

        let app_len = read_u32(&mut b)? as usize;

        b.seek_relative(0x1C)?;
        let dolpos = read_u32(&mut b)?;
        let toc_start = read_u32(&mut b)?;
        let toc_len = read_u32(&mut b)? as usize;

        b.seek_relative(0x08)?;

        let _data_start = read_u32(&mut b)?;

        b.seek(SeekFrom::Start(0))?;

        let mut data = vec![0; 0x2440];
        b.read_exact(&mut data)?;

        files.push(GcFile::new(PathBuf::from(ISO_HDR), data, None)?);

        // These two read directly next to eachother
        data = vec![0; app_len];
        log::info!("APP {:X} {:X}", b.stream_position()?, app_len);
        b.read_exact(&mut data)?;

        files.push(GcFile::new(PathBuf::from(APPLOADER_LDR), data, None)?);

        log::info!("DOL {:X}", dolpos);

        b.seek(SeekFrom::Start(dolpos as u64))?;
        data = vec![0; (toc_start - dolpos) as usize];
        b.read_exact(&mut data)?;

        files.push(GcFile::new(PathBuf::from(START_DOL), data, None)?);

        log::info!("TCL {:X} {:X}", toc_start, toc_start + toc_len as u32);

        data = vec![0; toc_len];
        b.read_exact(&mut data)?;

        files.push(GcFile::new(PathBuf::from(GAME_TOC), data, None)?);

        let toc_files = Self::read_toc(&mut b, toc_start)?;

        files.extend(toc_files.into_iter());

        Ok(Self { reader: b, files })
    }

    fn read_toc(b: &mut Reader, toc_start: u32) -> Result<Vec<GcFile>, ReadError> {
        seek_near(b, toc_start as i64)?;

        let mut out = Vec::with_capacity(8);
        // Supposedly the fst is an actual entry, or I guess a "faux" entry, and all
        // of these fields we read are pretty relevant to how TOC file entries are regularly
        // read
        let fst = read_u32(b)?;
        if fst != 0x0100_0000 {
            return Err(ReadError::MultiFst(fst, 0));
        }
        let fst = read_u32(b)?;
        if fst != 0 {
            return Err(ReadError::MultiFst(fst, 1));
        }

        let num_entries = read_u32(b)?;
        // 12 being the length of an entry, we can find the start of the name table like this
        let name_table_start = num_entries * 12;
        let mut cur_path = PathBuf::new();
        let mut cutoff_inds = Vec::with_capacity(4);

        // Because the first little header (fst to num_entries) is 12 bytes as well, we start at 1
        // (because it's not really a valid file entry)
        for i in 1..num_entries {
            let toc_file = TocFileData::parse(b, name_table_start, toc_start)?;

            // If there's an entry at all (not root) and we're over cap, go back
            // 1 level of dir
            // (Be aware i is 1 indexed, not 0)
            while cutoff_inds.last().is_some_and(|v| i >= *v) {
                // We're done with that folder
                cur_path.pop();
                cutoff_inds.pop();
            }
            log::info!("TCI {}", i);
            let gc = if toc_file.is_dir {
                log::info!("TOC {:?} {}", toc_file, toc_file.start_or_parent);
                cur_path.push(toc_file.name);
                cutoff_inds.push(toc_file.len);
                GcFile::new(cur_path.clone(), Vec::new(), None)?
            } else {
                let pos = b.stream_position()?;
                log::info!(
                    "START {:X} {:X}",
                    toc_file.start_or_parent,
                    toc_file.start_or_parent + toc_file.len
                );
                b.seek(SeekFrom::Start(toc_file.start_or_parent as u64))?;
                let mut data = vec![0; toc_file.len as usize];
                b.read_exact(&mut data)?;

                let g = GcFile::new(cur_path.join(toc_file.name), data, None)?;
                b.seek(SeekFrom::Start(pos))?;
                g
            };

            out.push(gc);
        }

        Ok(out)
    }

    pub fn write<W: Write + Seek>(w: &mut W, proj_dir: PathBuf) -> Result<(), IsoWriteError> {
        // ISO/GCM layout:
        // ISO header
        //    notes:
        //      - 0x400 = App loader len
        //      - 0x420 = DOL data offset
        //      - 0x424 = TOC data offset
        //      - 0x428 = TOC len
        //      - 0x42C = TOC len again (all TOCs put together)
        //      - 0x434 = File data offset
        //      - 0x438 = File data len
        // AppLoader
        // some small stuff? padding? it's like 96 bytes of all 0, maybe aligned to 0x100 flat
        // DOL
        // TOC
        // seemingly junk data
        // File data (start eg 0x410000-0x57058000)
        //    notes:
        //      - the file data goes to 411960 and then next-lowest is 11311B3C, seemingly junk data
        // END OF FILE

        let build_dir = proj_dir.join("_build");
        let frag_dir = build_dir.join("frag");
        let szs_dir = build_dir.join("szs");

        if !szs_dir.exists() {
            create_dir_all(&szs_dir)?;
        }

        let mut toc = Vec::with_capacity(180);
        // 1.5gb reserved
        let mut data = Vec::with_capacity(1610612736);
        let mut data_cur = Cursor::new(&mut data);
        recur(
            frag_dir.clone(),
            &frag_dir,
            &szs_dir,
            0,
            &mut toc,
            &mut data_cur,
        )?;
        log::info!("Data compilation complete");

        let num_tocs = toc.len() + 1;
        let mut toc_data = Vec::with_capacity(num_tocs * 12);
        let mut toc_cur = Cursor::new(&mut toc_data);
        // insert first dummy entry
        wraw!(toc_cur, [0x01, 0, 0, 0, 0, 0, 0, 0])?;
        w!(toc_cur, num_tocs as u32)?;

        let mut toc_names = Vec::with_capacity(toc.iter().fold(0, |a, b| a + b.name.len() + 1));
        let mut names_cur = Cursor::new(&mut toc_names);

        let mut data_offset_pass = Vec::with_capacity(num_tocs);
        for item in toc {
            item.write(&mut toc_cur, &mut names_cur, &mut data_offset_pass)?;
        }

        let header_path = proj_dir.join(ISO_HDR);
        let mut header = read(header_path)?;
        let mut header_cur = Cursor::new(&mut header);
        let toc_len = (toc_data.len() + toc_names.len()) as u32;
        wat!(header_cur, toc_len, 0x428);
        wat!(header_cur, toc_len, 0x42C);
        wat!(header_cur, data.len() as u32, 0x438);

        let apploader_path = proj_dir.join(APPLOADER_LDR);
        let apploader = read(apploader_path)?;
        wat!(header_cur, apploader.len() as u32, 0x400);

        let dol_path = proj_dir.join(START_DOL);
        let dol = read(dol_path)?;

        wraw!(w, header)?;
        wraw!(w, apploader)?;

        write_align(w, 0x100, AlignType::Zero)?;

        let dol_offset = w.stream_position()? as u32;

        wat!(w, dol_offset, 0x420);

        wraw!(w, dol)?;

        let toc_offset = w.stream_position()?;

        wat!(w, toc_offset as u32, 0x424);

        wraw!(w, toc_data)?;
        wraw!(w, toc_names)?;

        write_align(w, 4, AlignType::Zero)?;

        let data_start_offset = w.stream_position()? as u32;
        wat!(w, data_start_offset, 0x434);

        // Modify our data offsets to be correct finally
        for item in data_offset_pass {
            wat!(
                w,
                data_start_offset + item.data_offset,
                toc_offset + item.at
            );
        }

        log::info!("Writing file data to ISO...");
        wraw!(w, data)?;
        log::info!("Done writing file data");

        Ok(())
    }

    /// It's important to be aware of where this W starts, because the data offset pass will be
    /// relative to that writer's start
    #[allow(unused)]
    fn write_toc<W: Write + Seek>(
        &self,
        w: &mut W,
        entries: Vec<TocFileData>,
        data_offset_pass: &mut Vec<DataPassInfo>,
    ) -> Result<(), TocWriteError> {
        w!(w, 0x0100_0000u32)?;
        w!(w, 0u32)?;
        // add one to include this data as an entry
        w!(w, entries.len() as u32 + 1)?;

        let mut name_table = Vec::with_capacity(1024);
        let mut name_table_cur = Cursor::new(&mut name_table);

        for entry in entries {
            entry.write(w, &mut name_table_cur, data_offset_pass)?;
        }

        // Write name table directly after
        wraw!(w, name_table)?;

        Ok(())
    }
}

struct DataPassInfo {
    /// The offset to write the file data offset to, relative to the start of the TOC writer
    pub at: u64,
    /// The file data offset relative to the start of the data section.
    /// Needs the rest of the data added to it to complete the location
    pub data_offset: u32,
}

#[derive(Debug, Clone)]
struct TocFileData {
    pub name: String,
    pub is_dir: bool,
    /// The start address of the data (relative to the start of the iso) if it's a file, if it's a
    /// dir this is the FST ind of the parent directory (0 for root)
    ///
    /// When writing, for files this will be the file's data offset relative to the data section
    pub start_or_parent: u32,
    /// The len of the file data if this is a file, if it's a dir this is the cutoff point
    /// where files are no longer in this dir (denoting the ID after the final TOC entry's ID)
    pub len: u32,
}

impl TocFileData {
    /// Assumes we're already in the correct position to start reading
    pub fn parse(b: &mut Reader, name_table_start: u32, toc_start: u32) -> Result<Self, ReadError> {
        let name_data = read_u32(b)?;
        let is_dir = name_data & 0x0100_0000 != 0;
        let name_offset = name_data & 0x00FF_FFFF;
        let start_or_parent = read_u32(b)?;
        // If we're a dir, this number represents a cutoff point before we need to go
        // backwards 1 level of dir
        let len = read_u32(b)?;
        let old_pos = b.stream_position()?;
        seek_near(b, (toc_start + name_table_start + name_offset) as i64)?;
        let mut buf_name = Vec::with_capacity(8);
        b.read_until(0, &mut buf_name)?;
        // remove trailing 0
        buf_name.pop();
        // Go back to prev location so next loop starts in the right spot
        seek_near(b, old_pos as i64)?;

        Ok(Self {
            name: String::from_utf8(buf_name)?,
            is_dir,
            start_or_parent,
            len,
        })
    }

    pub fn write<W: Write + Seek, X: Write + Seek>(
        &self,
        w: &mut W,
        name_table: &mut X,
        data_offset_pass: &mut Vec<DataPassInfo>,
    ) -> Result<(), TocWriteError> {
        let name_offset = u32::try_from(name_table.stream_position()?)
            .map_err(|_| TocWriteError::ExcessiveNames)?;
        if name_offset & 0xFF00_0000 != 0 {
            return Err(TocWriteError::ExcessiveNames);
        }
        write_to_name_table(name_table, self.name.clone())?;

        let mut name_data = name_offset;
        if self.is_dir {
            name_data |= 0x0100_0000;
        }

        w!(w, name_data)?;

        if self.is_dir {
            w!(w, self.start_or_parent)?;
        } else {
            // We need to alter this offset later to be correct
            data_offset_pass.push(DataPassInfo {
                at: w.stream_position()?,
                data_offset: self.start_or_parent,
            });

            w!(w, 0u32)?;
        }

        w!(w, self.len)?;

        Ok(())
    }
}

fn recur<W: Write + Seek>(
    path: PathBuf,
    frag_dir: &PathBuf,
    szs_dir: &PathBuf,
    dir_ind: u32,
    toc: &mut Vec<TocFileData>,
    file_data: &mut W,
) -> Result<(), IsoWriteError> {
    for entry in read_dir(path)? {
        let entry = entry?;
        let entry_path = entry.path();

        let os_name = entry_path.file_name().unwrap();
        let name = os_name.to_string_lossy().to_string();

        if name.ends_with(COMPANION_EXT) || name.starts_with('_') {
            continue;
        }

        log::debug!("Processing entry {:?}...", entry_path);

        // if szs then compile the szs and it's not a dir
        let is_szs = name.ends_with(".szs");
        let is_dir = !is_szs && entry_path.is_dir();

        if is_dir {
            toc.push(TocFileData {
                name,
                is_dir,
                start_or_parent: dir_ind,
                // fill out later if dir
                len: 0,
            });
            let entry_dir_ind = toc.len() as u32;
            recur(entry_path, frag_dir, szs_dir, entry_dir_ind, toc, file_data)?;
            // add 1, because the item after the items within this dir
            // should trigger the path to go back 1 dir before processing itself
            toc[entry_dir_ind as usize - 1].len = (toc.len() + 1) as u32;
        } else {
            let data = if is_szs {
                compile_szs(entry_path, frag_dir, szs_dir)?
            } else {
                read(entry_path)?
            };
            write_align(file_data, 4, AlignType::Zero)?;
            // todo limit to size of gamecube disc
            let start = u32::try_from(file_data.stream_position()?)
                .map_err(|_| IsoWriteError::ExcessiveFiles)?;
            let len = data.len() as u32;
            wraw!(file_data, data)?;

            toc.push(TocFileData {
                name,
                is_dir,
                start_or_parent: start,
                len,
            });
        };
    }

    Ok(())
}

fn compile_szs(
    entry_path: PathBuf,
    frag_dir: &PathBuf,
    szs_dir: &PathBuf,
) -> Result<Vec<u8>, IsoWriteError> {
    let mut preszs_data = Vec::with_capacity(2000000);
    let mut data_cur = Cursor::new(&mut preszs_data);
    let rel_path = entry_path.strip_prefix(frag_dir)?;
    log::debug!("Compiling SZS {:?}", rel_path);
    Rarc::compile_dir(&mut data_cur, entry_path.clone())?;
    // szs is yaz0'd
    let mut out = Vec::with_capacity(preszs_data.len());

    let cur_szs = szs_dir.join(rel_path);
    if cur_szs.exists() {
        let comp = read(&cur_szs)?;
        let cursor = Cursor::new(&comp);
        let decomp = Yaz0Archive::new(cursor)?.decompress()?;

        if preszs_data == decomp {
            log::debug!("SZS was cached, skipping...");
            return Ok(comp);
        }
    } else {
        // Create all the rel dirs so we can write the file
        if let Some(parent) = cur_szs.parent() {
            create_dir_all(parent)?;
        }
    }

    // Compressing is slow so info for visibility
    log::info!(
        "Compressing {:?} ({} bytes)...",
        entry_path,
        preszs_data.len()
    );
    Yaz0Writer::new(&mut out)
        .compress_and_write(&preszs_data, CompressionLevel::Lookahead { quality: 10 })?;
    log::info!("Done compressing");

    write(cur_szs, &out)?;

    Ok(out)
}
