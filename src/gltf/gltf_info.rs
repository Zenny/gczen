//! Convert gltf file into bmd related data

use std::{collections::HashMap, str::FromStr};

use glam::{Mat4, Quat, Vec3, Vec3A};
use mugltf::{Gltf, MeshPrimitive, Node};
use serde_json::{Number, Value};
use tristripper::{PrimitiveGroup, PrimitiveType, TriStripper};

use crate::{
    companion::bmd::BmdCompanion,
    errify, find_or_insert,
    gc::{
        bmd::{
            consts::{BILLBOARD_NORMAL, BILLBOARD_XY, BILLBOARD_Y},
            drw1::{Drw1, WeightKind},
            error::GltfReadError,
            evp1::Evp1,
            inf1::{self, Hierarchy, Inf1, NodeKind},
            jnt1::{BoneData, Config, Jnt1, MatrixKind},
            mat3::{Mat3, Material},
            processing::{inv_mat_mul_vert_norm, inv_mat_mul_vert_pos, MatrixDataCompactor},
            shp1::{Shape, Shp1},
            vtx1::Vtx1,
        },
        types::mesh::{
            DataVec, GxAttribute, IndexedPrimitive, IndexedVertex, Mode, PrimitiveKind,
            VertexDescriptor,
        },
    },
    gltf::{
        accessor_reader::{AccessorReader, BufferType},
        util::{parse_bounding, single_weight},
    },
    util::normalize_weights,
};

pub struct GltfInfo {
    accessor_reader: AccessorReader,
    pub shp1: Shp1,
    pub vtx1: Vtx1,
    pub jnt1: Jnt1,
    pub drw1: Drw1,
    pub evp1: Evp1,
    pub inf1: Inf1,
    pub mat3: Mat3,
    /// Since our gltf joints include faux joints for shapes, this remaps the real joints
    /// from where they are in the gltf list to where they should be minus the shapes.
    /// This also accesses the inv bind mats properly
    bone_remaps: HashMap<u16, u16>,
    /// The joint list taken from the skin
    all_bones: Vec<u16>,
}

impl GltfInfo {
    /// Buffer cache capacity should be how many buffers are in the gltf
    pub fn new(gltf: &Gltf, companion: BmdCompanion) -> Result<Self, GltfReadError> {
        if gltf.skins.len() == 0 {
            return Err(GltfReadError::NoSkins);
        } else if gltf.skins.len() > 1 {
            log::warn!("Model contains more than 1 skin. Only the first skin will be used.");
        }

        if gltf.scenes.len() == 0 {
            return Err(GltfReadError::NoScenes);
        } else if gltf.scenes.len() > 1 {
            log::warn!("Model contains more than 1 scene. Only the default scene will be used.");
        }

        if gltf.scene.is_none() {
            log::warn!("Model has no default scene. Scene 0 will be selected.");
        }

        let mut this = Self {
            accessor_reader: AccessorReader::new(gltf.buffers.len(), gltf.accessors.len()),
            shp1: Shp1 {
                len: 0,
                shapes: vec![],
            },
            vtx1: Vtx1 {
                formats: companion.vertex_data,
                attributes: Default::default(),
                len: 0,
            },
            jnt1: Jnt1 {
                len: 0,
                bone_data: vec![],
            },
            drw1: Drw1 {
                len: 0,
                matrix_entries: vec![],
            },
            evp1: Evp1 {
                len: 0,
                weights: vec![],
                matrices: vec![],
            },
            bone_remaps: HashMap::with_capacity(gltf.skins[0].joints.len()),
            inf1: Inf1 {
                len: 0,
                flags: 1,
                num_packets: 0,
                num_verts: 0,
                hierarchy_offset: 0,
                hierarchy: Hierarchy::new(),
            },
            mat3: Mat3 {
                indirect_texes: companion.indirect_tex_info,
                hard_params: companion.material_tables,
                materials: companion.materials,
                len: 0,
            },
            all_bones: gltf.skins[0]
                .joints
                .iter()
                .map(|v| u16::try_from(*v).map_err(|_| GltfReadError::UnsupportedNumNodes))
                .collect::<Result<Vec<_>, _>>()?,
        };

        this.preload_bones(gltf)?;

        let scene = &gltf.scenes[gltf.scene.unwrap_or_default()];

        if scene.nodes.len() == 0 {
            return Err(GltfReadError::NoObjects);
        } else if scene.nodes.len() > 1 {
            return Err(GltfReadError::UnsupportedNumObjects);
        }

        log::debug!("Processing hierarchy...");
        this.parse_from_nodes(gltf, scene.nodes[0], None, usize::MAX)?;

        // Finalize fields that require info from various sections
        this.inf1.num_packets = this
            .shp1
            .shapes
            .iter()
            .fold(0, |n, s| n + s.mat_prims.len()) as u32;
        let pos = errify!(
            this.vtx1.attributes.get(&GxAttribute::Position),
            GltfReadError::NoPosData
        )?;
        this.inf1.num_verts = pos.len() as u32;

        Ok(this)
    }

    fn preload_bones(&mut self, gltf: &Gltf) -> Result<(), GltfReadError> {
        let skin = &gltf.skins[0];

        log::debug!("Preloading bone data...");

        let inv_binds = if let Some(id) = skin.inverse_bind_matrices {
            let accessor = self.accessor_reader.read_accessor(gltf, id)?;
            accessor
                .clone()
                .into_iter()
                .map(|b| match b {
                    BufferType::Mat4(mat) => {
                        let mat = mat.map(|v| v.as_f32());
                        Ok(Mat4::from_cols_array(&mat))
                    }
                    t => Err(GltfReadError::InvalidInvBindType(t)),
                })
                .collect::<Result<Vec<_>, _>>()?
        } else {
            vec![Mat4::IDENTITY; skin.joints.len()]
        };

        let mut all_identity = true;
        let mut remap = 0;
        // The position in the lists relates inv binds and joints
        for (i, inv) in inv_binds.into_iter().enumerate() {
            let joint = skin.joints[i];
            let node = &gltf.nodes[joint];

            // If we're not a faux joint (shape) node
            if gltf
                .nodes
                .iter()
                .find(|n| n.mesh.is_some() && n.name == node.name)
                .is_none()
            {
                log::debug!("Processing joint {:?}", node.name);
                self.evp1.matrices.push(inv);
                if inv != Mat4::IDENTITY {
                    all_identity = false;
                }

                let matrix_kind = node.extras.get("billboard").map(|v| {
                    let val = v.as_u64().unwrap_or(BILLBOARD_NORMAL as u64) as usize;
                    if val == BILLBOARD_NORMAL {
                        MatrixKind::Standard
                    } else if val == BILLBOARD_Y {
                        MatrixKind::BillboardY
                    } else if val == BILLBOARD_XY {
                        MatrixKind::BillboardXY
                    } else {
                        log::warn!("Found invalid billboard value \"{}\" on bone {:?}: No billboarding will be applied.", val, node.name);
                        MatrixKind::Standard
                    }
                }).unwrap_or_else(|| MatrixKind::Standard);

                let flags = node
                    .extras
                    .get("flags")
                    .map(|v| v.as_u64().map(|v| u8::try_from(v).ok()).flatten())
                    .flatten()
                    .unwrap_or_else(|| 0);

                let compensate_scale = node
                    .extras
                    .get("compensate_scale")
                    .map(|v| v.as_u64())
                    .flatten()
                    .unwrap_or_else(|| 0)
                    != 0;

                let translate = node
                    .translation
                    .map(|v| Vec3::from_array(v))
                    .unwrap_or_else(|| Vec3::ZERO);
                let rotation = node
                    .rotation
                    .map(|v| Quat::from_array(v))
                    .unwrap_or_else(|| Quat::IDENTITY);
                let scale = node
                    .scale
                    .map(|v| Vec3::from_array(v))
                    .unwrap_or_else(|| Vec3::ONE);

                let (bounding_sphere_radius, bounding_box_min, bounding_box_max) =
                    parse_bounding(node);

                let bone_data = BoneData {
                    name: node.name.clone(),
                    config: Config {
                        matrix_kind,
                        flags,
                        compensate_scale,
                        scale,
                        rotation,
                        translate,
                        bounding_sphere_radius,
                        bounding_box_min,
                        bounding_box_max,
                    },
                };
                self.jnt1.bone_data.push(bone_data);

                let joint = u16::try_from(joint).map_err(|_| GltfReadError::UnsupportedNumNodes)?;

                self.bone_remaps.insert(joint, remap as u16);

                remap += 1;
            }
        }

        // todo it's possible that more checks are needed to make sense, such as no skinned meshes?
        // Since we inject identity in as inv_binds if one is not present (see: add_joint_to_skin)
        // a matrix of all identity inv_binds implies there were none
        if all_identity {
            self.evp1.matrices.clear();
        }

        Ok(())
    }

    pub fn parse_from_nodes(
        &mut self,
        gltf: &Gltf,
        cur_node_id: usize,
        mut parent: Option<u16>,
        mut material_id: usize,
    ) -> Result<(), GltfReadError> {
        let node = &gltf.nodes[cur_node_id];
        // If we have a mesh field, we're a shape, but we need to start with a faux joint so we
        // have all the info. Otherwise the check below would still pass and be wrong
        if node.mesh.is_some() {
            return Ok(());
        }

        let mut this_id = self.inf1.hierarchy.next_id();

        // Because our joints also contain faux shape nodes for hierarchy sake, to be sure
        // we're a shape we need to find another node related by name which holds mesh
        if let Some(real_node) = gltf
            .nodes
            .iter()
            .find(|n| n.mesh.is_some() && n.name == node.name)
        {
            // shape
            log::debug!("Processing shape {:?}", node.name);
            let mesh_id = real_node.mesh.unwrap();
            let mesh = &gltf.meshes[mesh_id];

            if mesh.primitives.len() == 0 {
                // todo maybe we can just skip these meshes
                return Err(GltfReadError::NoPrimitives);
            }

            // Theoretically, all the prims materials should be the same. But if they aren't,
            // we just pick the minimum. It's possible that no prim has a material, in which
            // case min_mat_id will be None
            let mut min_mat_id: Option<usize> = None;
            let mut min_gltf_mat_id = 0;
            let mut multi_materials = false;
            for prim in mesh.primitives.iter() {
                if let Some(mat) = &prim.material {
                    let material = &gltf.materials[*mat];
                    let mat_id = material
                        .extras
                        .get("index")
                        .map(|v| {
                            v.as_u64().map(|v| {
                                let usz = usize::try_from(v).ok();
                                // 64 bit systems will support everything, however 32 bit systems will not
                                if usz.is_none() {
                                    log::warn!(
                                        "Material {:?} has invalid index. Index will not be used.",
                                        material.name
                                    );
                                }

                                usz
                            })
                        })
                        .flatten()
                        .flatten();

                    let Some(mat_id) = mat_id else {
                        // todo warn in blender export
                        log::warn!("Material {:?} has no index.", material.name);
                        continue;
                    };

                    if min_mat_id.is_none() {
                        min_gltf_mat_id = *mat;
                        min_mat_id = Some(mat_id);
                    } else if min_mat_id.is_some_and(|min| mat_id < min) {
                        multi_materials = true;
                        min_gltf_mat_id = *mat;
                        min_mat_id = Some(mat_id);
                    } else if min_mat_id.is_some_and(|min| mat_id > min) {
                        multi_materials = true;
                    }
                }
            }

            if multi_materials {
                // todo the CORRECT implementation of this is to split all of the primitives with
                //  their own materials into their own mesh/shape but it's lazy mode right now
                log::warn!("Multiple materials found for mesh {:?}. The lowest material will be used for all parts.", mesh.name);
            }

            let this_mat_id = if let Some(mat_id) = min_mat_id {
                let mat = &gltf.materials[min_gltf_mat_id];

                if let Some(material) = self.mat3.materials.get_mut(mat_id) {
                    // Update the name of our material
                    material.name = if mat.name.len() > 0 {
                        mat.name.clone()
                    } else {
                        format!("Unknown_{}", mat_id)
                    };

                    mat_id
                } else {
                    log::warn!(
                        "No material found at index {} for mesh {:?}. A default material will be used.",
                        mat_id,
                        mesh.name
                    );
                    let new_mat_id = self.mat3.materials.len();
                    // todo resave companion file and reload/replace model in editor?
                    //  should warn that there is a default in console so they replace it
                    self.mat3.materials.push(Material::simple_material(
                        format!("simple_{}", new_mat_id),
                        &mut self.mat3.hard_params,
                    ));
                    new_mat_id
                }
            } else {
                // todo warn in blender export
                log::warn!(
                    "No material found for mesh {:?}. A default material will be used.",
                    mesh.name
                );
                let new_mat_id = self.mat3.materials.len();
                // If this material is brand new, we need to insert it
                // todo resave companion file and reload/replace model in editor?
                //  should warn that there is a default in console so they replace it
                self.mat3.materials.push(Material::simple_material(
                    format!("simple_{}", new_mat_id),
                    &mut self.mat3.hard_params,
                ));
                new_mat_id
            };

            // We found a change in material
            if this_mat_id != material_id {
                // We need to insert this new material as a node which will parent the shape
                let mat_node = inf1::Node {
                    // I made up the ID so let's make it up again who cares as long as it's unique
                    id: this_id,
                    ind: this_mat_id as u16,
                    kind: NodeKind::Material,
                    parent,
                    children: vec![],
                };
                self.inf1.hierarchy.insert(this_id, mat_node);

                // We're going to change parents so...
                // Update the parent to have this as the child
                if let Some(parent) = &parent {
                    let parent_node = self.inf1.hierarchy.get_mut(parent).unwrap();
                    parent_node.children.push(this_id);
                }

                // Material is the parent now
                parent = Some(this_id);
                // Shape needs a new ID
                this_id = self.inf1.hierarchy.next_id();
                material_id = this_mat_id;
            }

            let billboard = errify!(
                node.extras
                    .get("billboard")
                    .cloned()
                    .unwrap_or_else(|| Value::Number(Number::from(0)))
                    .as_u64(),
                GltfReadError::InvalidBillboard(node.name.clone())
            )?;

            if billboard > 2 {
                return Err(GltfReadError::InvalidBillboard(node.name.clone()));
            }

            let shape = self.make_shape(&mesh.primitives, gltf, billboard, node)?;
            let shape_ind = self.shp1.shapes.len();
            self.shp1.shapes.push(shape);

            let inf1_node = inf1::Node {
                // I made up the ID so let's make it up again who cares as long as it's unique
                id: this_id,
                // todo all of these inds should error if more than u16
                ind: shape_ind as u16,
                kind: NodeKind::Shape,
                parent,
                children: vec![],
            };

            // Update the parent to have this as the child
            if let Some(parent) = &parent {
                let parent_node = self.inf1.hierarchy.get_mut(parent).unwrap();
                parent_node.children.push(this_id);
            }

            self.inf1.hierarchy.insert(this_id, inf1_node);
        } else if self.bone_remaps.contains_key(&(cur_node_id as u16)) {
            // joint
            log::debug!("Processing joint {:?}", node.name);

            let inf1_node = inf1::Node {
                // I made up the ID so let's make it up again who cares as long as it's unique
                id: this_id,
                // It should always exist
                ind: self
                    .bone_remaps
                    .get(&(cur_node_id as u16))
                    .cloned()
                    .unwrap(),
                kind: NodeKind::Joint,
                parent,
                children: vec![],
            };

            // Update the parent to have this as the child
            if let Some(parent) = &parent {
                let parent_node = self.inf1.hierarchy.get_mut(parent).unwrap();
                parent_node.children.push(this_id);
            }

            self.inf1.hierarchy.insert(this_id, inf1_node);
        } else {
            let skin = &gltf.skins[0];
            // Pretend node named after skin never existed (blender makes a new node for it)
            if node.name != skin.name {
                // Some kind of empty object
                // return early because this node does nothing else for us
                return Ok(());
            }
        };

        for child in &node.children {
            let skin = &gltf.skins[0];
            // Pretend node named after skin never existed (blender makes a new node for it)
            let parent = if node.name == skin.name {
                None
            } else {
                Some(this_id)
            };

            self.parse_from_nodes(gltf, *child, parent, material_id)?;
        }

        Ok(())
    }

    pub fn make_shape(
        &mut self,
        prims: &Vec<MeshPrimitive>,
        gltf: &Gltf,
        billboard: u64,
        shape_node: &Node,
    ) -> Result<Shape, GltfReadError> {
        log::debug!("Generating shape...");
        let mut mode = match billboard {
            0 => Mode::Normal,
            1 => Mode::BillboardY,
            2 => Mode::BillboardXY,
            _ => unreachable!(),
        };
        let mut desc = VertexDescriptor::new();

        let mut mat_compactor = MatrixDataCompactor::new();
        // A PNMI vec for each MatrixData
        let mut pnmi_store = Vec::with_capacity(16);
        let mut definitely_skinned = false;

        for (i, prim) in prims.into_iter().enumerate() {
            let inds = errify!(
                prim.indices,
                GltfReadError::NoIndices(i, shape_node.name.clone())
            )?;
            let inds_data = self.accessor_reader.read_accessor(gltf, inds)?;
            let inds_data = inds_data
                .into_iter()
                .map(|v| {
                    // Theoretically it'll always be scalar
                    if let BufferType::Scalar(v) = v {
                        Ok(v.as_u32())
                    } else {
                        Err(GltfReadError::UnsupportedIndsType)
                    }
                })
                .collect::<Result<Vec<_>, _>>()?;

            //log::info!("Inds {} {:?}", inds_data.len(), inds_data);

            // Convert the mesh primitives further into tristrips
            let mut strip = TriStripper::new(&inds_data, Some(32), true);
            let mut prim_groups = Vec::with_capacity(18);
            strip.strip(&mut prim_groups);

            if prim_groups.len() == 0 {
                log::warn!("TriStripper created no strips for primitive #{}!", i);
            }

            //log::info!("Tris {:?}", prim_groups);

            #[cfg(debug_assertions)]
            {
                use itertools::Itertools;
                let b: Vec<u32> = prim_groups
                    .iter()
                    .fold(vec![], |mut a, b| {
                        a.extend(&b.indices);
                        a
                    })
                    .into_iter()
                    .unique()
                    .collect();

                // Our strips should contain all of our ind data
                for v in inds_data.iter() {
                    assert!(b.contains(v));
                }
            }

            for strip in prim_groups {
                match strip.kind {
                    PrimitiveType::Triangles => {
                        // Process each triangle by itself, there's probably a better way? todo
                        for tri_start in (0..strip.indices.len()).step_by(3) {
                            let tri = PrimitiveGroup {
                                indices: vec![
                                    strip.indices[tri_start],
                                    strip.indices[tri_start + 1],
                                    strip.indices[tri_start + 2],
                                ],
                                kind: PrimitiveType::Triangles,
                            };

                            self.process_strip(
                                tri,
                                gltf,
                                prim,
                                &mut definitely_skinned,
                                &mut mat_compactor,
                                &mut pnmi_store,
                                &mut desc,
                                &shape_node.name,
                            )?;
                        }
                    }
                    PrimitiveType::TriangleStrip => {
                        self.process_strip(
                            strip,
                            gltf,
                            prim,
                            &mut definitely_skinned,
                            &mut mat_compactor,
                            &mut pnmi_store,
                            &mut desc,
                            &shape_node.name,
                        )?;
                    }
                }
            }
        }

        // We had to wait until we were done checking everything to detect which skin type we
        // are. If we're a normal mode, we're already done as everything is single weighted.
        // Otherwise, we have to be a skinned mesh to access various weights
        let mut mat_prims = mat_compactor.finalize();
        // If we have an envelope type inserted, we're definitely skinned. However if the total
        // of all inds is more than 1, we have to be skinned as well (eg. many single weights
        // for a single shape)
        // Assumption: Technically multiple single weights can also work using cur, but it
        // doesn't really make sense for billboarding to have more than 1 influence...
        if definitely_skinned || mat_prims.iter().fold(0, |a, b| a + b.inds.len()) > 1 {
            // Multi weighted mesh can only be skinned
            mode = Mode::SkinnedMesh;
            for (mat_ind, mat_data) in mat_prims.iter_mut().enumerate() {
                let mut i = 0;
                for prim in mat_data.prims.iter_mut() {
                    for ind_vert in prim.verts.iter_mut() {
                        ind_vert.set(GxAttribute::PositionNormalMatrixInd, pnmi_store[mat_ind][i]);
                        i += 1;
                    }
                }
            }
            // PNMI is only set for skinned shapes
            desc.attributes.insert(
                GxAttribute::PositionNormalMatrixInd,
                GxAttribute::PositionNormalMatrixInd.default_kind().unwrap(),
            );
        }

        let (bounding_sphere_radius, bounding_box_min, bounding_box_max) =
            parse_bounding(shape_node);

        // To fully flesh out all of the info that should exist
        desc.calc_descriptor();

        Ok(Shape {
            mode,
            mat_prims,
            vertex_descriptor: desc,
            bounding_sphere_radius,
            bounding_box_min,
            bounding_box_max,
        })
    }

    fn process_strip(
        &mut self,
        strip: PrimitiveGroup,
        gltf: &Gltf,
        prim: &MeshPrimitive,
        definitely_skinned: &mut bool,
        mat_compactor: &mut MatrixDataCompactor,
        pnmi_store: &mut Vec<Vec<u16>>,
        desc: &mut VertexDescriptor,
        shape_name: &str,
    ) -> Result<(), GltfReadError> {
        log::trace!("Processing strip...");
        let mut ind_prim = IndexedPrimitive::new(PrimitiveKind::TriangleStrip, strip.indices.len());
        let mut inds = Vec::with_capacity(strip.indices.len());

        for (i, vert_ind) in strip.indices.iter().enumerate() {
            let mut ind_vert = IndexedVertex::new();

            self.process_attributes(
                &mut ind_vert,
                vert_ind,
                gltf,
                prim,
                definitely_skinned,
                &mut inds,
                desc,
                shape_name,
            )?;

            // todo instead of mem replace maybe use the verts vec that already exists
            let _ = std::mem::replace(&mut ind_prim.verts[i], ind_vert);
        }

        #[cfg(debug_assertions)]
        {
            assert_eq!(strip.indices.len(), inds.len());
            assert_eq!(strip.indices.len(), ind_prim.verts.len());
        }

        // Since the inds are the same order as they were passed in, this
        // should align with the order of the verts here
        let (mat_ind, pnmi_inds) = mat_compactor.insert_prim(inds, ind_prim);

        #[cfg(debug_assertions)]
        {
            assert_eq!(strip.indices.len(), pnmi_inds.len());
        }

        // technically should never be greater than, only equal
        if mat_ind >= pnmi_store.len() {
            pnmi_store.push(pnmi_inds);
        } else {
            pnmi_store[mat_ind].extend(pnmi_inds.into_iter());
        }

        Ok(())
    }

    fn process_attributes(
        &mut self,
        ind_vert: &mut IndexedVertex,
        vert_ind: &u32,
        gltf: &Gltf,
        prim: &MeshPrimitive,
        definitely_skinned: &mut bool,
        inds: &mut Vec<u16>,
        desc: &mut VertexDescriptor,
        shape_name: &str,
    ) -> Result<(), GltfReadError> {
        log::trace!("Processing attributes for vert {}...", vert_ind);
        let mut tmp_joints: Option<BufferType> = None;
        let mut tmp_weights: Option<BufferType> = None;

        for (attr_str, accessor_ind) in prim.attributes.iter() {
            // We need to handle joints and weights specially as GxAttribute does not
            // support them. They're both equal to PositionNormalMatrixInd in a way.
            // They don't EXACTLY mean that PNMI exists, because all meshes are weighted.
            // We need to find out if the weight is real or placeholder (single weight)
            match attr_str.as_str() {
                // todo self.mat_prims[mat_prim_ind].inds is accessed if more
                //  than one weight, if one weigh then cur is accessed.
                //  cur seems to point to inds[0]
                // We need to check both weights and joints to make certain of pnmi
                "JOINTS_0" => {
                    let data = self.accessor_reader.read_accessor(gltf, *accessor_ind)?;
                    let data_piece = &data[*vert_ind as usize];

                    if let Some(weights) = tmp_weights {
                        match data_piece {
                            BufferType::Vec2(bones) => {
                                let mut weights = weights.as_vec2();
                                let weight_ref = weights.as_mut();
                                let bone_slice = bones.map(|v| v.as_u32());

                                let drw1_ind = self.insert_weights(
                                    weight_ref,
                                    bone_slice.as_ref(),
                                    shape_name,
                                    definitely_skinned,
                                )?;

                                inds.push(drw1_ind);
                            }
                            BufferType::Vec3(bones) => {
                                let mut weights = weights.as_vec3a();
                                let weight_ref = weights.as_mut();
                                let bone_slice = bones.map(|v| v.as_u32());

                                let drw1_ind = self.insert_weights(
                                    weight_ref,
                                    bone_slice.as_ref(),
                                    shape_name,
                                    definitely_skinned,
                                )?;

                                inds.push(drw1_ind);
                            }
                            BufferType::Vec4(bones) | BufferType::Mat2(bones) => {
                                let mut weights = weights.as_vec4();
                                let weight_ref = weights.as_mut();
                                let bone_slice = bones.map(|v| v.as_u32());

                                let drw1_ind = self.insert_weights(
                                    weight_ref,
                                    bone_slice.as_ref(),
                                    shape_name,
                                    definitely_skinned,
                                )?;

                                inds.push(drw1_ind);
                            }
                            _ => return Err(GltfReadError::UnsupportedNumBoneInfluences),
                        }
                    } else {
                        // We need all the data first
                        tmp_joints = Some(*data_piece);
                    }
                }
                "WEIGHTS_0" => {
                    let data = self.accessor_reader.read_accessor(gltf, *accessor_ind)?;
                    let data_piece = &data[*vert_ind as usize];

                    if let Some(bones) = tmp_joints {
                        match data_piece {
                            BufferType::Vec2(weights) => {
                                let bone_slice = match bones {
                                    BufferType::Vec2(v) => v.map(|d| d.as_u32()),
                                    _ => panic!("Mismatched bone and weight types"),
                                };
                                let mut weights = weights.map(|v| v.as_f32());
                                let weight_ref = weights.as_mut();

                                let drw1_ind = self.insert_weights(
                                    weight_ref,
                                    bone_slice.as_ref(),
                                    shape_name,
                                    definitely_skinned,
                                )?;

                                inds.push(drw1_ind);
                            }
                            BufferType::Vec3(weights) => {
                                let bone_slice = match bones {
                                    BufferType::Vec3(v) => v.map(|d| d.as_u32()),
                                    _ => panic!("Mismatched bone and weight types"),
                                };
                                let mut weights = weights.map(|v| v.as_f32());
                                let weight_ref = weights.as_mut();

                                let drw1_ind = self.insert_weights(
                                    weight_ref,
                                    bone_slice.as_ref(),
                                    shape_name,
                                    definitely_skinned,
                                )?;

                                inds.push(drw1_ind);
                            }
                            BufferType::Vec4(weights) | BufferType::Mat2(weights) => {
                                let bone_slice = match bones {
                                    BufferType::Vec4(v) | BufferType::Mat2(v) => {
                                        v.map(|d| d.as_u32())
                                    }
                                    _ => panic!("Mismatched bone and weight types"),
                                };
                                let mut weights = weights.map(|v| v.as_f32());
                                let weight_ref = weights.as_mut();

                                let drw1_ind = self.insert_weights(
                                    weight_ref,
                                    bone_slice.as_ref(),
                                    shape_name,
                                    definitely_skinned,
                                )?;

                                inds.push(drw1_ind);
                            }
                            _ => return Err(GltfReadError::UnsupportedNumWeights),
                        }
                    } else {
                        // We need all the data first
                        tmp_weights = Some(*data_piece);
                    }
                }
                _ => {
                    // We need to wait for weight processing to be done first because
                    // single weighted stuff needs to be modified
                }
            }
        }

        // Last entry was just pushed in by us for the weights above
        let drw1_data = &self.drw1.matrix_entries[*inds.last().unwrap() as usize];

        for (attr_str, accessor_ind) in prim.attributes.iter() {
            let data = self.accessor_reader.read_accessor(gltf, *accessor_ind)?;
            let data_piece = &data[*vert_ind as usize];

            match attr_str.as_str() {
                "JOINTS_0" | "WEIGHTS_0" => {}
                supported => {
                    let attr = GxAttribute::from_str(supported)?;
                    // This will never fail in this case. Technically, in this
                    // case the kind is always Short
                    let kind = attr.default_kind().unwrap();
                    let datavec =
                        self.vtx1
                            .attributes
                            .entry(attr)
                            .or_insert_with(|| match data_piece {
                                BufferType::Scalar(_) => DataVec::Float(Vec::with_capacity(16)),
                                BufferType::Vec2(_) => DataVec::Vec2(Vec::with_capacity(16)),
                                BufferType::Vec3(_) => DataVec::Vec3(Vec::with_capacity(16)),
                                BufferType::Vec4(_) | BufferType::Mat2(_) => {
                                    DataVec::Color(Vec::with_capacity(16))
                                }
                                _ => unreachable!("this should never happen"),
                            });

                    let attr_ind = match datavec {
                        DataVec::Float(v) => {
                            let data = data_piece.as_f32();
                            find_or_insert!(v, data)
                        }
                        DataVec::Vec2(v) => {
                            let data = data_piece.as_vec2();
                            find_or_insert!(v, data)
                        }
                        DataVec::Vec3(_) => {
                            let mut vec = data_piece.as_vec3a();

                            if drw1_data.kind == WeightKind::Bone
                                && (attr == GxAttribute::Position || attr == GxAttribute::Normal)
                            {
                                // We need to undo inverse bind matrix for WeightKind::Bone verts
                                vec =
                                    self.untransform_vert_data(vec, drw1_data.ind as usize, attr)?;
                            }

                            // This is kind of dumb, but we need the data first to edit it, and
                            // then we need to insert it, but references made it annoying to insert
                            let entry = self.vtx1.attributes.get_mut(&attr).unwrap();
                            match entry {
                                DataVec::Vec3(v) => {
                                    find_or_insert!(v, vec)
                                }
                                _ => unreachable!(),
                            }
                        }
                        DataVec::Color(v) => {
                            let data = data_piece.as_color();
                            find_or_insert!(v, data)
                        }
                    };

                    ind_vert.set(attr, attr_ind as u16);
                    desc.attributes.insert(attr, kind);
                }
            }
        }

        Ok(())
    }

    /// Untransforms some indexed vert data (like positions) against some matrix
    /// so they undo what export did
    fn untransform_vert_data(
        &self,
        data: Vec3A,
        bone_ind: usize,
        attr: GxAttribute,
    ) -> Result<Vec3A, GltfReadError> {
        if self.evp1.matrices.len() > bone_ind {
            let mat = errify!(
                self.evp1.matrices.get(bone_ind),
                GltfReadError::InvalidEvp1Matrix(bone_ind)
            )?;

            Ok(match attr {
                GxAttribute::Position => inv_mat_mul_vert_pos(data, mat),
                GxAttribute::Normal => inv_mat_mul_vert_norm(data, mat),
                _ => unreachable!(),
            })
        } else {
            let bone_data = errify!(
                self.jnt1.bone_data.get(bone_ind),
                GltfReadError::InvalidJnt1Bone(bone_ind)
            )?;
            let m4 = Mat4::from_scale_rotation_translation(
                bone_data.config.scale,
                bone_data.config.rotation,
                bone_data.config.translate,
            );
            let m4 = m4.inverse();

            Ok(m4.transform_vector3a(data))
        }
    }

    fn insert_weights(
        &mut self,
        weight_ref: &mut [f32],
        bone_slice: &[u32],
        shape_name: &str,
        definitely_skinned: &mut bool,
    ) -> Result<u16, GltfReadError> {
        normalize_weights(weight_ref);

        // Remap the bones from an index into our joint list, to the node index, to the joint index
        // it would be if shapes were removed (they aren't real joints)
        let bone_slice = bone_slice
            .into_iter()
            .map(|v| {
                usize::try_from(*v)
                    .map(|v| {
                        let real_bone =
                            errify!(self.all_bones.get(v), GltfReadError::InvalidWeightBone(v))?;
                        errify!(
                            self.bone_remaps.get(real_bone).cloned(),
                            GltfReadError::InvalidBoneRemap(*real_bone)
                        )
                    })
                    .map_err(|_| GltfReadError::TooManyBones)
            })
            .flatten()
            .collect::<Result<Vec<_>, _>>()?;

        // If only one bone is weighted, we are a WeightKind of Bone
        let drw1_ind = if let Some(ind) = single_weight(weight_ref, shape_name) {
            // bone inds here point to index in joint list and bind mats
            let bone = bone_slice[ind];
            let drw_ind = self.drw1.insert_matrix(WeightKind::Bone, bone)?; // an ind into evp1.matrices

            drw_ind
        } else {
            *definitely_skinned = true;

            let evp_ind = self.evp1.insert_raw_weights(weight_ref, &bone_slice);
            let drw_ind = self
                .drw1
                .insert_matrix(WeightKind::Envelope, evp_ind as u16)?;

            drw_ind
        } as u16;

        Ok(drw1_ind)
    }
}
