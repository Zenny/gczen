use std::collections::HashMap;

use base64::Engine;
use glam::{Vec2, Vec3A, Vec4};
use bevy_color::Color;
use bytes::{Buf, Bytes};
use mugltf::{Accessor, AccessorComponentType, AccessorType, Gltf};

use crate::{
    errify,
    gc::types::mesh::{DataVec, GxDataType},
    gltf::error::AccessorReadError,
    util::remove_start_letters,
};

/*
   Byte
   UnsignedByte
   Short
   UnsignedShort
   UnsignedInt
   Float
*/
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum BufferData {
    Byte(i8),
    UByte(u8),
    Short(i16),
    UShort(u16),
    UInt(u32),
    Float(f32),
}

impl BufferData {
    pub fn is_zero(&self) -> bool {
        match self {
            BufferData::Byte(v) => *v == 0,
            BufferData::UByte(v) => *v == 0,
            BufferData::Short(v) => *v == 0,
            BufferData::UShort(v) => *v == 0,
            BufferData::UInt(v) => *v == 0,
            BufferData::Float(v) => *v == 0.0,
        }
    }

    /// Panics if it's a float or negative todo error
    pub fn as_u32(&self) -> u32 {
        match self {
            BufferData::Byte(v) => {
                if v.is_negative() {
                    panic!("BufferData negative i8 can't be u32");
                }
                *v as u32
            }
            BufferData::UByte(v) => *v as u32,
            BufferData::Short(v) => {
                if v.is_negative() {
                    panic!("BufferData negative i16 can't be u32");
                }
                *v as u32
            }
            BufferData::UShort(v) => *v as u32,
            BufferData::UInt(v) => *v,
            BufferData::Float(_) => panic!("BufferData f32 can't be u32"),
        }
    }

    /// Converts every kind of type to f32. For example, if it's a u8, the range of 0 to 255 is
    /// converted to a range of 0.0 to 1.0
    pub fn as_f32(&self) -> f32 {
        match self {
            BufferData::Byte(v) => *v as f32 * (1.0 / 128.0),
            BufferData::UByte(v) => *v as f32 * (1.0 / u8::MAX as f32),
            BufferData::Short(v) => *v as f32 * (1.0 / 32768.0),
            BufferData::UShort(v) => *v as f32 * (1.0 / u16::MAX as f32),
            BufferData::UInt(v) => *v as f32 * (1.0 / u32::MAX as f32),
            BufferData::Float(v) => *v,
        }
    }

    pub fn to_datatype(&self) -> GxDataType {
        match self {
            BufferData::Byte(_) => GxDataType::I8,
            BufferData::UByte(_) => GxDataType::U8,
            BufferData::Short(_) => GxDataType::I16,
            BufferData::UShort(_) => GxDataType::U16,
            BufferData::UInt(i) => {
                // This may never be called. But just in case, let's fall back to
                // u16 unless we can't
                let i = *i;
                if i <= u16::MAX as u32 {
                    GxDataType::U16
                } else {
                    // todo could be a recoverable error with file warning
                    panic!(
                        "Unsupported u32 range for BufferData. u32 {} is larger than u16",
                        i
                    );
                }
            }
            BufferData::Float(_) => GxDataType::F32,
        }
    }
}

impl Into<BufferData> for i8 {
    fn into(self) -> BufferData {
        BufferData::Byte(self)
    }
}

impl Into<BufferData> for u8 {
    fn into(self) -> BufferData {
        BufferData::UByte(self)
    }
}

impl Into<BufferData> for i16 {
    fn into(self) -> BufferData {
        BufferData::Short(self)
    }
}

impl Into<BufferData> for u16 {
    fn into(self) -> BufferData {
        BufferData::UShort(self)
    }
}

impl Into<BufferData> for u32 {
    fn into(self) -> BufferData {
        BufferData::UInt(self)
    }
}

impl Into<BufferData> for f32 {
    fn into(self) -> BufferData {
        BufferData::Float(self)
    }
}

/*
   Scalar,
   Vec2,
   Vec3,
   Vec4,
   Mat2,
   Mat3,
   Mat4,
*/
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum BufferType {
    Scalar(BufferData),
    Vec2([BufferData; 2]),
    Vec3([BufferData; 3]),
    Vec4([BufferData; 4]),
    Mat2([BufferData; 4]),
    Mat3([BufferData; 9]),
    Mat4([BufferData; 16]),
}

impl BufferType {
    pub fn as_slice(&self) -> &[BufferData] {
        match self {
            BufferType::Vec2(v) => v,
            BufferType::Vec3(v) => v,
            BufferType::Vec4(v) => v,
            BufferType::Mat2(v) => v,
            BufferType::Mat3(v) => v,
            BufferType::Mat4(v) => v,
            _ => unimplemented!("Can't slice scalar"),
        }
    }

    /// Panics if conversion will not work
    pub fn as_f32(&self) -> f32 {
        match self {
            BufferType::Scalar(f) => f.as_f32(),
            a => panic!("Buffertype {:?} cannot be f32", a),
        }
    }

    /// Panics if conversion will not work
    pub fn as_vec2(&self) -> Vec2 {
        match self {
            BufferType::Vec2(f) => Vec2::new(f[0].as_f32(), f[1].as_f32()),
            a => panic!("Buffertype {:?} cannot be vec2", a),
        }
    }

    /// Panics if conversion will not work
    pub fn as_vec3a(&self) -> Vec3A {
        match self {
            BufferType::Vec3(f) => Vec3A::new(f[0].as_f32(), f[1].as_f32(), f[2].as_f32()),
            a => panic!("Buffertype {:?} cannot be vec3", a),
        }
    }

    /// Panics if conversion will not work
    pub fn as_color(&self) -> Color {
        match self {
            BufferType::Vec4(f) | BufferType::Mat2(f) => {
                Color::linear_rgba(f[0].as_f32(), f[1].as_f32(), f[2].as_f32(), f[3].as_f32())
            }
            a => panic!("Buffertype {:?} cannot be color", a),
        }
    }

    /// Panics if conversion will not work
    /// The difference between this and Color is that Color tends to be a little more effort to
    /// work with, thanks to gamma and linear conversions
    pub fn as_vec4(&self) -> Vec4 {
        match self {
            BufferType::Vec4(f) | BufferType::Mat2(f) => {
                Vec4::new(f[0].as_f32(), f[1].as_f32(), f[2].as_f32(), f[3].as_f32())
            }
            a => panic!("Buffertype {:?} cannot be vec4", a),
        }
    }
}

impl Into<DataVec> for Vec<BufferType> {
    fn into(self) -> DataVec {
        let size = self.len();

        if size == 0 {
            todo!(
                "Converting an empty buffertype to a datavec is impossible and requires a refactor"
            );
        }

        match self[0] {
            BufferType::Scalar(_) => {
                let mut vec = Vec::with_capacity(size);

                for ty in self {
                    vec.push(ty.as_f32());
                }

                DataVec::Float(vec)
            }
            BufferType::Vec2(_) => {
                let mut vec = Vec::with_capacity(size);

                for ty in self {
                    vec.push(ty.as_vec2());
                }

                DataVec::Vec2(vec)
            }
            BufferType::Vec3(_) => {
                let mut vec = Vec::with_capacity(size);

                for ty in self {
                    vec.push(ty.as_vec3a());
                }

                DataVec::Vec3(vec)
            }
            BufferType::Vec4(_) | BufferType::Mat2(_) => {
                let mut vec = Vec::with_capacity(size);

                for ty in self {
                    vec.push(ty.as_color());
                }

                DataVec::Color(vec)
            }
            a => unimplemented!("Cannot convert buffertype array of {:?} to datavec", a),
        }
    }
}

pub struct AccessorReader {
    /// A cache for decoded buffer data by ID so we don't decode it a ton
    buf_cache: HashMap<usize, Vec<u8>>,
    /// A cache of accessor data that was parsed from a buffer
    accessor_cache: HashMap<usize, Vec<BufferType>>,
}

impl AccessorReader {
    pub fn new(buf_capacity: usize, accessor_capacity: usize) -> Self {
        Self {
            buf_cache: HashMap::with_capacity(buf_capacity),
            accessor_cache: HashMap::with_capacity(accessor_capacity),
        }
    }

    /// This assumes the GLTF that is passed in is always the same. If it is not, it will
    /// fail to function as one might intend.
    pub fn read_accessor(
        &mut self,
        gltf: &Gltf,
        accessor_ind: usize,
    ) -> Result<&Vec<BufferType>, AccessorReadError> {
        // if let Some() doesn't seem to work here due to lifetimes?
        if self.accessor_cache.contains_key(&accessor_ind) {
            return Ok(self.accessor_cache.get(&accessor_ind).unwrap());
        }

        let accessor = errify!(
            gltf.accessors.get(accessor_ind),
            AccessorReadError::InvalidAccessor
        )?;

        let view_id = errify!(accessor.buffer_view, AccessorReadError::NoBufferView)?;

        let view = errify!(
            gltf.buffer_views.get(view_id),
            AccessorReadError::InvalidBufferView
        )?;

        let data = if let Some(data) = self.buf_cache.get_mut(&view.buffer) {
            data
        } else {
            let buf = errify!(
                gltf.buffers.get(view.buffer),
                AccessorReadError::InvalidBuffer
            )?;

            // todo support file based binary data
            if !buf.uri.starts_with("data:application/octet-stream;base64,") {
                return Err(AccessorReadError::UnsupportedBufferData);
            }

            // Remove the metadata prefix
            let only_data = remove_start_letters(&buf.uri, 37);
            let data = base64::engine::general_purpose::STANDARD.decode(only_data)?;
            self.buf_cache.insert(view.buffer, data);

            self.buf_cache.get_mut(&view.buffer).unwrap()
        };

        // We need to skip to the view's byte offset, read the num of bytes from the view, then
        // skip to the accessor's offset in that view
        let mut bytes = Bytes::from_iter(
            data.iter()
                .skip(view.byte_offset)
                .take(view.byte_length)
                .skip(accessor.byte_offset)
                .cloned(),
        );

        let mut accum = Vec::with_capacity(accessor.count);

        for _ in 0..accessor.count {
            // Supposedly the common practice is that stride is after the size of the type was read.
            // eg. read 2 bytes (SHORT) -> skip 1 byte (based on byteStride) -> 2 bytes (SHORT)
            accum.push(read_type(&accessor, &mut bytes));

            // Skip the byte stride
            for _ in 0..view.byte_stride {
                bytes.get_u8();
            }
        }

        self.accessor_cache.insert(accessor_ind, accum);

        Ok(self.accessor_cache.get(&accessor_ind).unwrap())
    }
}

/// Creates an array using the first call of `$value` as the default for all elements,
/// then proceeds to call `$value` again `$count - 1` more times. Effectively calling
/// `$value` `$count` times to fill an array of `$count` elements.
macro_rules! call_and_collect {
    ($value:expr; $count:expr) => {{
        let mut array = [$value; $count];
        // first is already set
        for i in 1..$count {
            array[i] = $value;
        }
        array
    }};
}

fn read_type(accessor: &Accessor, bytes: &mut Bytes) -> BufferType {
    match accessor.ty {
        AccessorType::Scalar => BufferType::Scalar(match accessor.component_type {
            AccessorComponentType::Byte => bytes.get_i8().into(),
            AccessorComponentType::UnsignedByte => bytes.get_u8().into(),
            AccessorComponentType::Short => bytes.get_i16_le().into(),
            AccessorComponentType::UnsignedShort => bytes.get_u16_le().into(),
            AccessorComponentType::UnsignedInt => bytes.get_u32_le().into(),
            AccessorComponentType::Float => bytes.get_f32_le().into(),
        }),
        AccessorType::Vec2 => {
            const N: usize = 2;
            BufferType::Vec2(match accessor.component_type {
                AccessorComponentType::Byte => {
                    call_and_collect!(bytes.get_i8().into(); N)
                }
                AccessorComponentType::UnsignedByte => {
                    call_and_collect!(bytes.get_u8().into(); N)
                }
                AccessorComponentType::Short => {
                    call_and_collect!(bytes.get_i16_le().into(); N)
                }
                AccessorComponentType::UnsignedShort => {
                    call_and_collect!(bytes.get_u16_le().into(); N)
                }
                AccessorComponentType::UnsignedInt => {
                    call_and_collect!(bytes.get_u32_le().into(); N)
                }
                AccessorComponentType::Float => {
                    call_and_collect!(bytes.get_f32_le().into(); N)
                }
            })
        }
        AccessorType::Vec3 => {
            const N: usize = 3;
            BufferType::Vec3(match accessor.component_type {
                AccessorComponentType::Byte => {
                    call_and_collect!(bytes.get_i8().into(); N)
                }
                AccessorComponentType::UnsignedByte => {
                    call_and_collect!(bytes.get_u8().into(); N)
                }
                AccessorComponentType::Short => {
                    call_and_collect!(bytes.get_i16_le().into(); N)
                }
                AccessorComponentType::UnsignedShort => {
                    call_and_collect!(bytes.get_u16_le().into(); N)
                }
                AccessorComponentType::UnsignedInt => {
                    call_and_collect!(bytes.get_u32_le().into(); N)
                }
                AccessorComponentType::Float => {
                    call_and_collect!(bytes.get_f32_le().into(); N)
                }
            })
        }
        AccessorType::Vec4 => {
            const N: usize = 4;
            BufferType::Vec4(match accessor.component_type {
                AccessorComponentType::Byte => {
                    call_and_collect!(bytes.get_i8().into(); N)
                }
                AccessorComponentType::UnsignedByte => {
                    call_and_collect!(bytes.get_u8().into(); N)
                }
                AccessorComponentType::Short => {
                    call_and_collect!(bytes.get_i16_le().into(); N)
                }
                AccessorComponentType::UnsignedShort => {
                    call_and_collect!(bytes.get_u16_le().into(); N)
                }
                AccessorComponentType::UnsignedInt => {
                    call_and_collect!(bytes.get_u32_le().into(); N)
                }
                AccessorComponentType::Float => {
                    call_and_collect!(bytes.get_f32_le().into(); N)
                }
            })
        }
        AccessorType::Mat2 => {
            const N: usize = 4;
            BufferType::Mat2(match accessor.component_type {
                AccessorComponentType::Byte => {
                    call_and_collect!(bytes.get_i8().into(); N)
                }
                AccessorComponentType::UnsignedByte => {
                    call_and_collect!(bytes.get_u8().into(); N)
                }
                AccessorComponentType::Short => {
                    call_and_collect!(bytes.get_i16_le().into(); N)
                }
                AccessorComponentType::UnsignedShort => {
                    call_and_collect!(bytes.get_u16_le().into(); N)
                }
                AccessorComponentType::UnsignedInt => {
                    call_and_collect!(bytes.get_u32_le().into(); N)
                }
                AccessorComponentType::Float => {
                    call_and_collect!(bytes.get_f32_le().into(); N)
                }
            })
        }
        AccessorType::Mat3 => {
            const N: usize = 9;
            BufferType::Mat3(match accessor.component_type {
                AccessorComponentType::Byte => {
                    call_and_collect!(bytes.get_i8().into(); N)
                }
                AccessorComponentType::UnsignedByte => {
                    call_and_collect!(bytes.get_u8().into(); N)
                }
                AccessorComponentType::Short => {
                    call_and_collect!(bytes.get_i16_le().into(); N)
                }
                AccessorComponentType::UnsignedShort => {
                    call_and_collect!(bytes.get_u16_le().into(); N)
                }
                AccessorComponentType::UnsignedInt => {
                    call_and_collect!(bytes.get_u32_le().into(); N)
                }
                AccessorComponentType::Float => {
                    call_and_collect!(bytes.get_f32_le().into(); N)
                }
            })
        }
        AccessorType::Mat4 => {
            const N: usize = 16;
            BufferType::Mat4(match accessor.component_type {
                AccessorComponentType::Byte => {
                    call_and_collect!(bytes.get_i8().into(); N)
                }
                AccessorComponentType::UnsignedByte => {
                    call_and_collect!(bytes.get_u8().into(); N)
                }
                AccessorComponentType::Short => {
                    call_and_collect!(bytes.get_i16_le().into(); N)
                }
                AccessorComponentType::UnsignedShort => {
                    call_and_collect!(bytes.get_u16_le().into(); N)
                }
                AccessorComponentType::UnsignedInt => {
                    call_and_collect!(bytes.get_u32_le().into(); N)
                }
                AccessorComponentType::Float => {
                    call_and_collect!(bytes.get_f32_le().into(); N)
                }
            })
        }
    }
}
