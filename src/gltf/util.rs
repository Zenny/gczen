use glam::Vec3;
use mugltf::Node;
use serde::{Deserialize, Serialize};
use serde_json::Value;

pub fn value_as_f32(v: &Value) -> Option<f32> {
    match v {
        Value::Number(n) => n
            .as_f64()
            .map(|v| {
                let f = v as f32;
                if f.is_finite() {
                    Some(f)
                } else {
                    // Out of range
                    None
                }
            })
            .flatten(),
        _ => {
            // NaN
            None
        }
    }
}

/// Returns which ind is the single weight
pub fn single_weight(v: &[f32], mesh: &str) -> Option<usize> {
    let mut nonzero = 0;
    let mut ind = 0;
    for (i, weight) in v.into_iter().enumerate() {
        if *weight != 0.0 {
            nonzero += 1;
            ind = i;

            if nonzero > 1 {
                return None;
            }
        }
    }

    // If it's zero, this is also false
    if nonzero == 0 {
        log::warn!("Found vertex with zero weights in mesh {:?}. This is usually unintentional, and will force it to be a skinned mesh.", mesh);
        None
    } else {
        Some(ind)
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct BoundingBox {
    pub center: Xyz,
    pub size: Xyz,
}

impl BoundingBox {
    pub fn min_max(&self) -> (Vec3, Vec3) {
        let halfsize = self.size.as_vec3() * 0.5;
        let min = Vec3::new(
            self.center.x - halfsize.x,
            self.center.y - halfsize.y,
            self.center.z - halfsize.z,
        );
        let max = Vec3::new(
            self.center.x + halfsize.x,
            self.center.y + halfsize.y,
            self.center.z + halfsize.z,
        );

        (min, max)
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Xyz {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Xyz {
    pub fn all_valid(&self) -> bool {
        self.x.is_finite() && self.y.is_finite() && self.z.is_finite()
    }

    pub fn as_vec3(&self) -> Vec3 {
        Vec3::new(self.x, self.y, self.z)
    }
}

/// Returns (bounding_sphere_radius, bounding_box_min, bounding_box_max)
pub fn parse_bounding(node: &Node) -> (f32, Vec3, Vec3) {
    let bounding_sphere_radius = node
        .extras
        .get("bounding_sphere_radius")
        .map(value_as_f32)
        .unwrap_or_else(|| {
            // No field found
            log::warn!("bounding_sphere_radius defaulting: no field found");
            Some(0.0)
        })
        .unwrap_or_else(|| {
            log::warn!(
                "bounding_sphere_radius is not a valid value for node {:?}. Defaulting to 0",
                node.name
            );
            0.0
        });

    let mut bounding_box = node
        .extras
        .get("bounding_box")
        .map(|v| serde_json::from_value::<BoundingBox>(v.clone()).ok())
        .unwrap_or_else(|| {
            // No field found
            log::warn!("bounding_box defaulting: no field found");
            Some(Default::default())
        })
        .unwrap_or_else(|| {
            // parse failed
            log::warn!(
                "bounding_box is not a valid value for node {:?}. Defaulting to 0",
                node.name
            );
            Default::default()
        });

    if !bounding_box.center.all_valid() || !bounding_box.size.all_valid() {
        log::warn!(
            "bounding_box contains invalid values for node {:?}. Defaulting to 0",
            node.name
        );
        bounding_box = Default::default();
    }

    let (bounding_box_min, bounding_box_max) = bounding_box.min_max();

    (bounding_sphere_radius, bounding_box_min, bounding_box_max)
}
