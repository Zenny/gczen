use base64::Engine;
use glam::Mat4;
use mugltf::*;

use crate::{errify, error::GltfError, gc::bmd::evp1::Evp1, util::accessor_alignment};

pub struct GltfBuilder {
    gltf: Gltf,
    buffer: Vec<u8>,
    bind_matrices: Vec<Mat4>,
}

impl GltfBuilder {
    pub fn new() -> Self {
        let mut gltf: Gltf = Default::default();

        gltf.asset.generator = format!("Sunburst v{}", clap::crate_version!());
        gltf.asset.version = "2.0".to_string();
        gltf.scene = Some(0);
        // blender only supports 1 scene
        gltf.scenes.push(Scene {
            nodes: vec![],
            name: "".to_string(),
            extras: Default::default(),
        });

        Self {
            gltf,
            buffer: vec![],
            bind_matrices: vec![],
        }
    }

    /// The count is the actual count of elements before they were turned to bytes.
    ///
    /// Returns accessor index
    pub fn add_accessor(
        &mut self,
        view_id: usize,
        acc_name: String,
        byte_offset: usize,
        count: usize,
        component_type: AccessorComponentType,
        ty: AccessorType,
    ) -> usize {
        let acc = Accessor {
            buffer_view: Some(view_id),
            byte_offset,
            component_type,
            normalized: false,
            count,
            ty,
            max: vec![],
            min: vec![],
            sparse: None,
            name: acc_name,
            extras: Default::default(),
        };

        let accessor_id = self.gltf.accessors.len();

        self.gltf.accessors.push(acc);

        accessor_id
    }

    pub fn add_buffer_view(
        &mut self,
        buf_name: String,
        data: Vec<u8>,
        target: Option<BufferViewTarget>,
        component_type: AccessorComponentType,
    ) -> usize {
        // Align the buffer
        if let Some(align) = accessor_alignment(&component_type) {
            let md = self.buffer.len() % align;
            // if it's 0 we're already in a good spot
            if md != 0 {
                let pads = align - md;
                self.buffer.resize(self.buffer.len() + pads, 0xFF);
            }
        }

        // view for this attribute's data
        let view = BufferView {
            buffer: 0,
            byte_offset: self.buffer.len(),
            byte_length: data.len(),
            byte_stride: 0,
            target: target,
            name: buf_name,
            extras: Default::default(),
        };

        let view_id = self.gltf.buffer_views.len();

        self.gltf.buffer_views.push(view);

        self.buffer.extend(data);

        view_id
    }

    /// This assumes data was reserved and already sized properly
    pub fn replace_buffer_view(
        &mut self,
        buf_name: String,
        data: Vec<u8>,
    ) -> Result<usize, GltfError> {
        let Some(view_id) = self.find_buffer_view(buf_name.clone()) else {
            return Err(GltfError::NoBufferViewByName(buf_name));
        };

        let buf = errify!(
            self.gltf.buffer_views.get(view_id),
            GltfError::NoBufferViewById(view_id)
        )?;

        if data.len() != buf.byte_length {
            return Err(GltfError::BufferViewReservedWrong(
                buf_name,
                buf.byte_length,
                data.len(),
            ));
        }

        let offset = buf.byte_offset;

        self.buffer[offset..][..data.len()].copy_from_slice(&data);

        Ok(view_id)
    }

    pub fn add_mesh(&mut self, mesh: Mesh) -> usize {
        let id = self.gltf.meshes.len();

        self.gltf.meshes.push(mesh);

        id
    }

    pub fn add_node(&mut self, node: Node) -> usize {
        let id = self.gltf.nodes.len();

        self.gltf.nodes.push(node);

        id
    }

    pub fn add_scene_node(&mut self, node_id: usize) {
        self.gltf.scenes[0].nodes.push(node_id);
    }

    pub fn add_scene(&mut self, scene: Scene) -> usize {
        let id = self.gltf.scenes.len();

        self.gltf.scenes.push(scene);

        id
    }

    pub fn add_sampler(&mut self, sampler: Sampler) -> usize {
        let id = self.gltf.samplers.len();

        self.gltf.samplers.push(sampler);

        id
    }

    pub fn add_texture(&mut self, tex: Texture) -> usize {
        let id = self.gltf.textures.len();

        self.gltf.textures.push(tex);

        id
    }

    pub fn add_image(&mut self, img: Image) -> usize {
        let id = self.gltf.images.len();

        self.gltf.images.push(img);

        id
    }

    pub fn add_material(&mut self, mat: Material) -> usize {
        let id = self.gltf.materials.len();

        self.gltf.materials.push(mat);

        id
    }

    pub fn add_skin(&mut self, skin: Skin) -> usize {
        let id = self.gltf.skins.len();

        self.gltf.skins.push(skin);

        id
    }

    pub fn add_joint_to_skin(
        &mut self,
        skin_id: usize,
        joint_id: usize,
        bone_id: u16,
        evp1: &Evp1,
    ) {
        self.gltf.skins[skin_id].joints.push(joint_id);
        // identity is used if no matrix exists (Objects are just parented to bones and that's it).
        // If all matrices are identity, none need to be packed into the bmd (?)
        let bind = evp1
            .matrices
            .get(bone_id as usize)
            .cloned()
            .unwrap_or_else(|| Mat4::IDENTITY);
        self.bind_matrices.push(bind);
    }

    pub fn invert_skin_joints(&mut self, skin_id: usize) {
        self.gltf.skins[skin_id].joints.reverse();
        self.bind_matrices.reverse();
    }

    pub fn finalize(mut self) -> Gltf {
        if self.bind_matrices.len() > 0 {
            let bind_mat_bytes = self
                .bind_matrices
                .iter()
                .map(|m| m.to_cols_array().into_iter().map(|f| f.to_le_bytes()))
                .flatten()
                .flatten()
                .collect::<Vec<_>>();

            let bind_mat_buf = self.add_buffer_view(
                "BIND_MATS".to_string(),
                bind_mat_bytes,
                None,
                AccessorComponentType::Float,
            );

            let bind_mat_acc = self.add_accessor(
                bind_mat_buf,
                "BIND_MATS".to_string(),
                0,
                self.bind_matrices.len(),
                AccessorComponentType::Float,
                AccessorType::Mat4,
            );

            // todo more than one skin?
            self.gltf.skins[0].inverse_bind_matrices = Some(bind_mat_acc);
        }

        let len = self.buffer.len();
        let b64buf = base64::engine::general_purpose::STANDARD.encode(&self.buffer);

        let out_buf = Buffer {
            uri: format!("data:application/octet-stream;base64,{}", b64buf),
            byte_length: len,
            name: "".to_string(),
            extras: Default::default(),
        };

        self.gltf.buffers.push(out_buf);

        self.gltf
    }

    pub fn next_buffer_view_id(&self) -> usize {
        self.gltf.buffer_views.len()
    }

    pub fn find_buffer_view(&self, name: String) -> Option<usize> {
        self.gltf.buffer_views.iter().position(|v| v.name == name)
    }

    pub fn find_accessor(&self, name: String) -> Option<usize> {
        self.gltf.accessors.iter().position(|a| a.name == name)
    }
}
